<?php
use Magento\Framework\App\Bootstrap;
require dirname(__DIR__) . '/app/bootstrap.php';

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();

$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

//get category factory
$categoryCollectionFactory = $objectManager->create('\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory');
$categoryCollection = $categoryCollectionFactory->create();
$categoryCollection->addAttributeToSelect('*');

foreach ($categoryCollection as $category) {
	$id = $category->getId();
	$categories[$id]['name'] = $category->getName();
	$categories[$id]['path'] = $category->getPath();
}
foreach ($categoryCollection as $category) {
	$id = $category->getId();
	$path = explode('/', $categories[$id]['path']);
	$string = '';
	foreach ($path as $pathId) {
		$string .= $categories[$pathId]['name'] . ' : ';
		$cnt++;
	}
//	$string .= ';' . $id . "<br />";
	$string .= "<br />";
	echo $string;
}
