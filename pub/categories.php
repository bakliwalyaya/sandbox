
<?php

require dirname(__DIR__) . '/app/bootstrap.php';

$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
$_objectManager = $bootstrap->getObjectManager();
$state = $_objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$registry = $_objectManager->get('Magento\Framework\Registry');
$registry->register('isSecureArea', true);

$productid = 7010;
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$product = $objectManager->get('Magento\Catalog\Api\ProductRepositoryInterface')->getById($productid);

// Collect options applicable to the configurable product
$productAttributeOptions = $product->getTypeInstance(true)->getConfigurableAttributesAsArray($product);
$attributeOptions = array();
foreach ($productAttributeOptions as $productAttribute) {
	foreach ($productAttribute['values'] as $attribute) {
		$attributeOptions[$productAttribute['label']][$attribute['value_index']] = $attribute['store_label'];
	}
}

$eavConfig = $_objectManager->create('\Magento\Eav\Model\Config');
$attribute = $eavConfig->getAttribute('catalog_product', 'colour');
$options = $attribute->getSource()->getAllOptions();

echo "<pre>";
print_r($attributeOptions);
