<?php
require dirname(__DIR__) . '/app/bootstrap.php';

$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
$_objectManager = $bootstrap->getObjectManager();
$state = $_objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');
$registry = $_objectManager->get('Magento\Framework\Registry');
$registry->register('isSecureArea', true);

//Store id of exported products, This is useful when we have multiple stores.
$store_id = 1;


$attributeset= $_objectManager->get('Magento\Eav\Api\AttributeSetRepositoryInterface');
//Store id of exported products, This is useful when we have multiple stores.
$store_id = 1;

$fp = fopen("attributesets.csv", "w+");
$collection = $_objectManager
        ->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory')
        ->create()
        //->addAttributeToFilter('type_id', array('neq' => 'configurable'))
        ->addAttributeToSelect('*')     
        ->setOrder('created_at', 'desc')
        ->load();

fputcsv($fp,
        [
                'id',
                'sku',
                'type_id',
                'attribute_set',
                'created_at',
                'updated_at',

        ]);
foreach ($collection as $product) {
        $data = array();
        $data[] = $product->getId();
        $data[] = $product->getSku();
        $data[] = $product->getTypeId();
        $attributeSetRepository = $attributeset->get($product->getAttributeSetId());
        $data[] = $attributeSetRepository->getAttributeSetName();
        $data[] = $product->getCreatedAt();
        $data[] = $product->getUpdatedAt();
        fputcsv($fp, $data);

}
fclose($fp);
?>

