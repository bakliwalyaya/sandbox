<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

/*
 * cPanel & WHM® Site Software
 *
 * Core updates should be disabled entirely by the cPanel & WHM® Site Software
 * plugin, as Site Software will provide the updates.  The following line acts
 * as a safeguard, to avoid automatically updating if that plugin is disabled.
 *
 * Allowing updates outside of the Site Software interface in cPanel & WHM®
 * could lead to DATA LOSS.
 *
 * Re-enable automatic background updates at your own risk.
 */
define( 'WP_AUTO_UPDATE_CORE', false );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'alogic_wp');


/** MySQL database username */
define('DB_USER', 'user');

/** MySQL database password */
define('DB_PASSWORD', 'AO!u20wy9say1ad173!');

/** MySQL hostname */
define('DB_HOST', 'alogicdb-cluster.cluster-cgb3qedfdbg7.ap-southeast-2.rds.amazonaws.com');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tX_1jvFmkNWAC17bPWFcHDhMNPtn4HZcCyk5QGLMwJ3BYUGm45Og7SpQ2604pkV8');
define('SECURE_AUTH_KEY',  '15hzyn4LkOfDOqfAPtHCJbFSptFp31fWV9kdyZzoWtGKvBrqG9xrcAw3lkTBDyQD');
define('LOGGED_IN_KEY',    '8zd9FnCPS6a6NNcrYhntfERG9PP7tXGNPXx2jvFFRQnyj8IVvy0PuWuPIMWw1xhR');
define('NONCE_KEY',        'JLtp_R90owsbTdGg_r67GSt9RUiirkGfgfcWlekCTYnjuIrefqFDUTsgJE9L0Srh');
define('AUTH_SALT',        '7MJCuUsUepR9IkaBWYZiWseETW2s7bs0W1Xc6XE4U2fq8Dmr0EiBR1q8RPLmQ56Z');
define('SECURE_AUTH_SALT', '3v6vWkeA1kFGAf1GJ96ZUveOpTjqZ9vJicrGQxffkw0zZ6jqPtXBwguH0eIMACoK');
define('LOGGED_IN_SALT',   'r0GtE1TmuiaCIuIwSbiO_PpACvLG88Xt96sZvoOqcsYDlOI4WPsCdiM04Zfr5jpF');
define('NONCE_SALT',       '7TD1v7GhEBuBxl3p2cMc7XPshd8BXKS4P5V7g_t0Dyq2VIcOlQ7sX7n8DaWgJ8qR');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
/* Multisite */
define( 'WP_ALLOW_MULTISITE', false );


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
