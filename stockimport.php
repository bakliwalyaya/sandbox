<?php
/**
 *
 * @author Raj KB<magepsycho@gmail.com>
 * @website http://www.magepsycho.com
 * @extension MassImporterPro: Pricing - http://www.magepsycho.com/mass-importer-pro-price-importer-regular-special-tier-group.html
 */
include 'gmail-imap.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
// Capture warning / notice as exception
/*set_error_handler('ctv_exceptions_error_handler');
function ctv_exceptions_error_handler($severity, $message, $filename, $lineno) {
if (error_reporting() == 0) {
return;
}
if (error_reporting() & $severity) {
throw new ErrorException($message, 0, $severity, $filename, $lineno);
}
}*/

use Magento\Framework\App\Bootstrap;

require __DIR__ . '/app/bootstrap.php';

$params = $_SERVER;

$bootstrap = Bootstrap::create(BP, $params);

$obj = $bootstrap->getObjectManager();

$state = $obj->get('Magento\Framework\App\State');
//$state->setAreaCode('frontend');
$state->setAreaCode('adminhtml');

/**************************************************************************************************/
// UTILITY FUNCTIONS - START
/**************************************************************************************************/

function _mpLog($data, $includeSep = false) {
	$fileName = BP . '/var/log/m2-magepsycho-import-prices.log';
	if ($includeSep) {
		$separator = str_repeat('=', 70);
		file_put_contents($fileName, $separator . '<br />' . PHP_EOL, FILE_APPEND | LOCK_EX);
	}
	file_put_contents($fileName, $data . '<br />' . PHP_EOL, FILE_APPEND | LOCK_EX);
}

function mpLogAndPrint($message, $separator = false) {
	_mpLog($message, $separator);
	if (is_array($message) || is_object($message)) {
		print_r($message);
	} else {
		echo $message . '<br />' . PHP_EOL;
	}
	if ($separator) {
		echo str_repeat('=', 70) . '<br />' . PHP_EOL;
	}
}

function getIndex($field) {
	global $headers;
	$index = array_search($field, $headers);
	if (!strlen($index)) {
		$index = -1;
	}
	return $index;
}

function readCsvRows($csvFile) {
	$rows = [];
	$fileHandle = fopen($csvFile, 'r');
	while (($row = fgetcsv($fileHandle, 0, ',', '"', '"')) !== false) {
		$rows[] = $row;
	}
	fclose($fileHandle);
	return $rows;
}

function _getResourceConnection() {
	global $obj;
	return $obj->get('Magento\Framework\App\ResourceConnection');
}

function _getReadConnection() {
	return _getConnection('core_read');
}

function _getWriteConnection() {
	return _getConnection('core_write');
}

function _getConnection($type = 'core_read') {
	return _getResourceConnection()->getConnection($type);
}

function _getTableName($tableName) {
	return _getResourceConnection()->getTableName($tableName);
}

function _getAttributeId($attributeCode) {
	$connection = _getReadConnection();
	$sql = "SELECT attribute_id FROM " . _getTableName('eav_attribute') . " WHERE entity_type_id = ? AND attribute_code = ?";
	return $connection->fetchOne(
		$sql,
		[
			_getEntityTypeId('catalog_product'),
			$attributeCode,
		]
	);
}

function _getEntityTypeId($entityTypeCode) {
	$connection = _getConnection('core_read');
	$sql = "SELECT entity_type_id FROM " . _getTableName('eav_entity_type') . " WHERE entity_type_code = ?";
	return $connection->fetchOne(
		$sql,
		[
			$entityTypeCode,
		]
	);
}

function _getIdFromSku($sku) {
	$connection = _getConnection('core_read');
	$sql = "SELECT entity_id FROM " . _getTableName('catalog_product_entity') . " WHERE sku = ?";
	return $connection->fetchOne(
		$sql,
		[
			$sku,
		]
	);
}

function checkIfSkuExists($sku) {
	$connection = _getConnection('core_read');
	$sql = "SELECT COUNT(*) AS count_no FROM " . _getTableName('catalog_product_entity') . " WHERE sku = ?";
	return $connection->fetchOne($sql, [$sku]);
}

function updateStocks($sku, $stock) {
	$connection = _getWriteConnection();
	$entityId = _getIdFromSku($sku);
	// $attributeId    = _getAttributeId();
	global $obj;
	$stockRegistry = $obj->get('Magento\CatalogInventory\Api\StockRegistryInterface');
	if ($stock <= 0) {
		$stock = 0;
		$storeId = 1;
		$productFactory = $obj->get('Magento\Catalog\Api\ProductRepositoryInterface');
		$product = $productFactory->get($sku);
		$product->setStoreId($storeId);
		$product->setForSale(0);
		$product->setShowForSale(0);
		$product->setComingSoon(1);
		$product->save();
		$c = $obj->get('Magento\ConfigurableProduct\Model\Product\Type\Configurable');
		$configurable = $c->getParentIdsByChild($product->getId());
		if (count($configurable) > 0) {
			$cproduct = $obj->create('Magento\Catalog\Model\Product')->load($configurable);
			$cproduct->setStoreId($storeId);
			$cproduct->setShowForSale(0);
			$product->setComingSoon(1);
			$cproduct->setForSale(0);
			$cproduct->save();
		}
	}
	$stockItem = $stockRegistry->getStockItemBySku($sku);
	$stockItem->setQty($stock);
	$stockItem->setIsInStock((bool) $stock);
	$stockRegistry->updateStockItemBySku($sku, $stockItem);
}

function enableAddToCart($sku, $price) {
	global $obj;
	$storeId = 1;
	$productFactory = $obj->get('Magento\Catalog\Api\ProductRepositoryInterface');
	$product = $productFactory->get($sku);
	echo "Getting SKU";
	$c = $obj->get('Magento\ConfigurableProduct\Model\Product\Type\Configurable');
	$configurable = $c->getParentIdsByChild($product->getId());
	if (count($configurable) > 0) {
		echo "Getting configurable";
		$cproduct = $obj->create('Magento\Catalog\Model\Product')->load($configurable);
		$cproduct->setStoreId($storeId);
		$cproduct->setShowForSale(1);
		$cproduct->setForSale(1);
		$cproduct->save();
	}
	$product->setPrice($price);
	$product->setStoreId($storeId);
	$product->setForSale(1);
	$product->setShowForSale(1);
	$product->save();
}

function updatePrices($sku, $price, $storeId = 0) {
	$connection = _getWriteConnection();
	$entityId = _getIdFromSku($sku);
	$attributeId = _getAttributeId('price');
	// $sql = "INSERT INTO " . _getTableName('catalog_product_entity_decimal') . " (attribute_id, store_id, entity_id, value) VALUES (?, 0, ?, ?) ON DUPLICATE KEY UPDATE value=VALUES($price)";

	$connection->query("UPDATE catalog_product_entity_decimal SET value =" . $price . " WHERE entity_id =" . $entityId . ";");

}

/**************************************************************************************************/
// UTILITY FUNCTIONS - END
/**************************************************************************************************/
try {
	echo "Looking for the file";
	$csvFile = 'var/stockimport/searchresults.csv'; #EDIT - The path to import CSV file (Relative to Magento2 Root)
	$csvData = readCsvRows(BP . '/' . $csvFile);
	$headers = array_shift($csvData);
	$count = 0;
	echo "File Found and starting import";
	foreach ($csvData as $_data) {
		$count++;
		$sku = $_data[getIndex('Name')];
		echo $sku . "\n";
		$stock = $_data[getIndex('Location Available')];
		$price = $_data[getIndex('Base Price')];
		if (!checkIfSkuExists($sku)) {
			$message = $count . '. FAILURE:: Product with SKU (' . $sku . ') doesn\'t exist.';
			mpLogAndPrint($message);
			continue;
		}
		try {
			echo "Updating add to cart";
			enableAddToCart($sku, $price);
			echo "Updating stock";
			if ($sku == 'ULDWAV' || $sku == 'ULDWAV-SLV' || $sku == 'ULDWAV-SGR') {
				continue;
			}

			updateStocks($sku, $stock);
			//updatePrices($sku, $price);
			$message = $count . '. SUCCESS:: Updated SKU (' . $sku . ') with price (' . $price . ')';
			mpLogAndPrint($message);
		} catch (Exception $e) {
			$message = $count . '. ERROR:: While updating  SKU (' . $sku . ') with Price (' . $price . ') => ' . $e->getMessage();
			mpLogAndPrint($message);
		}
	}
} catch (Exception $e) {
	mpLogAndPrint(
		'EXCEPTION::' . $e->getTraceAsString()
	);
}
unlink(dirname(__FILE__) . "/var/stockimport/" . 'searchresults.csv');