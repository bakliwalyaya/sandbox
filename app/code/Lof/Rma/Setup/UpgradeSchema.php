<?php
/**
 * Landofcoder
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * https://landofcoder.com/license
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category   Landofcoder
 * @package    Lof_Rma
 * @copyright  Copyright (c) 2019 Landofcoder (http://www.landofcoder.com/)
 * @license    http://www.landofcoder.com/LICENSE-1.0.html
 */

namespace Lof\Rma\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (version_compare($context->getVersion(), '1.0.7', '<')) {
            $table = $installer->getTable('lof_rma_rma');
            $installer->getConnection()->addColumn(
                $table,
                'parent_rma_id',
                [
                    'type'     => Table::TYPE_INTEGER,
                    'length'   => 11,
                    'default'  => '0',
                    'unsigned' => false,
                    'comment'  => 'parent rma id, default: 0'
                ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.8', '<')) {
            $table = $installer->getConnection()->newTable(
                   $installer->getTable('lof_rma_rma_track')
               )->addColumn(
                   'entity_id',
                   Table::TYPE_INTEGER,
                   null,
                   ['unsigned' => false, 'nullable' => false, 'identity' => true, 'primary' => true],
                   'Track Entity Id'
               )->addColumn(
                    'parent_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => false, 'nullable' => false, 'default' => 0],
                    'Parent id'
                )->addColumn(
                    'weight',
                    Table::TYPE_DECIMAL,
                    '12,4',
                    ['nullable' => true],
                    'Weight'
                )->addColumn(
                    'qty',
                    Table::TYPE_DECIMAL,
                    '12,4',
                    ['nullable' => true],
                    'QTY'
                )->addColumn(
                    'rma_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'RMA Id'
                )->addColumn(
                   'track_number',
                   Table::TYPE_TEXT,
                   '64k',
                   ['nullable' => true],
                   'Return Tracking Number'
               )->addColumn(
                   'description',
                   Table::TYPE_TEXT,
                   '64k',
                   ['nullable' => true],
                   'Description'
               )->addColumn(
                    'title',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Title'
                )->addColumn(
                    'carrier_code',
                    Table::TYPE_TEXT,
                    32,
                    ['nullable' => true],
                    'Carrier Code'
                )->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Created At'
                )->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => true, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                    'Updated At'
                );
           $installer->getConnection()->createTable($table);
        }
        if (version_compare($context->getVersion(), '1.0.9', '<')) {
            $table = $installer->getTable('lof_rma_rma');
            $installer->getConnection()->addColumn(
                $table,
                'customer_email',
                [
                    'type'     => Table::TYPE_TEXT,
                    'length'   => 150,
                    'default'  => '',
                    'comment'  => 'customer email to filter rma by guest'
                ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.10', '<')) {
            $table = $installer->getTable('lof_rma_message');
            $installer->getConnection()->addColumn(
                $table,
                'customer_email',
                [
                    'type'     => Table::TYPE_TEXT,
                    'length'   => 150,
                    'default'  => '',
                    'comment'  => 'customer email to filter rma by guest'
                ]
            );
        }
        $installer->endSetup();
    }
}
