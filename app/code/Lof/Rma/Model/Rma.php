<?php
/**
 * LandOfCoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://www.landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   LandOfCoder
 * @package    Lof_Rma
 * @copyright  Copyright (c) 2020 Landofcoder (http://www.LandOfCoder.com/)
 * @license    http://www.LandOfCoder.com/LICENSE-1.0.html
 */



namespace Lof\Rma\Model;

use Magento\Framework\DataObject\IdentityInterface;

class Rma extends \Magento\Framework\Model\AbstractModel
    implements \Lof\Rma\Api\Data\RmaInterface, IdentityInterface
{
    const CACHE_TAG = 'rma_rma';

    /**
     * {@inheritdoc}
     */
    protected $_cacheTag = 'rma_rma';

    /**
     * {@inheritdoc}
     */
    protected $_eventPrefix = 'rma_rma';

    public function __construct(
        \Lof\Rma\Helper\Data $datahelper,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);

        $this->orderFactory = $orderFactory;
        $this->datahelper = $datahelper;
    }

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init('Lof\Rma\Model\ResourceModel\Rma');
    }

    /**
     * {@inheritdoc}
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG.'_'.$this->getId()];
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave()
    {
        parent::afterSave();
        if (!$this->getIncrementId()) {
            $this->setIncrementId($this->datahelper->generateIncrementId($this));
            $this->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getIncrementId()
    {
        return $this->getData(self::KEY_INCREMENT_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setIncrementId($incrementId)
    {
        return $this->setData(self::KEY_INCREMENT_ID, $incrementId);
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomerEmail()
    {
        return $this->getData(self::KEY_CUSTOMER_EMAIL);
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomerEmail($customerEmail)
    {
        return $this->setData(self::KEY_CUSTOMER_EMAIL, $customerEmail);
    }

    /**
     * {@inheritdoc}
     */
    public function getFirstname()
    {
        return $this->getData(self::KEY_FIRSTNAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setFirstname($firstname)
    {
        return $this->setData(self::KEY_FIRSTNAME, $firstname);
    }

    /**
     * {@inheritdoc}
     */
    public function getLastname()
    {
        return $this->getData(self::KEY_LASTNAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setLastname($lastname)
    {
        return $this->setData(self::KEY_LASTNAME, $lastname);
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail()
    {
        return $this->getData(self::KEY_EMAIL);
    }

    /**
     * {@inheritdoc}
     */
    public function setEmail($email)
    {
        return $this->setData(self::KEY_EMAIL, $email);
    }

   

    /**
     * {@inheritdoc}
     */
    public function getCustomerId()
    {
        return $this->getData(self::KEY_CUSTOMER_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::KEY_CUSTOMER_ID, $customerId);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrderId()
    {
        return $this->getData(self::KEY_ORDER_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::KEY_ORDER_ID, $orderId);
    }

    /**
     * {@inheritdoc}
     */
    public function getStatusId()
    {
        return $this->getData(self::KEY_STATUS_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setStatusId($statusId)
    {
        return $this->setData(self::KEY_STATUS_ID, $statusId);
    }

    /**
     * {@inheritdoc}
     */
    public function getStoreId()
    {
        if ($this->getData(self::KEY_STORE_ID)) {
            return $this->getData(self::KEY_STORE_ID);
        }

        return 1;
    }

    /**
     * {@inheritdoc}
     */
    public function setStoreId($storeId)
    {
        return $this->setData(self::KEY_STORE_ID, $storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getTrackingCode()
    {
        return $this->getData(self::KEY_TRACKING_CODE);
    }

    /**
     * {@inheritdoc}
     */
    public function setTrackingCode($trackingCode)
    {
        return $this->setData(self::KEY_TRACKING_CODE, $trackingCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getIsResolved()
    {
        return $this->getData(self::KEY_IS_RESOLVED);
    }

    /**
     * {@inheritdoc}
     */
    public function setIsResolved($isResolved)
    {
        return $this->setData(self::KEY_IS_RESOLVED, $isResolved);
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->getData(self::KEY_CREATED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedAt($date)
    {
        return $this->setData(self::KEY_CREATED_AT, $date);
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::KEY_UPDATED_AT);
    }

    /**
     * {@inheritdoc}
     */
    public function setUpdatedAt($date)
    {
        return $this->setData(self::KEY_UPDATED_AT, $date);
    }

    /**
     * {@inheritdoc}
     */
    public function getIsGift()
    {
        return $this->getData(self::KEY_IS_GIFT);
    }

    /**
     * {@inheritdoc}
     */
    public function setIsGift($isGift)
    {
        return $this->setData(self::KEY_IS_GIFT, $isGift);
    }

    /**
     * {@inheritdoc}
     */
    public function getIsAdminRead()
    {
        return $this->getData(self::KEY_IS_ADMIN_READ);
    }

    /**
     * {@inheritdoc}
     */
    public function setIsAdminRead($isAdminRead)
    {
        return $this->setData(self::KEY_IS_ADMIN_READ, $isAdminRead);
    }


    /**
     * {@inheritdoc}
     */
    public function getUserId()
    {
        return $this->getData(self::KEY_USER_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setUserId($userId)
    {
        return $this->setData(self::KEY_USER_ID, $userId);
    }


    /**
     * {@inheritdoc}
     */
    public function getLastReplyName()
    {
        return $this->getData(self::KEY_LAST_REPLY_NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setLastReplyName($lastReplyName)
    {
        return $this->setData(self::KEY_LAST_REPLY_NAME, $lastReplyName);
    }

    /**
     * {@inheritdoc}
     */
    public function getReturnAddress()
    {
        return $this->getData(self::KEY_RETURN_ADDRESS);
    }

    /**
     * {@inheritdoc}
     */
    public function setReturnAddress($address)
    {
        return $this->setData(self::KEY_RETURN_ADDRESS, $address);
    }

    /**
     * {@inheritdoc}
     */
    public function getParentRmaId(){
        return $this->getData(self::KEY_PARENT_RMA_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setParentRmaId($rma_id){
        return $this->setData(self::KEY_PARENT_RMA_ID, (int)$rma_id);
    }

    /**
     * {@inheritdoc}
     */
    public function getCode()
    {
        return self::MESSAGE_CODE.$this->getIncrementId();
    }

    public function getRmaOrder(){
        if($this->getId() && $this->getOrderId()){
            $order = $this->orderFactory->create()->load($this->getOrderId());
            return $order;
        }
        return false;
    }

    public function getChildRmaIds($parent_rma_id = 0){
        $parent_rma_id = $parent_rma_id?(int)$parent_rma_id:$this->getId();
        if($parent_rma_id){
            $rma_ids = $this->getResource()->getChildRmaIds($parent_rma_id);
            return $rma_ids;
        }
        return false;
    }

    public function getListChildRma($parent_rma_id = 0){
        $rma_ids = $this->getChildRmaIds($parent_rma_id);
        $list_rma = [];
        if($rma_ids){
            foreach($rma_ids as $_rma_id){
                $object = clone $this;
                $list_rma[] = $object->load($_rma_id);
            }
        }
        return $list_rma;
    }

    /**
     * {@inheritdoc}
     */
    public function getBundleOrderIds(){
        return $this->getData(self::KEY_BUNDLE_ORDER_IDS);
    }

    /**
     * {@inheritdoc}
     */
    public function setBundleOrderIds($bundleOrderIds){
        return $this->setData(self::KEY_BUNDLE_ORDER_IDS, $bundleOrderIds);
    }

    /**
     * {@inheritdoc}
     */
    public function getChildIds(){
        $child_rma_ids = $this->getData(self::KEY_CHILD_RMA_IDS);
        $child_rma_ids = is_array($child_rma_ids)?$child_rma_ids:[];
        $current_child_rma = [];
        if($rma_id = $this->getId()){
            $current_child_rma = $this->getChildRmaIds((int)$rma_id);
        }
        $child_rma_ids = array_merge($current_child_rma, $child_rma_ids);
        return $child_rma_ids;
    }

    /**
     * {@inheritdoc}
     */
    public function setChildIds($childRmaIds){
        $child_rma_ids = $this->getChildIds();
        if(!$child_rma_ids){
            $child_rma_ids = [];
        }
        $childRmaIds = trim($childRmaIds);
        if($childRmaIds){
            $arr_rma_ids = explode(",",$childRmaIds);
            if($arr_rma_ids){
                foreach($arr_rma_ids as $rma_id){
                    $rma_id = (int)$rma_id;
                    if(!isset($child_rma_ids[$rma_id])){
                        $child_rma_ids[] = $rma_id;
                    }
                }
            }
        }
        return $this->setData(self::KEY_CHILD_RMA_IDS, $child_rma_ids);
    }

    public function getOrderItemData($orderItemId = 0){
        $orderItemData = [];
        if($orderItemId){
            $orderItemData = $this->getResource()->getOrderItemData($orderItemId);
        }
        return $orderItemData;
    }
}
