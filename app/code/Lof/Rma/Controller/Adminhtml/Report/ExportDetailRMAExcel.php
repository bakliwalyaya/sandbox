<?php

namespace Lof\Rma\Controller\Adminhtml\Report;

use Magento\Framework\App\ResponseInterface;

class ExportDetailRMAExcel extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $_fileFactory;

    /**
     * @var \Magento\Framework\View\Result\LayoutFactory
     */
    protected $resultLayoutFactory;

    /**
     * ExportDetailRMAExcel constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
    )
    {
        $this->_fileFactory = $fileFactory;
        $this->resultLayoutFactory = $resultLayoutFactory;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        $fileName = 'exportReportDetailRmaExcel' . time() . '.xml';
        $content = $this->_view->getLayout()->createBlock(
            '\Lof\Rma\Block\Adminhtml\Report\DetailGrid'
        )->setSaveParametersInSession(
            true
        )->getExcel();
        return $this->_fileFactory->create($fileName, $content);
    }
}
