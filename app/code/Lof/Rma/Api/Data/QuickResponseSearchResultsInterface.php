<?php
/**
 * LandOfCoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://www.landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   LandOfCoder
 * @package    Lof_Rma
 * @copyright  Copyright (c) 2020 Landofcoder (http://www.LandOfCoder.com/)
 * @license    http://www.LandOfCoder.com/LICENSE-1.0.html
 */



namespace Lof\Rma\Api\Data;

/**
 * Interface for quick response search results.
 */
interface QuickResponseSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get quick responses list.
     *
     * @return \Lof\Rma\Api\Data\QuickResponseInterface[]
     */
    public function getItems();

    /**
     * Set quick responses list.
     *
     * @param \Lof\Rma\Api\Data\QuickResponseInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
