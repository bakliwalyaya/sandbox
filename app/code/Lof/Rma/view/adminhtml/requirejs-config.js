var config = {
    map: {
        '*': {
            jqueryMultiFile: 'Lof_Rma/js/multifile/jquery.MultiFile'
        }
    },
    paths: {
        'google-charts': 'https://www.gstatic.com/charts/loader'
    }
};