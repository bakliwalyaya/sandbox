<?php

namespace Lof\Rma\Block\Adminhtml\Report;

class DetailGrid extends \Magento\Backend\Block\Widget\Grid\Extended
{

    /**
     * Registry object
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * DetailGrid constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Lof\Rma\Model\ResourceModel\Rma\Collection $RmaFactory
     * @param \Lof\Rma\Model\ResourceModel\Status\Collection $StatusFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Lof\Rma\Model\ResourceModel\Rma\ExtendedCollection $RmaFactory,
        \Lof\Rma\Model\ResourceModel\Status\Collection $StatusFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    )
    {
        $this->_RmaFactory = $RmaFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_StatusFactory = $StatusFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('lofGrid');
        $this->setSaveParametersInSession(true);
        $this->setFilterVisibility(false);
        $this->setSortable(false);
        $this->setUseAjax(false);
    }

    /**
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareCollection()
    {
        if ($params = $this->getRequest()->getParams()) {
            $type = isset($params['type']) ? $params['type'] : 'month';
            $params['to'] = isset($params['to']) ? $params['to'] : date("Y-m-d");
            $params['from'] = isset($params['from']) ? $params['from'] : date("Y-m-d", strtotime("-1 month"));
            $endate = str_replace('.', '-', $params['to']);
            $startdate = str_replace('.', '-', $params['from']);
        } else {
            $type = 'month';
            $startdate = date("Y-m-d", strtotime("-1 month"));
            $endate = date("Y-m-d");
        }
        $collection = $this->_RmaFactory
            ->setPeriodType($type)
            ->setDateColumnFilter('created_at')
            ->addDateFromFilter($startdate)
            ->addDateToFilter($endate)
            ->_getSelectedColumns();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'increment_id', [
                'header' => __('RMA Number'),
                'type' => 'text',
                'index' => 'increment_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'width' => '30px',
            ]
        );
        $this->addColumn(
            'order_increment_id', [
                'header' => __('Order ID'),
                'index' => 'order_increment_id',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'sku', [
                'header' => __('SKU'),
                'index' => 'sku',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'total_request', [
                'header' => __('Total Request'),
                'index' => 'total_request',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'reason', [
                'header' => __('Reason for return'),
                'index' => 'reason',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'order_customer_email', [
                'header' => __('Customer Email'),
                'index' => 'order_customer_email',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'last_reply_name', [
                'header' => __('Last reply name'),
                'index' => 'last_reply_name',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'status_name', [
                'header' => __('Status'),
                'index' => 'status_name',
                'width' => '50px',
            ]
        );

        $this->addExportType('*/Report/ExportDetailRMACsv', __('CSV'));
        $this->addExportType('*/Report/ExportDetailRMAExcel', __('Excel'));

        return parent::_prepareColumns();
    }

    protected function _toHtml()
    {
        if (isset($_GET["showchart"]) && $_GET["showchart"] == 1) {
            return "";
        }
        return parent::_toHtml();
    }
}
