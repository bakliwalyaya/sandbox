<?php
/**
 * LandOfCoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://www.landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   LandOfCoder
 * @package    Lof_Rma
 * @copyright  Copyright (c) 2020 Landofcoder (http://www.LandOfCoder.com/)
 * @license    http://www.LandOfCoder.com/LICENSE-1.0.html
 */


namespace Lof\Rma\Block\Adminhtml\Rma\Edit\Tab;


class Items extends \Magento\Backend\Block\Template
{

    public function __construct(
        \Lof\Rma\Helper\Data $dataHelper,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Sales\Model\OrderFactory                      $orderFactory,
        \Lof\Rma\Model\RmaFactory                           $rmaFactory ,
        \Lof\Rma\Api\Repository\RmaRepositoryInterface $rmaRepository,
        array $data = []
    ) {

        $this->registry            = $registry;
        $this->request =  $context->getRequest();
        $this->dataHelper             = $dataHelper;
        $this->orderFactory         = $orderFactory;
        $this->rmaFactory = $rmaFactory;
        $this->rmaRepository     = $rmaRepository;
        parent::__construct($context, $data);
    }

    /**
     * @return \Lof\Rma\Model\Rma
     */
    public function getCurrentRma()
    {
        if ($this->registry->registry('current_rma') && $this->registry->registry('current_rma')->getId()) {
            return $this->registry->registry('current_rma');
        }
    } 

    public function getOrder($order_id = 0) {
        $order_id = $order_id?(int)$order_id:$this->getOrderId();
        if(!isset($this->_orders)){
            $this->_orders = [];
        }
        if(!isset($this->_orders[$order_id])){
            $this->_orders[$order_id] = $this->orderFactory->create()->load($order_id);
        }
        
        return $this->_orders[$order_id];
    }

    public function getOrderId() {
        if($this->getCurrentRma())
            return $this->getCurrentRma()->getOrderId();

        $order_id = $this->request->getParam("order_id");
        if(!$order_id || !is_numeric($order_id)){
            $path = trim($this->request->getPathInfo(), '/');
            $params = explode('/', $path);
            $order_id = end($params);
        }
        return (int)$order_id;
    }

    public function getOrderIdByRmaId($rma_id = 0){
        if($rma_id){
            $child_rma = $this->getChildRma($rma_id);
            if($child_rma){
                $order = $child_rma->getRmaOrder();
                if($order){
                    return $order->getId();
                }
            }
        }
        return 0;
    }

    public function getRmaOrderId($rma_id = 0){
        if($rma_id){
            $child_rma = $this->getChildRma($rma_id);
            if($child_rma){
                $order = $child_rma->getRmaOrder();
                if($order){
                    return $order->getIncrementId();
                }
            }
        }
        return "";
    }

    public function getChildRma($rma_id){
        if(!isset($this->_rma_list)){
            $this->_rma_list = [];
        }
        if(!isset($this->_rma_list[$rma_id])){
            $this->_rma_list[$rma_id] = $this->rmaRepository->getById($rma_id);
        }
        return $this->_rma_list[$rma_id];
    }

    public function getRmaItemData($item)
    {
        $rma_id = $item->getData("rma_id");
        if(!$rma_id) {
            $rma_id = $this->getCurrentRma()->getId();
        }
        return $this->dataHelper->getRmaItemData($item,(int)$rma_id);
    }

    public function getListChildRma($parent_rma_id){
        if(!isset($this->_rma_list)){
            $this->_rma_list = [];
        }
        $child_rma_ids = $this->rmaFactory->create()->getChildRmaIds($parent_rma_id);
        if($child_rma_ids){
            foreach($child_rma_ids as $rma_id){
                if(!isset($this->_rma_list[$rma_id])){
                    $this->_rma_list[$rma_id] = $this->rmaRepository->getById($rma_id);
                }
            }
        }
        return $this->_rma_list;
    }

    public function getChildRmaOrderItems($parent_rma_id) {
        if(!isset($this->_child_rma_order_items)){
            $this->_child_rma_order_items = [];
            if(!isset($this->_child_rma)){
                $rma_list = $this->getListChildRma($parent_rma_id);
                $this->_child_rma = $rma_list;
            }else {
                $rma_list = $this->_child_rma;
            }
            
            if($rma_list){
                foreach($rma_list as $_rma){
                    $order = $this->getOrder($_rma->getOrderId());
                    $tmpItems = $order->getAllItems();
                    if($tmpItems){
                        foreach($tmpItems as $_item){
                            $_item->setData("rma_id", $_rma->getId());
                            $this->_child_rma_order_items[$_item->getId()] = $_item;
                        }
                    }
                }
            }
        }
        return $this->_child_rma_order_items;
    }

    /**
     * @return \Lof\Rma\Api\Data\ReturnInterface[]
     */
    public function getConditions()
    {
        return $this->dataHelper->getConditions();
    }

    /**
     * @return \Lof\Rma\Api\Data\ReturnInterface[]
     */
    public function getResolutions()
    {
        return $this->dataHelper->getResolutions();
    }

    /**
     * @return \Lof\Rma\Api\Data\ReturnInterface[]
     */
    public function getReasons()
    {
        return $this->dataHelper->getReasons();
    }

    public function getQtyAvailable($item)
    {
        return $this->dataHelper->getItemQuantityAvaiable($item);
    }
   
    public function getQtyRequest($item, $rma_id = 0)
    {
        if ($this->getCurrentRma()){
            $rma_id = $rma_id?(int)$rma_id:$this->getCurrentRma()->getId();
            return $this->dataHelper->getQtyReturnedRma($item,(int)$rma_id);
        }
    }
    

     public function getAttachmentUrl($Uid){
        $this->context->getUrlBuilder()->getUrl('rma/attachment/download',['uid' => $Uid]);
    }
}