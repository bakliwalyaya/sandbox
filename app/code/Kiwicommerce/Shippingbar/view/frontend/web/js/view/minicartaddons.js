define([
       'ko',
       'uiComponent',
       'Magento_Customer/js/customer-data',
   ], function (ko, Component, customerData) {
       'use strict';
       var subtotalAmount;
       var maxPrice = maxpriceShipping;
       var percentage;
       return Component.extend({

           displaySubtotal: ko.observable(true),
           maxprice: '$' + maxPrice.toFixed(2),
           /**
            * @override
            */
           initialize: function () {
            console.log("maxpriceShipping", maxpriceShipping);
               this._super();
               this.cart = customerData.get('cart');
           },
           getTotalCartItems: function () {
               return customerData.get('cart')().summary_count;
           },
           isFreeShippingAvailable: function () {
              
            var subtotalAmountStr = customerData.get('cart')().subtotal_excl_tax;
            
            var number_price = parseFloat(subtotalAmountStr.match(/[\d\.]+/));
            
            //subtotalAmount = customerData.get('cart')().subtotalAmount;
            subtotalAmount = number_price;
               if (subtotalAmount > maxPrice) {
                   return true;
               }
               return false;
           },
           getpercentage: function () {
               subtotalAmount = customerData.get('cart')().subtotalAmount;
               if (subtotalAmount > maxPrice) {
                   subtotalAmount = maxPrice;
               }
               percentage = ((subtotalAmount * 100) / maxPrice);
               return percentage;
           }
       });
   });