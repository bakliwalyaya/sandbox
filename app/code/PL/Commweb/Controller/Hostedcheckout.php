<?php
/**
 * PL Development.
 *
 * @category    PL
 * @author      Linh Pham <plinh5@gmail.com>
 * @copyright   Copyright (c) 2016 PL Development. (http://www.polacin.com)
 */
namespace PL\Commweb\Controller;

abstract class Hostedcheckout extends \Magento\Framework\App\Action\Action
{
    protected $mpgsHelper;

    protected $plLogger;

    protected $checkoutSession;

    protected $storeManager;

    protected $orderFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \PL\Commweb\Helper\Data $mpgsHelper,
        \PL\Commweb\Logger\Logger $plLogger
    ) {
        parent::__construct($context);
        $this->mpgsHelper = $mpgsHelper;
        $this->plLogger = $plLogger;
        $this->checkoutSession = $checkoutSession;
        $this->storeManager = $storeManager;
        $this->orderFactory = $orderFactory;
    }
}
