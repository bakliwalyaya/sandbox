<?php
/**
 * PL Development.
 *
 * @category    PL
 * @author      Linh Pham <plinh5@gmail.com>
 * @copyright   Copyright (c) 2016 PL Development. (http://www.polacin.com)
 */
namespace PL\Commweb\Block\Info;

use Magento\Framework\View\Element\Template;

/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
class HostedCheckout extends \Magento\Payment\Block\Info
{
    protected $_template = 'PL_Commweb::info/hostedcheckout.phtml';

    /**
     * @var \PL\Commweb\Helper\Data
     */
    protected $mpgsHelper;

    /**
     * HostedCheckout constructor.
     * @param \PL\Commweb\Helper\Data $mpgsHelper
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \PL\Commweb\Helper\Data $mpgsHelper,
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->mpgsHelper = $mpgsHelper;
    }
}
