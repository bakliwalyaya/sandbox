<?php

namespace Newsroom\Common\Setup;

use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface {

	/**
	 * Category setup factory
	 *
	 * @var CategorySetupFactory
	 */
	private $categorySetupFactory;

	/**
	 * Init
	 *
	 * @param CategorySetupFactory $categorySetupFactory
	 */
	public function __construct(CategorySetupFactory $categorySetupFactory) {
		$this->categorySetupFactory = $categorySetupFactory;
	}
	public function upgrade(
		ModuleDataSetupInterface $setup,
		ModuleContextInterface $context
	) {
		$setup->startSetup();
		if (version_compare($context->getVersion(), '0.0.6', '<')) {
			$categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);

			$categorySetup->addAttribute(
				'catalog_category',
				'is_resource',
				array(
					'type' => 'int',
					'label' => 'Include in Product Resources',
					'input' => 'select',
					'required' => false,
					'sort_order' => 10,
					'default' => '0',
					'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
					'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
					'group' => 'General Information',
				));
		}
	}

}