<?php
namespace Newsroom\Presskit\Api;

use Newsroom\Presskit\Api\Data\ImageInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface ImageRepositoryInterface
 *
 * @api
 */
interface ImageRepositoryInterface
{
    /**
     * Create or update a Image.
     *
     * @param ImageInterface $page
     * @return ImageInterface
     */
    public function save(ImageInterface $page);

    /**
     * Get a Image by Id
     *
     * @param int $id
     * @return ImageInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If Image with the specified ID does not exist.
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve Images which match a specified criteria.
     *
     * @param SearchCriteriaInterface $criteria
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * Delete a Image
     *
     * @param ImageInterface $page
     * @return ImageInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If Image with the specified ID does not exist.
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(ImageInterface $page);

    /**
     * Delete a Image by Id
     *
     * @param int $id
     * @return ImageInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If customer with the specified ID does not exist.
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}
