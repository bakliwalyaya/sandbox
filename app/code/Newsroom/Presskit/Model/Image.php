<?php
namespace Newsroom\Presskit\Model;

/**
 * Class Image
 */
class Image extends \Magento\Framework\Model\AbstractModel implements
    \Newsroom\Presskit\Api\Data\ImageInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'newsroom_presskit_image';

    /**
     * Init
     */
    protected function _construct() // phpcs:ignore PSR2.Methods.MethodDeclaration
    {
        $this->_init(\Newsroom\Presskit\Model\ResourceModel\Image::class);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
