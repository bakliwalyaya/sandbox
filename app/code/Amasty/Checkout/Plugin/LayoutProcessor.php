<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_Checkout
 */

namespace Amasty\Checkout\Plugin;

use Amasty\Checkout\Helper\Onepage;
use Amasty\Checkout\Model\Config;

class LayoutProcessor {
	/**
	 * @var array
	 */
	protected $orderFixes = [];

	/**
	 * @var Onepage
	 */
	private $onepageHelper;

	/**
	 * @var Config
	 */
	private $checkoutConfig;

	public function __construct(
		Onepage $onepageHelper,
		Config $checkoutConfig
	) {
		$this->onepageHelper = $onepageHelper;
		$this->checkoutConfig = $checkoutConfig;
	}

	/**
	 * @param $field
	 * @param $order
	 */
	public function setOrder($field, $order) {
		$this->orderFixes[$field] = $order;
	}

	/**
	 * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
	 * @param array $result
	 * @return array
	 */
	public function afterProcess(
		\Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
		$result
	) {
		if ($this->checkoutConfig->isEnabled()) {			

			$layoutRoot = &$result['components']['checkout']['children']['steps']['children']['shipping-step']
				['children']['shippingAddress']['children'];

			$layoutRoot['shipping-address-fieldset']['children']['region_id']['config']['additionalClasses'] = 'region-cls';
			$layoutRoot['shipping-address-fieldset']['children']['postcode']['config']['additionalClasses'] = 'postcode-cls';
			$layoutRoot['shipping-address-fieldset']['children']['company']['config']['additionalClasses'] = 'company-cls';
			$layoutRoot['shipping-address-fieldset']['children']['telephone']['config']['additionalClasses'] = 'telephone-cls';
			$layoutRoot['shipping-address-fieldset']['children']['firstname']['config']['additionalClasses'] = 'firstname-cls';
			$layoutRoot['shipping-address-fieldset']['children']['lastname']['config']['additionalClasses'] = 'lastname-cls';
			$layoutRoot['shipping-address-fieldset']['children']['street']['config']['additionalClasses'] = 'street-cls';
			$layoutRoot['shipping-address-fieldset']['children']['country_id']['config']['additionalClasses'] = 'country_id-cls';
			$layoutRoot['shipping-address-fieldset']['children']['city']['config']['additionalClasses'] = 'city-cls';
			$layoutRoot['shipping-address-fieldset']['children']['firstname']['validation']['min_text_length'] = 3;
			$layoutRoot['shipping-address-fieldset']['children']['lastname']['validation']['min_text_length'] = 3;
			
			$layoutRoot['customer-email']['component'] = 'Amasty_Checkout/js/view/form/element/email';

			foreach ($this->orderFixes as $code => $order) {
				$layoutRoot['shipping-address-fieldset']['children'][$code]['sortOrder'] = $order;
			}
		}

		return $result;
	}
}
