<?php

namespace Amasty\AdvancedReview\Model\ResourceModel\Vote;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(
            \Amasty\AdvancedReview\Model\Vote::class,
            \Amasty\AdvancedReview\Model\ResourceModel\Vote::class
        );
    }

    /**
     * @return array
     */
    public function getVoteIpKeys()
    {
        $this->getSelect()->columns('CONCAT(review_id,ip) as vote_key');

        return $this->getColumnValues('vote_key');
    }
}
