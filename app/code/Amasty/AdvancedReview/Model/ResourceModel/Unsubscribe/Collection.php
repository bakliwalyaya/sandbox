<?php

namespace Amasty\AdvancedReview\Model\ResourceModel\Unsubscribe;

use Amasty\AdvancedReview\Model\Unsubscribe;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(
            Unsubscribe::class,
            \Amasty\AdvancedReview\Model\ResourceModel\Unsubscribe::class
        );
    }
}
