<?php

namespace Amasty\AdvancedReview\Model\Sources;

class Recommend
{
    const NOT_SELECTED = 0;
    const NOT_RECOMMENDED = 2;
    const RECOMMENDED = 1;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            [
                'value' => self::NOT_RECOMMENDED,
                'label' => __('No')
            ],
            [
                'value' => self::RECOMMENDED,
                'label' => __('Yes')
            ]
        ];

        return $options;
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function getLabel($value)
    {
        foreach ($this->toOptionArray() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }

        return '';
    }
}
