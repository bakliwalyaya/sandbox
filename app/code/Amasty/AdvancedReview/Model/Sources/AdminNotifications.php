<?php

namespace Amasty\AdvancedReview\Model\Sources;

class AdminNotifications
{
    const DISABLED = 0;

    const INSTANTLY = 2;

    const DAILY = 1;

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            [
                'value' => self::DISABLED,
                'label' => __('No')
            ],
            [
                'value' => self::INSTANTLY,
                'label' => __('Yes (Instantly)')
            ],
            [
                'value' => self::DAILY,
                'label' => __('Yes (Daily)')
            ]
        ];

        return $options;
    }

    /**
     * @param $value
     *
     * @return string
     */
    public function getLabel($value)
    {
        foreach ($this->toOptionArray() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }

        return '';
    }
}
