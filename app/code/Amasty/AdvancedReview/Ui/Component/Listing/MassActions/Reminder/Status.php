<?php

namespace Amasty\AdvancedReview\Ui\Component\Listing\MassActions\Reminder;

class Status extends \Amasty\AdvancedReview\Ui\Component\Listing\MassActions\MassAction
{
    /**
     * {@inheritdoc}
     */
    public function getUrlParams($optionValue)
    {
        return ['status' => $optionValue];
    }
}
