<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_Preorder
 */


declare(strict_types=1);

namespace Amasty\Preorder\Model\Product\Detect;

use Magento\Catalog\Api\Data\ProductInterface;

interface IsProductPreorderInterface
{
    public function execute(ProductInterface $product, float $requiredQty = 1): bool;
}
