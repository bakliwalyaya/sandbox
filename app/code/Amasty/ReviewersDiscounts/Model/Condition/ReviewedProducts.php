<?php

declare(strict_types=1);

namespace Amasty\ReviewersDiscounts\Model\Condition;

use Amasty\ReviewersDiscounts\Model\ResourceModel\Review as ReviewResource;
use Magento\Backend\Model\UrlInterface;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Model\AbstractModel;
use Magento\Rule\Model\Condition\AbstractCondition;
use Magento\Rule\Model\Condition\Context;

class ReviewedProducts extends AbstractCondition
{
    /**
     * @var AbstractElement
     */
    private $valueElement;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * @var ReviewResource
     */
    private $reviewResource;

    public function __construct(
        ReviewResource $reviewResource,
        UrlInterface $urlBuilder,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->urlBuilder = $urlBuilder;
        $this->reviewResource = $reviewResource;
    }

    /**
     * @param AbstractModel $model
     * @return bool
     */
    public function validate(AbstractModel $model)
    {
        $result = false;
        $customerId = $model->getQuote() ? $model->getQuote()->getCustomerId() : $model->getCustomerId();
        if ($customerId) {
            $result = $this->validateAttribute($this->reviewResource->getReviewedProducts($customerId));
        }

        return $result;
    }

    /**
     * Make == ( IS ) condition work as () (is one of) condition
     *
     * @return array
     */
    public function getValueParsed()
    {
        $valueParsed = parent::getValueParsed();

        return is_array($valueParsed) ? $valueParsed : [$valueParsed];
    }

    /**
     * Make == ( IS ) condition work as () (is one of) condition
     *
     * @return bool
     */
    public function isArrayOperatorType()
    {
        return true;
    }

    /**
     * Render element HTML
     *
     * @return string
     */
    public function asHtml()
    {
        $this->valueElement = $this->getValueElement();

        return $this->getTypeElementHtml()
            . __(
                'Reviewed Product %1 %2',
                $this->getOperatorElementHtml(),
                $this->valueElement->getHtml()
            )
            . $this->getRemoveLinkHtml()
            . $this->getChooserContainerHtml();
    }

    /**
     * Render chooser trigger
     *
     * @return string
     */
    public function getValueAfterElementHtml()
    {
        return '<a href="javascript:void(0)" class="rule-chooser-trigger"><img src="'
            . $this->_assetRepo->getUrl(
                'images/rule_chooser_trigger.gif'
            ) . '" alt="" class="v-middle rule-chooser-trigger" title="' . __(
                'Open Chooser'
            ) . '" /></a>';
    }

    /**
     * @return string
     */
    public function getValueElementChooserUrl()
    {
        $url = 'sales_rule/promo_widget/chooser/attribute/sku';
        if ($this->getJsFormObject()) {
            $url .= '/form/' . $this->getJsFormObject();
        }

        return $this->urlBuilder->getUrl($url);
    }

    /**
     * Value element type getter
     *
     * @return string
     */
    public function getValueElementType()
    {
        return 'text';
    }

    /**
     * Specify allowed comparison operators
     *
     * @return $this
     */
    public function loadOperatorOptions()
    {
        parent::loadOperatorOptions();
        $this->setOperatorOption(
            [
                '==' => __('is'),
                '()' => __('is one of'),
            ]
        );

        return $this;
    }

    /**
     * Retrieve Explicit Apply
     *
     * @return bool
     */
    public function getExplicitApply()
    {
        return true;
    }
}
