<?php

namespace Amasty\ReviewersDiscounts\Test\Unit\Model\Condition;

use Amasty\ReviewersDiscounts\Model\Condition\ApprovedReviewsQty;
use Amasty\ReviewersDiscounts\Model\ResourceModel\Review as ReviewResource;
use Amasty\ReviewersDiscounts\Test\Unit\Traits\ObjectManagerTrait;
use Amasty\ReviewersDiscounts\Test\Unit\Traits\ReflectionTrait;
use Magento\Framework\DataObject;
use Magento\Framework\Model\AbstractModel;

/**
 * Class ApprovedReviewsQtyTest
 *
 * @see ApprovedReviewsQty
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * phpcs:ignoreFile
 */
class ApprovedReviewsQtyTest extends \PHPUnit\Framework\TestCase
{
    use ObjectManagerTrait;
    use ReflectionTrait;

    /**
     * @covers ApprovedReviewsQty::validate
     *
     * @dataProvider validateDataProvider
     *
     * @throws \ReflectionException
     */
    public function testValidate($value, $operator, $customerId, $valueForValidate, $isQuote, $expectedResult)
    {
        /** @var ApprovedReviewsQty $model */
        $model = $this->getObjectManager()->getObject(ApprovedReviewsQty::class);
        $model->setData('value', $value);
        $model->setData('operator', $operator);

        $modelForValidate = $this->createPartialMock(AbstractModel::class, ['getCustomerId', 'getQuote']);
        if ($isQuote) {
            $quote = $this->getObjectManager()->getObject(DataObject::class, ['data' => ['customer_id' => $customerId]]);
            $modelForValidate->expects($this->any())->method('getQuote')->willReturn($quote);
        } else {
            $modelForValidate->expects($this->any())->method('getCustomerId')->willReturn($customerId);
        }

        $reviewResource = $this->createPartialMock(ReviewResource::class, ['getTotalReviews']);
        $reviewResource->expects($this->any())->method('getTotalReviews')->willReturn($valueForValidate);

        $this->setProperty($model, 'reviewResource', $reviewResource, ApprovedReviewsQty::class);

        $this->assertEquals($expectedResult, $model->validate($modelForValidate));
    }

    /**
     * Data provider for validate test
     * @return array
     */
    public function validateDataProvider()
    {
        return [
            [
                'value' => 1,
                'operator' => '==',
                'customer_id' => 1,
                'total_reviews' => 1,
                'is_quote' => true,
                'expected_result' => true
            ],
            [
                'value' => 1,
                'operator' => '==',
                'customer_id' => 1,
                'total_reviews' => 1,
                'is_quote' => false,
                'expected_result' => true
            ],
            [
                'value' => 12,
                'operator' => '>',
                'customer_id' => 1,
                'total_reviews' => 11,
                'is_quote' => true,
                'expected_result' => false
            ],
            [
                'value' => 12,
                'operator' => '<',
                'customer_id' => 1,
                'total_reviews' => 1,
                'is_quote' => true,
                'expected_result' => true
            ],
            [
                'value' => 12,
                'operator' => '>=',
                'customer_id' => 1,
                'total_reviews' => 12,
                'is_quote' => true,
                'expected_result' => true
            ],
            [
                'value' => 12,
                'operator' => '<=',
                'customer_id' => 1,
                'total_reviews' => 12,
                'is_quote' => true,
                'expected_result' => true
            ],
            [
                'value' => 11,
                'operator' => '<=',
                'customer_id' => 1,
                'total_reviews' => 12,
                'is_quote' => true,
                'expected_result' => false
            ],
            [
                'value' => 11,
                'operator' => '>=',
                'customer_id' => null,
                'total_reviews' => 12,
                'is_quote' => true,
                'expected_result' => false
            ]
        ];
    }
}
