<?php

declare(strict_types=1);

namespace Amasty\ReviewersDiscounts\Observer;

use Amasty\ReviewersDiscounts\Model\Condition\ApprovedReviewsQty;
use Amasty\ReviewersDiscounts\Model\Condition\ReviewedProducts;
use Amasty\ReviewersDiscounts\Model\ConfigProvider;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class AddRuleConditions implements ObserverInterface
{
    /**
     * @var ConfigProvider
     */
    private $configProvider;

    public function __construct(ConfigProvider $configProvider)
    {
        $this->configProvider = $configProvider;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if ($this->configProvider->getIsRulesEnabled()) {
            $additional = $observer->getEvent()->getAdditional();
            $conditions = (array)$additional->getConditions();
            $conditions = array_merge_recursive(
                $conditions,
                [
                    [
                        'label' => __('Reviewers Discounts'),
                        'value' => [
                            [
                                'label' => __('Approved Reviews Qty'),
                                'value' => ApprovedReviewsQty::class
                            ],
                            [
                                'label' => __('Reviewed Products'),
                                'value' => ReviewedProducts::class
                            ]
                        ]
                    ],
                ]
            );
            $additional->setConditions($conditions);
        }
    }
}
