<?php
/**
 * Copyright © 2016 MageWorx. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace MageWorx\Downloads\Block\Catalog\Product;

use Magento\Customer\Model\Context;
use Magento\Framework\View\Element\Template;
use MageWorx\Downloads\Helper\Data as HelperData;
use MageWorx\Downloads\Model\Attachment;
use MageWorx\Downloads\Model\Attachment\Product as AttachmentProduct;

class Attachments extends Template {
	/**
	 * Core registry
	 *
	 * @var \Magento\Framework\Registry
	 */
	protected $coreRegistry;

	/**
	 *
	 * @var \MageWorx\Downloads\Model\Attachment\Product
	 */
	protected $attachmentProduct;

	/**
	 *
	 * @var \MageWorx\Downloads\Helper\Data
	 */
	protected $helperData;

	/**
	 * @var \Magento\Framework\App\Http\Context
	 */
	protected $httpContext;

	/**
	 *
	 * @var \MageWorx\Downloads\Model\ResourceModel\Attachment\Collection
	 */
	protected $attachmentCollection;

	/**
	 *
	 * @var boolean
	 */
	protected $isHasNotAllowedLinks;

	/**
	 *
	 * @var array
	 */
	protected $attachments = [];

	/**
	 *
	 * @param \Magento\Framework\View\Element\Template\Context $context
	 * @param \Magento\Framework\App\Http\Context $httpContext
	 * @param \Magento\Framework\Registry $registry
	 * @param HelperData $helperData
	 * @param AttachmentProduct $attachmentProduct
	 * @param array $data
	 */
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Framework\App\Http\Context $httpContext,
		\Magento\Framework\Registry $registry,
		HelperData $helperData,
		AttachmentProduct $attachmentProduct,
		array $data = []
	) {
		$this->httpContext = $httpContext;
		$this->coreRegistry = $registry;
		$this->attachmentProduct = $attachmentProduct;
		$this->helperData = $helperData;
		parent::__construct($context, $data);
		$this->setTabTitle();
	}

	/**
	 * @param boolean $isUseCustomerGroupFilter
	 * @return \MageWorx\Downloads\Model\ResourceModel\Attachment\Collection
	 */
	public function getAttachmentCollection($isUseCustomerGroupFilter = true, $productId = 0) {
		if (!$productId) {
			$productId = $this->getProductId();
		}
		$collection = $this->attachmentProduct->getSelectedAttachmentsCollection($productId);
		$collection->addFieldToFilter('is_active', Attachment::STATUS_ENABLED);
		$collection->addFieldToFilter('section_table.is_active', \MageWorx\Downloads\Model\Section::STATUS_ENABLED);
		$collection->addStoreFilter($this->_storeManager->getStore()->getId());
		if ($isUseCustomerGroupFilter) {
			$collection->addCustomerGroupFilter($this->getCustomerGroupId());
		}
		$collection->setOrder('section_id', 'DESC');
		$collection->load();
		$this->attachmentCollection = $collection;
		return $this->attachmentCollection;
	}

	/**
	 * Get customer group id
	 *
	 * @return int
	 */
	protected function getCustomerGroupId() {
		$customerGroupId = (int) $this->getRequest()->getParam('cid');
		if ($customerGroupId == null) {
			$customerGroupId = $this->httpContext->getValue(Context::CONTEXT_GROUP);
		}
		return $customerGroupId;
	}

	/**
	 *
	 * @return string
	 */
	public function getTitle() {
		return $this->helperData->getProductDownloadsTitle();
	}

	/**
	 * Retrieve array of attachment object that allow for view
	 *
	 * @return array
	 */
	public function getAttachments() {
		if (!$this->attachments) {
			if ($this->helperData->isHideFiles()) {
				$collection = $this->getAttachmentCollection(true);
				$inGroupIds = $collection->getAllIds();
			} else {
				$collection = $this->getAttachmentCollection(false);
				$inGroupIds = $this->getAttachmentCollection(true)->getAllIds();
			}

			foreach ($collection->getItems() as $item) {
				if (!$this->isAllowByCount($item)) {
					continue;
				}

				if ($this->isAllowByCustomerGroup($item, $inGroupIds)) {
					$item->setIsInGroup('1');
				} else {
					$this->isHasNotAllowedLinks = true;
				}

				$this->attachments[] = $item;
			}
		}

		return $this->attachments;
	}

	/**
	 *
	 * @param \MageWorx\Downloads\Model\Attachment $item
	 * @return boolean
	 */
	public function isAllowByCount($item) {
		$limit = $item->getDownloadsLimit();
		if ($limit) {
			if ($item->getDownloads() >= $limit) {
				return false;
			}
		}
		return true;
	}

	/**
	 *
	 * @param \MageWorx\Downloads\Model\Attachment $item
	 * @param array $inGroupIds
	 * @return boolean
	 */
	protected function isAllowByCustomerGroup($item, $inGroupIds) {
		return in_array($item->getId(), $inGroupIds);
	}

	/**
	 * Retrieve structured by sections list of attachments object that allow for view
	 *
	 * @return array
	 */
	public function getGroupAttachments() {
		$attachments = $this->getAttachments();
		$grouped = [];

		foreach ($attachments as $attachment) {
			$grouped[$attachment->getSectionId()]['attachments'][] = $attachment;
			$grouped[$attachment->getSectionId()]['title'] = $attachment->getSectionName();
		}

		return $grouped;
	}

	/**
	 * Get current product id
	 *
	 * @return null|int
	 */
	public function getProductId() {
		$product = $this->coreRegistry->registry('product');
		return $product ? $product->getId() : null;
	}

	/**
	 * @return $this
	 */
	protected function _prepareLayout() {
		$template = $this->getLayout()->createBlock('Magento\Framework\View\Element\Template');
		$template->setFragment('catalog.product.list.mageworx.downloads.attachments');
		return parent::_prepareLayout();
	}

	/**
	 * @return $this
	 */
	public function setTabTitle() {
		$tabTitle = $this->helperData->getProductDownloadsTabTitle();
		$title = __($tabTitle) . '&nbsp;' . '<span class="counter">' .
		count($this->getAttachments()) . '</span>';
		$this->setTitle($title);
		return $this;
	}

	/**
	 *
	 * @param \MageWorx\Downloads\Model\Attachment $attachment
	 * @return string
	 */
	public function getAttachmentHtml($attachment) {
		$block = $this->getLayout()->createBlock('MageWorx\Downloads\Block\Catalog\Product\Link')
			->setTemplate('MageWorx_Downloads::catalog/product/attachment_link.phtml');
		$block->setData('item', $attachment);

		return $block->toHtml();
	}
	/**
	 *
	 * @param \MageWorx\Downloads\Model\Attachment $attachment
	 * @return string
	 */
	public function getCustomAttachmentHtml($attachment) {
		$block = $this->getLayout()->createBlock('MageWorx\Downloads\Block\Catalog\Product\Link')
			->setTemplate('Magento_Catalog::product/attachment_link.phtml');
		$block->setData('item', $attachment);

		return $block->toHtml();
	}
	/**
	 *
	 * @param \MageWorx\Downloads\Model\Attachment $attachment
	 * @return string
	 */
	public function getAttachmentLinkHtml($attachment) {
		$block = $this->getLayout()->createBlock('MageWorx\Downloads\Block\Catalog\Product\Link')
			->setTemplate('MageWorx_Downloads::catalog/product/attachment_link_specsheet.phtml');
		$block->setData('item', $attachment);

		return $block->toHtml();
	}

	/**
	 *
	 * @return boolean
	 */
	public function isGroupBySection() {
		return $this->helperData->isGroupBySection();
	}

	public function isShowHowToDownloadMessage() {
		return $this->isHasNotAllowedLinks && $this->getHowToDownloadMessage();
	}

	public function getHowToDownloadMessage() {
		return $this->helperData->getHowToDownloadMessage();
	}
}
