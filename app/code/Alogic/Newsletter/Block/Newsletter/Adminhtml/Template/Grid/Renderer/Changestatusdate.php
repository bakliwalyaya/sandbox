<?php
namespace Alogic\Newsletter\Block\Newsletter\Adminhtml\Template\Grid\Renderer;
use Magento\Framework\DataObject;

class Changestatusdate extends \Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer {
	public function render(\Magento\Framework\DataObject $row) {
			return ($row->getData('change_status_at') != '' ? $row->getData('change_status_at') : '----');
	}
}