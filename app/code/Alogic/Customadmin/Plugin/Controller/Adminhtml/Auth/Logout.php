<?php
/**
 * DISCLAIMER
 *
 * It will impact on all ui grid past search.
 *
 * @author    vikas kalal
 * @package   General
 */

namespace Alogic\Customadmin\Plugin\Controller\Adminhtml\Auth;

use Magento\Backend\Model\Auth\Session;

class Logout {
	const BOOKMARK_NAMESPACE = 'current';

	/**
	 * @var \Magento\Ui\Model\BookmarkFactory $bookmarkFactory
	 */
	private $bookmarkFactory;

	/**
	 * @param \Magento\Ui\Model\BookmarkFactory $bookmarkFactory
	 * @param Session $authSession
	 */
	public function __construct(
		\Magento\Ui\Model\BookmarkFactory $bookmarkFactory,
		Session $authSession
	) {
		$this->bookmarkFactory = $bookmarkFactory;
		$this->authSession = $authSession;
	}

	/**
	 * Before Admin Logout Plugin
	 *
	 * @param  \Magento\Backend\Controller\Adminhtml\Auth\Logout $subject
	 * @return void
	 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
	 */
	public function beforeExecute(
		\Magento\Backend\Controller\Adminhtml\Auth\Logout $subject
	) {
		$currentAdminUserId = $this->getCurrentUserId() ?? '';

		if ($currentAdminUserId != '') {
			$bookmarkCollection = $this->bookmarkFactory->create()->getCollection();
			$bookmarkCollection->addFieldToFilter('identifier', self::BOOKMARK_NAMESPACE);
			$bookmarkCollection->addFieldToFilter('user_id', $currentAdminUserId);

			$bookmarkCollection->walk('delete');
		}
	}

	/**
	 * Get Current Admin User
	 *
	 * @return int
	 */
	public function getCurrentUserId() {
		return $this->authSession->getUser()->getId();
	}
}