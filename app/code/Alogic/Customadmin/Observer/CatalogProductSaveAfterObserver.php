<?php

namespace Alogic\Customadmin\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class CatalogProductSaveAfterObserver implements ObserverInterface
{
    /**
     * @var \Magento\SalesRule\Model\RuleFactory
     */
    protected $ruleFactory;
    /**
     * @var \Magento\SalesRule\Model\Converter\ToDataModel
     */
    protected $toDataModelConverter;
    /**
     * @param \Magento\SalesRule\Model\RuleFactory $ruleFactory
     * @param \Magento\Framework\Serialize\Serializer\Json $serializer
     * @param \Magento\SalesRule\Model\Converter\ToDataModel $toDataModelConverter
     */
    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     * @since 100.2.0
     */
    protected $serializer;

    public function __construct(
        \Magento\SalesRule\Model\RuleFactory $ruleFactory,
        \Magento\SalesRule\Model\Converter\ToDataModel $toDataModelConverter,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null
    ) {
        $this->ruleFactory = $ruleFactory;
        $this->toDataModelConverter = $toDataModelConverter;
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()->get(
            \Magento\Framework\Serialize\Serializer\Json::class
        );

    }

    public function removeItemFromOtherRules($sku, $ruleId)
    {

        $ruleCollection = $this->ruleFactory->create()->getCollection()
            ->addFieldToFilter('product_rule', 1)
            ->addFieldToFilter('rule_id', ['neq' => $ruleId]);
        foreach ($ruleCollection as $rule) {

            $couponCode = $rule->getCode();

            $ruleActions = $rule->getActions()->asArray();

            if (isset($ruleActions['conditions'])) {
                if (isset($ruleActions['conditions'][0]['attribute'])
                    && $ruleActions['conditions'][0]['attribute'] == 'sku'
                    && $ruleActions['conditions'][0]['operator'] == '()'
                    && $couponCode
                ) {
                    if (isset($ruleActions['conditions'][0]['value'])) {
                        $skus = explode(',', $ruleActions['conditions'][0]['value']);
                        if (($key = array_search($sku, $skus)) !== false) {
                            unset($skus[$key]);
                        }
                        $ruleData = $rule->getData();
                        $ruleActions['conditions'][0]['value'] = trim(implode(",", $skus), ',');
                        $rule->setActionsSerialized($this->serializer->serialize($ruleActions));
                        //$rule->setCode($couponCode);
                        $rule->setCode($couponCode);
                        $rule->setCouponCode($couponCode);
                        $rule->setActions(null);
                        $rule->save();

                    }
                }

            }
        }

    }

    public function execute(EventObserver $observer)
    {
        try {
            $product = $observer->getEvent()->getProduct();
            if ($product->getId()) {
                if ($ruleId = $product->getProductRules()) {
                    $sku = $product->getSku();
                    $model = $this->ruleFactory->create()->load($ruleId);
                    $this->removeItemFromOtherRules($sku, $ruleId);
                    $ruleActions = $model->getActions()->asArray();


                    if (isset($ruleActions['conditions'])) {
                        $conditions = $ruleActions['conditions'];
                        $skus = explode(',', $ruleActions['conditions'][0]['value']);
                        $skus[] = $sku;
                        $skus = implode(',', array_unique($skus));
                        $ruleActions['conditions'][0]['value'] = trim($skus, ',');
                        $model->setActionsSerialized($this->serializer->serialize($ruleActions));
                        $model->setActions(null);
                        $model->save();
                    } else {
                        $conditions = [
                            [
                                'type' => 'Magento\SalesRule\Model\Rule\Condition\Product',
                                'attribute' => 'sku',
                                'operator' => '()',
                                'value' => $sku
                            ]

                        ];
                        $ruleActions['conditions'] = $conditions;
                        $model->setActionsSerialized($this->serializer->serialize($ruleActions));
                        $model->setActions(null);
                        $model->save();
                    }

                }

            }
        } catch (\Exception $e) {

        }
    }
}
