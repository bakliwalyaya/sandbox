<?php

namespace Alogic\Customadmin\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Type
 * @package Mageplaza\BannerSlider\Model\Config\Source
 */
class Notify implements ArrayInterface {
	const NO = '0';
	const YES = '1';

	/**
	 * to option array
	 *
	 * @return array
	 */
	public function toOptionArray() {
		$options = [
			[
				'value' => self::NO,
				'label' => __('No'),
			],
			[
				'value' => self::YES,
				'label' => __('Yes'),
			],
		];

		return $options;
	}
}
