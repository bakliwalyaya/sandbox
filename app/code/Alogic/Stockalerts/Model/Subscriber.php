<?php
namespace Alogic\Stockalerts\Model;
class Subscriber extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'alogic_stockalerts_subscriber';

    protected $_cacheTag = 'alogic_stockalerts_subscriber';

    protected $_eventPrefix = 'alogic_stockalerts_subscriber';

    protected function _construct()
    {
        $this->_init('Alogic\Stockalerts\Model\ResourceModel\Subscriber');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}

