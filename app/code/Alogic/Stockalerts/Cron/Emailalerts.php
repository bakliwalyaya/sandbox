<?php
namespace Alogic\Stockalerts\Cron;

use Magento\Framework\App\Area;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\InventoryCatalog\Model\GetStockIdForCurrentWebsite;
use Magento\InventorySalesApi\Model\GetStockItemDataInterface;
use Magento\InventorySales\Model\GetProductSalableQty;
use Magento\InventorySales\Model\ResourceModel\GetAssignedStockIdForWebsite;
use Magento\ProductAlert\Block\Email\Stock;
use Magento\ProductAlert\Helper\Data;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

class Emailalerts {
	private $getAssignedStockIdForWebsite;
	public $websiteRepository;
	protected $_productRepository;
	protected $logger;
	protected $_subscriberFactory;
	protected $transportBuilder;
	protected $storeManager;
	protected $inlineTranslation;
	/**
	 * @var \Magento\Framework\App\State
	 */
	protected $_appState;
	/**
	 * @var Emulation
	 */
	protected $_appEmulation;
	/**
	 * @var GetProductSalableQty
	 */
	private $getProductSalableQty;

	/**
	 * @var GetStockIdForCurrentWebsite
	 */
	private $getStockIdForCurrentWebsite;

	/**
	 * @var GetStockItemDataInterface
	 */
	private $getStockItemData;
	protected $_stockBlock;
	protected $_productAlertData = null;

	public function __construct(LoggerInterface $logger,
		\Alogic\Stockalerts\Model\SubscriberFactory $subscriberFactory,
		GetProductSalableQty $getProductSalableQty,
		GetStockIdForCurrentWebsite $getStockIdForCurrentWebsite,
		\Magento\Catalog\Model\ProductRepository $productRepository,
		GetAssignedStockIdForWebsite $getAssignedStockIdForWebsite,
		WebsiteRepositoryInterface $websiteRepository,
		GetStockItemDataInterface $getStockItemData,
		TransportBuilder $transportBuilder,
		StoreManagerInterface $storeManager,
		Emulation $appEmulation,
		Data $productAlertData,
		\Magento\Framework\App\State $_appState

	) {
		$this->logger = $logger;
		$this->_subscriberFactory = $subscriberFactory;
		$this->getProductSalableQty = $getProductSalableQty;
		$this->getStockIdForCurrentWebsite = $getStockIdForCurrentWebsite;
		$this->_productRepository = $productRepository;
		$this->getAssignedStockIdForWebsite = $getAssignedStockIdForWebsite;
		$this->websiteRepository = $websiteRepository;
		$this->transportBuilder = $transportBuilder;
		$this->storeManager = $storeManager;
		$this->_appEmulation = $appEmulation;

		$this->_productAlertData = $productAlertData;
		$this->_appState = $_appState;
		$this->getStockItemData = $getStockItemData;
	}

	/**
	 * Write to system.log
	 *
	 * @return void
	 */
	public function execute() {

		$subscriber = $this->_subscriberFactory->create();
		$collection = $subscriber->getCollection();
		foreach ($collection as $item) {
			$this->getStock($item);
		}
		//$this->logger->info('Cron Works');
	}
	public function getStock($item) {

		$product = $this->getProductById($item->getProductId());

		$website_id = $item->getWebsiteId();
		$website_code = $item->getWebsiteCode();
		if (!$website_code) {
			return;
		}

		//$this->logger->info('new website code ' . $website_code);
		$stockId = $this->getStockIdForWebsite($website_code);

		$productQtyInStock = $this->getProductSalableQty->execute($product->getSku(), $stockId);
		$isInStock = $productQtyInStock ? 1 : 0;
		$qtyAsked = $item->getQty();
		//$this->logger->info('isInStock =>' . $isInStock);
		//$this->logger->info('qtyAsked =>' . $qtyAsked);
		if ($isInStock && $productQtyInStock >= $qtyAsked) {
			if ($item->getSendCount() == 0) {
				$this->notifyCustomer($item, $product);
				$item->setSendCount(1);
				$item->save();
			}

		}

	}
	private function getStore(int $storeId) {
		return $this->storeManager->getStore($storeId);
	}
	protected function _getStockBlock() {
		if ($this->_stockBlock === null) {
			$this->_stockBlock = $this->_productAlertData->createBlock(Stock::class);
		}
		return $this->_stockBlock;
	}
	public function getProductHtml($item, $product) {
		return $html = '<table
    width="100%"
    style="width:100% !important;"
    cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td   style="font-size: 16px; line-height: 26px; color: #000;font-weight:bold;">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; border-collapse: collapse !important;">
                <tr style="background-color: #fff; margin-top:40px;border-bottom:1px solid #707070;padding-bottom:30px;margin-bottom:0;"

                    class="mteditor-bgcolor table-head-bg">
                    <td height="45"  width="2%">&nbsp;</td>
                    <td style="text-align: left">
                        Product Name
                    </td>
                    <td style="text-align: left">
                        Qty Requested
                    </td>
                </tr>
                <tr style="background-color: #fff;"
                    class="mteditor-bgcolor table-head-bg">
                    <td height="45"  width="2%">&nbsp;</td>
                    <td style="text-align: left">' . $product->getName() . '
                        <br />
                        ' . $product->getSku() . '
                    </td>
                    <td style="text-align: left">
                    ' . $item->getQty() . '
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="" style="color:#fff; background-color: #000; height:45px; line-height:45px; padding: 0 25px; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;text-align:center; width:30%;display: inline-block;margin: 50px 0;" data-var-style="button1_style" bgcolor="#000" data-var-bgcolor="button1_bgcolor" align="center" height="45">
                        <a class="mteditor-color button-text-color mteditor-content-helper-link" contenteditable="true" data-var-text="button1_text" style="font-family: Helvetica; font-weight: normal; color: #ffffff !important; font-size: 15px; text-decoration: none !important; display: inline-block;text-align:center;" data-var-style="button1_text_style" href="' . $product->getProductUrl() . '" data-var-href="button1_href" target="_blank" data-disable-remove="1">
                            LEARN MORE                        </a>
                    </td>
    </tr>
</table>

';
	}
	public function notifyCustomer($item, $product) {
		$storeId = $item->getStoreId();
		$store = $this->getStore($storeId);
		$this->_appEmulation->startEnvironmentEmulation($storeId);

		$block = $this->_getStockBlock();
		$block->setStore($store)->reset();
		$block->addProduct($product);
		$productHtml = $this->getProductHtml($item, $product);
		$alertGrid = $this->_appState->emulateAreaCode(
			Area::AREA_FRONTEND,
			[$block, 'toHtml']
		);
		// this is an example and you can change template id,fromEmail,toEmail,etc as per your need.
		$templateId = 41; // template id
		$fromEmail = 'weborders@alogic.co'; // sender Email id
		$fromName = 'Admin'; // sender Name
		$toEmail = $item->getEmail(); // receiver email id

		try {
			// template variables pass here
			$templateVars = [
				'customerName' => $item->getFirstname(),
				'store_email' => 'store@email.com',
				'alertGrid' => $alertGrid,
				'sku' => $product->getSku(),
				'qty' => $item->getQty(),
				'html' => $productHtml,
			];

			$storeId = $this->storeManager->getStore()->getId();

			$from = ['email' => $fromEmail, 'name' => $fromName];
			//$this->inlineTranslation->suspend();

			$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
			$templateOptions = [
				'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
				'store' => $storeId,
			];
			$transport = $this->transportBuilder->setTemplateIdentifier($templateId, $storeScope)
				->setTemplateOptions($templateOptions)
				->setTemplateVars($templateVars)
				->setFrom($from)
				->addTo($toEmail)
				->getTransport();
			$transport->sendMessage();
			//$this->inlineTranslation->resume();
		} catch (\Exception $e) {
			$this->_logger->info($e->getMessage());
		}

		//$this->logger->info('notifying =>' . $item->getEmail());
	}
	public function getProductById($id) {
		return $this->_productRepository->getById($id);
	}
	public function getStockIdForWebsite($website_code) {

		//$this->logger->info('Website Code=>' . $website_code);
		return $this->getAssignedStockIdForWebsite->execute($website_code);
	}
	public function getWebsiteCode($websiteId) {

		try {

			$website = $this->websiteRepository->getById($websiteId);

			return $websiteCode = $website->getCode();
		} catch (Exception $exception) {
			$this->logger->error($exception->getMessage());
		}
	}
}
