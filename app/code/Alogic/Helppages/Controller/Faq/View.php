<?php
namespace Alogic\Helppages\Controller\Faq;
class View extends \Magento\Framework\App\Action\Action {

	protected $resultPageFactory;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory) {
		$this->resultPageFactory = $resultPageFactory;
		parent::__construct($context);
	}

	public function execute() {
		$resultPageFactory = $this->resultPageFactory->create();
		/** @var Template $block */
		$block = $resultPageFactory->getLayout()->getBlock('alogic_faqview_block_main');
		$id = $this->getFaqId();
		if ($id) {
			try {
				$model = $this->_objectManager->create('MW\EasyFaq\Model\Faq')->load($id);
			} catch (\Magento\Framework\Exception\NoSuchEntityException $exception) {
				$this->messageManager->addError(__('This FAQ no longer exists.'));
				$this->_redirect('*/*/*');
				return;
			}
		} else {
			/** @var \MW\EasyFaq\Model\Faq $model */
			$model = $this->_objectManager->create('MW\EasyFaq\Model\Faq');
		}
		$block->setData('item', $model);
		// Add page title
		$resultPageFactory->getConfig()->getTitle()->set(__('Help'));

		return $resultPageFactory;

	}
	protected function getFaqId() {
		return $this->getRequest()->getParam('id');
	}
}
