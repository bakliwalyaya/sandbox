<?php
namespace Alogic\Productregistration\Controller\Index;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\ScopeInterface;
use \Twilio\Rest\Client as TwilioClient;

class Sendotp extends \Magento\Framework\App\Action\Action {
	const XML_PATH_REGISTRATION = 'prod_registration/';
	const MODULE_ACCOUNT_ID_CONFIG_PATH = 'alogic_product_registration/general/account_sid';
	const MODULE_AUTH_TOKEN_CONFIG_PATH = 'alogic_product_registration/general/auth_token';
	const MODULE_SERVICE_ID_CONFIG_PATH = 'alogic_product_registration/general/service_id';
	const MODULE_TWILIO_INDIA_CONFIG_PATH = 'alogic_product_registration/general/twilio_no_india';
	protected $resultPageFactory;
	protected $_resultJsonFactory;
	protected $productCollectionFactory;
	protected $_storeManager;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		ScopeConfigInterface $scopeConfig,
		JsonFactory $resultJsonFactory) {
		$this->_resultJsonFactory = $resultJsonFactory;
		$this->productCollectionFactory = $productCollectionFactory;
		$this->resultPageFactory = $resultPageFactory;
		$this->_storeManager = $storeManager;
		$this->scopeConfig = $scopeConfig;
		parent::__construct($context);
	}
	public function getStoreId() {
		return $this->_storeManager->getStore()->getId();
	}
	public function execute() {
		$result = $this->_resultJsonFactory->create();
		$data = $this->getRequest()->getParams();
// Your Account SID and Auth Token from twilio.com/console
		$account_sid = $this->scopeConfig->getValue(self::MODULE_ACCOUNT_ID_CONFIG_PATH, ScopeInterface::SCOPE_STORE,
			$this->getStoreId());
		$auth_token = $this->scopeConfig->getValue(self::MODULE_AUTH_TOKEN_CONFIG_PATH, ScopeInterface::SCOPE_STORE,
			$this->getStoreId());
		$serviceId = $this->scopeConfig->getValue(self::MODULE_SERVICE_ID_CONFIG_PATH, ScopeInterface::SCOPE_STORE,
			$this->getStoreId());
		$twilio_number = $this->scopeConfig->getValue(self::MODULE_TWILIO_INDIA_CONFIG_PATH, ScopeInterface::SCOPE_STORE,
			$this->getStoreId());

		$client = new TwilioClient($account_sid, $auth_token);

		$verification = $client->verify->v2->services($serviceId)
			->verifications
			->create('+91' . $data['phone'], "sms");

		if ($verification->status) {
			//echo $verification_check->status;
			//die();
		}
		$result->setData(['output' => $verification->status]);
		return $result;

		// return $this->resultPageFactory->create();
	}

}
