<?php

namespace Alogic\Customtheme\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper {

	const XML_PATH_ZENDESK = 'zendesk_ticket/';
	const MODULE_SHOW_CART_CONFIG_PATH = 'alogic_store_switch/general/disable_cart';
	protected $storeManager;

	protected $scopeConfig;
	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		ScopeConfigInterface $scopeConfig,
		array $data = []
	) {
		$this->scopeConfig = $scopeConfig;
		$this->storeManager = $storeManager;

	}
	public function getStore() {
		return $this->storeManager->getStore();
	}

	public function getIsCartDisabled() {
		return $this->scopeConfig->getValue(self::MODULE_SHOW_CART_CONFIG_PATH, ScopeInterface::SCOPE_STORE,
			$this->getStoreId());
	}
	public function getStoreId() {
		return $this->storeManager->getStore()->getId();
	}
	public function getConfigValue($field, $storeId = null) {
		return $this->scopeConfig->getValue(
			$field, ScopeInterface::SCOPE_STORE, $storeId
		);
	}

	public function getGeneralConfig($code, $storeId = null) {

		return $this->getConfigValue(self::XML_PATH_ZENDESK . 'general/' . $code, $storeId);
	}

}
