

require(["jquery", "customtheme/jqueryvisible"], function ($) {
    $(document).ready(function ($) {
        //check if the .cd-image-container is in the viewport
        //if yes, animate itlet windowPort = window.outerWidth;
        let windowPort = window.outerWidth;

        checkPosition($('.cd-image-container'));
        $(window).on('scroll', function(){
            checkPosition($('.cd-image-container'));
        });

        //make the .cd-handle element draggable and modify .cd-resize-img width according to its position
        drags($('.cd-handle'), $('.cd-resize-img'), $('.cd-image-container'), $('.cd-image-label[data-type="original"]'), $('.cd-image-label[data-type="modified"]'));

        //upadate images label visibility
        $(window).on('resize', function () {
            updateLabel($('.cd-image-label[data-type="modified"]'), $('.cd-resize-img'), 'left');
            updateLabel($('.cd-image-label[data-type="original"]'), $('.cd-resize-img'), 'right');
        });

        function checkPosition(container) {
          if ($('.image-converstion').length >= 1) {
            if( $(window).scrollTop() + $(window).height()*0.5 > container.offset().top) {
                container.addClass('is-visible');
            }
          }
        }

        // Sticky Mobile Header
        if (windowPort < 768) {

          $(function() {
          	createSticky(jQuery(".enabled-header-sticky"));
          });

          function createSticky(sticky) {

          	if (typeof sticky !== "undefined") {

          		var	pos = sticky.offset().top,
          				win = jQuery(window);

          		win.on("scroll", function() {
              		win.scrollTop() >= pos ? sticky.addClass("enable-sticky") : sticky.removeClass("enable-sticky");
          		});
          	}
          }
        }

        // Sticky add to cart button for Mobile
        // if (windowPort < 768) {
        //   if ($('.primary.addcart-link').scrollTop() > 30) {
        //     $('.product-options-bottom .actions').addClass('sticky-addtocart');
        //   } else {
        //     $('.product-options-bottom .actions').removeClass('sticky-addtocart');
        //   }
        // }

      });

    //draggable funtionality - credits to https://css-tricks.com/snippets/jquery/draggable-without-jquery-ui/
    function drags(dragElement, resizeElement, container, labelContainer, labelResizeElement) {
    dragElement.on("mousedown vmousedown", function(e) {
        dragElement.addClass('draggable');
        resizeElement.addClass('resizable');

        let dragWidth = dragElement.outerWidth(),
            xPosition = dragElement.offset().left + dragWidth - e.pageX,
            containerOffset = container.offset().left,
            containerWidth = container.outerWidth(),
            minLeft = containerOffset + 10,
            maxLeft = containerOffset + containerWidth - dragWidth - 10;

        dragElement.parents().on("mousemove vmousemove", function(e) {
            leftValue = e.pageX + xPosition - dragWidth;

            //constrain the draggable element to move inside his container
            if(leftValue < minLeft ) {
                leftValue = minLeft;
            } else if ( leftValue > maxLeft) {
                leftValue = maxLeft;
            }

            widthValue = (leftValue + dragWidth/2 - containerOffset)*100/containerWidth+'%';

            $('.draggable').css('left', widthValue).on("mouseup vmouseup", function() {
                $(this).removeClass('draggable');
                resizeElement.removeClass('resizable');
            });

            $('.resizable').css('width', widthValue);

            updateLabel(labelResizeElement, resizeElement, 'left');
            updateLabel(labelContainer, resizeElement, 'right');

        }).on("mouseup vmouseup", function(e){
            dragElement.removeClass('draggable');
            resizeElement.removeClass('resizable');
        });
        e.preventDefault();
      }).on("mouseup vmouseup", function(e) {
          dragElement.removeClass('draggable');
          resizeElement.removeClass('resizable');
      });
    }

    function updateLabel(label, resizeElement, position) {
      if(position == 'left') {
          ( label.offset().left + label.outerWidth() < resizeElement.offset().left + resizeElement.outerWidth() ) ? label.removeClass('is-hidden') : label.addClass('is-hidden') ;
      } else {
          ( label.offset().left > resizeElement.offset().left + resizeElement.outerWidth() ) ? label.removeClass('is-hidden') : label.addClass('is-hidden') ;
      }
    }

});
