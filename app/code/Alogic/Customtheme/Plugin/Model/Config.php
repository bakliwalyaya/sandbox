<?php

namespace Alogic\Customtheme\Plugin\Model;
use Magento\Store\Model\StoreManagerInterface;

class Config {
	protected $_storeManager;

	public function __construct(
		StoreManagerInterface $storeManager
	) {
		$this->_storeManager = $storeManager;
	}

	public function afterGetAttributeUsedForSortByArray(\Magento\Catalog\Model\Config $catalogConfig, $options) {
		//$customOption['newest_product'] = __('New to Old');
		//$customOption['low_to_high'] = __('Price - Low To High');
		//$customOption['high_to_low'] = __('Price - High To Low');
		//$options = array_merge($customOption, $options);
		unset($options['created_at']);
		return $options;
	}
}