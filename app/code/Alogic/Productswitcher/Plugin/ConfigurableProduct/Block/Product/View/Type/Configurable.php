<?php
namespace Alogic\Productswitcher\Plugin\ConfigurableProduct\Block\Product\View\Type;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\InventoryApi\Api\Data\SourceItemInterface;
use Magento\InventoryApi\Api\SourceItemRepositoryInterface;
use Psr\Log\LoggerInterface;

class Configurable {

	protected $_stockItemRepository;
	/**
	 * @var SearchCriteriaBuilder
	 */
	private $searchCriteriaBuilder;

	/**
	 * @var SourceItemRepositoryInterface
	 */
	private $sourceItemRepository;
	protected $_storeManagerInterface;
	/**
	 * @var LoggerInterface
	 */
	private $logger;
	public function __construct(
		SearchCriteriaBuilder $searchCriteriaBuilder,
		\Magento\CatalogInventory\Api\StockRegistryInterface $stockItemRepository,
		SourceItemRepositoryInterface $sourceItemRepository,
		\Magento\Store\Model\StoreManagerInterface $storeManagerInterface
	) {
		$this->searchCriteriaBuilder = $searchCriteriaBuilder;
		$this->sourceItemRepository = $sourceItemRepository;
		$this->_stockItemRepository = $stockItemRepository;

		$this->_storeManagerInterface = $storeManagerInterface;

	}
	/**
	 * Retrieves links that are assigned to $stockId
	 *
	 * @param string $sku
	 * @return SourceItemInterface[]
	 */
	public function getSourceItemDetailBySKU(string $sku): array
	{
		$searchCriteria = $this->searchCriteriaBuilder
			->addFilter(SourceItemInterface::SKU, $sku)
			->create();

		$result = $this->sourceItemRepository->getList($searchCriteria)->getItems();

		return $result;
	}
	public function getStockItem($productId) {
		return $this->_stockItemRepository->getStockItem($productId);
	}
	public function afterGetJsonConfig(
		\Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $subject,
		$result
	) {

		$jsonResult = json_decode($result, true);
		$jsonResult['skus'] = [];
		$jsonResult['barcodes'] = [];
		$jsonResult['names'] = [];
		$jsonResult['qtys'] = [];
		foreach ($subject->getAllowProducts() as $simpleProduct) {
			$jsonResult['skus'][$simpleProduct->getId()] = $simpleProduct->getSku();
			$jsonResult['barcodes'][$simpleProduct->getId()] = $simpleProduct->getBarcodeEan();
			$jsonResult['names'][$simpleProduct->getId()] = $simpleProduct->getName();
			$sku = $simpleProduct->getSku(); //sku of product

			$sourceDataBySku = \Magento\Framework\App\ObjectManager::getInstance()->get(\Magento\InventoryCatalogAdminUi\Model\GetSourceItemsDataBySku::class);

//load quantities of assigned source
			$data = $sourceDataBySku->execute($sku);
			

			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$StockState = $objectManager->get('\Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku');
			$qtys = $StockState->execute($sku);
			
			if ($this->_storeManagerInterface->getStore()->getCode() == 'in') {
				$jsonResult['qtys'][$simpleProduct->getId()] =
					[
					'qty' => $this->searchForId('India', $qtys),
				];
			} elseif ($this->_storeManagerInterface->getStore()->getCode() == 'uk') {
				$jsonResult['qtys'][$simpleProduct->getId()] =
					[
					'qty' => $this->searchForId('United Kingdom', $qtys),
				];
			} elseif ($this->_storeManagerInterface->getStore()->getCode() == 'default') {
				$jsonResult['qtys'][$simpleProduct->getId()] =
					[
					'qty' => $this->searchForId('Default Stock', $qtys),
				];
			} elseif ($this->_storeManagerInterface->getStore()->getCode() == 'us') {
				$jsonResult['qtys'][$simpleProduct->getId()] =
					[
					'qty' => $this->searchForId('United States', $qtys),
				];
			} else {
				$jsonResult['qtys'][$simpleProduct->getId()] =
					[
					'qty' => $this->searchForId('Default Stock', $qtys),
				];
			}
			//$_productStock = $this->getStockItem($simpleProduct->getId());
			/*$jsonResult['qtys'][$simpleProduct->getId()] =
				[
				'qty' => $data,
			];*/
		}
		$result = json_encode($jsonResult);
		return $result;
	}

	public function searchForId($id, $array) {
		foreach ($array as $key => $val) {
			if ($val['stock_name'] === $id) {
				return $val['qty'];
			}
		}
		return null;
	}
}