define([
    "jquery",
    "jquery/ui"
], function($){
    "use strict";
     
    function main(config, element) {
        var $element = $(element);
        var AjaxUrl = config.AjaxUrl;
         
        var dataForm = $('#form-docksearch');
        dataForm.mage('validation', {});
         
        $(document).on('click','.submit',function() {
             
            if (dataForm.valid()){
            event.preventDefault();
                var param = dataForm.serialize();
                //alert(param);
                    $.ajax({
                        showLoader: true,
                        url: AjaxUrl,
                        data: param,
                        type: "POST"
                    }).done(function (data) {
                        $('#searchResults').html(data.output);
                        
                        //document.getElementById("form-docksearch").reset();
                        return true;
                    });
                }
            });
    };
return main;
     
     
});
