<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */

namespace Biztech\Geoip\Controller\Adminhtml\Style;

class Configure extends \Magento\Backend\App\Action
{
    protected $resourceConfig;
    protected $helper;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Config\Model\ResourceModel\Config $resourceConfig
     * @param \Biztech\Geoip\Helper\Data $helper
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
	    \Magento\Config\Model\ResourceModel\Config $resourceConfig,
	    \Biztech\Geoip\Helper\Data $helper
    ) {
        $this->resourceConfig = $resourceConfig;
        $this->helper = $helper;
        parent::__construct($context);
	}

    /**
     * Save and Edit GeoIP rule.
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $style = $data['style'];
        if (!$style || ($style>4 && $style<1)) {
            $this->_redirect('geoip/style/index');
            return;
        }
        
        try {
	        $this->resourceConfig->saveConfig("geoip/general/popup_style",$style, 'default', 0);
	        $this->resourceConfig->saveConfig('geoip/popup/style1', 0, 'default', 0);
	        $this->resourceConfig->saveConfig('geoip/popup/style2', 0, 'default', 0);
	        $this->resourceConfig->saveConfig('geoip/popup/style3', 0, 'default', 0);
	        $this->resourceConfig->saveConfig('geoip/popup/style4', 0, 'default', 0);
	        if($style==1) {
	            $this->resourceConfig->saveConfig('geoip/popup/style1', 1, 'default', 0);
	        } elseif ($style==2) {
	            $this->resourceConfig->saveConfig('geoip/popup/style2', 1, 'default', 0);
	        } elseif ($style==3) {
	            $this->resourceConfig->saveConfig('geoip/popup/style3', 1, 'default', 0);
	        } elseif ($style==4) {
	            $this->resourceConfig->saveConfig('geoip/popup/style4', 1, 'default', 0);
	        }
        	$types = ['config','full_page'];
        	$this->helper->flushCache($types);
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        $this->messageManager->addSuccess(__('You have configured <b>Style '.$style.'</b> of redirection pop up.'));
        $this->_redirect('adminhtml/system_config/edit', ['section' => 'geoip']);
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Biztech_Geoip::config');
    }
}