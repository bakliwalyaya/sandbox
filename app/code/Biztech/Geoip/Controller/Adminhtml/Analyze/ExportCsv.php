<?php

namespace Biztech\Geoip\Controller\Adminhtml\Analyze;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Model\Export\ConvertToCsv;
use Magento\Framework\App\Response\Http\FileFactory;

class ExportCsv extends Action
{

    /**
     * @var ConvertToCsv
     */
    protected $converter;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @param Context $context
     * @param ConvertToCsv $converter
     * @param FileFactory $fileFactory
     */
    public function __construct(
        Context $context,
        ConvertToCsv $converter,
        FileFactory $fileFactory
    ) {
        parent::__construct($context);
        $this->converter = $converter;
        $this->fileFactory = $fileFactory;
    }

    /**
     * Export data provider to CSV
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        $layout = $this->_objectManager->get('\Magento\Framework\View\LayoutInterface');
        $fileName = 'GeoipAnalyzeData.csv';
        $content = $layout->createBlock('Biztech\Geoip\Block\Adminhtml\Analyzedata\Grid')->getCsv();
        return $this->fileFactory->create($fileName, $content, 'var');
    }
}
