<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */

namespace Biztech\Geoip\Controller\Adminhtml\Rule;

use Magento\Backend\App\Action;

class MassStatus extends Action
{
    protected $_filter;
    protected $_rule;
    protected $_collectionFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Biztech\Geoip\Model\Rule $rule,
        \Biztech\Geoip\Model\ResourceModel\Rule\CollectionFactory $collectionFactory
    ) {
        $this->_filter = $filter;
        $this->_rule = $rule;
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        try {
            $status = $this->_request->getParam('status');
            $collection = $this->_filter->getCollection($this->_collectionFactory->create());
            $collectionSize = $collection->getSize();
            foreach ($collection as $item) 
            {
                $item = $this->_rule->load($item->getRuleId());
                $item->setstatus($status);
                $item->save();
            }
            $this->messageManager->addSuccess(__("Total of ".$collectionSize." record(s) status has been updated."));
        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }
        return $this->_redirect('*/*/index');    
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Biztech_Geoip::rule');
    }
}
