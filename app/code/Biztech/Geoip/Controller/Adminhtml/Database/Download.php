<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */

namespace Biztech\Geoip\Controller\Adminhtml\Database;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Biztech\Geoip\Helper\GeoipDb;
use Biztech\Geoip\Helper\Data;

class Download extends Action
{
	/**
     * @var Biztech\Geoip\Helper\GeoipDb
     */
	public $_geoIpHelper;
	/**
     * @var Biztech\Geoip\Helper\Data
     */
	public $_data;

	public function __construct(
        Context $context,
        GeoipDb $geoIpHelper,
        Data $data
    ) {
        $this->_geoIpHelper = $geoIpHelper;
        $this->_data = $data;
        parent::__construct($context);
    }

    /**
     * Update action
     */
    public function execute()
    {
    	if(!$this->_data->isEnabled()) {
    		$this->messageManager->addError(__("Activate the GeoIP module for the Download MaxMind GeoIP Database."));
        	return $this->_redirect('adminhtml/system_config/edit', ['section' => 'geoip']);
    	}
    	$downloadStatus = $this->_geoIpHelper->downloadDatabase();
    	if($downloadStatus['success']) {
        	$this->messageManager->addSuccessMessage(__($downloadStatus['message']));
    	} elseif(!$downloadStatus['success']) {
    		$this->messageManager->addError(__($downloadStatus['message']));
    	}
        $this->_redirect('adminhtml/system_config/edit', ['section' => 'geoip']);
	}
}
