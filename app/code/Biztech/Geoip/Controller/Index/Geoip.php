<?php

/**
 *
 * Copyright © 2020 Biztech. All rights reserved.
 */

namespace Biztech\Geoip\Controller\Index;

use \Magento\Framework\App\Action\Context;
use \Magento\Framework\View\Result\PageFactory;
use \Biztech\Geoip\Helper\GeoipDb;

class Geoip extends \Magento\Framework\App\Action\Action
{

    const GEOIP_ENABLE = 'Geoip_store_switch';
    const GEOIP_POPUP_DISABLE = 'Geoip_popup_disable';
    const GEOIP_BACK_REDIRECT = 'Geoip_back_redirect';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var Biztech\Geoip\Helper\GeoipDb
     */
    protected $_geoipHelper;

    /**
     * @param Context $context
     * @param PageFactory resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        GeoipDb $geoipHelper,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Framework\Stdlib\Cookie\PublicCookieMetadata $publiccookieMetadata
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_geoipHelper = $geoipHelper;
        $this->cookieManager = $cookieManager;
        $this->_publiccookieMetadata = $publiccookieMetadata;
        parent::__construct($context);
    }

    public function execute()
    {
        $geoipEnableCoockie = $this->cookieManager->getCookie(self::GEOIP_ENABLE);
        $geoipPopupCoockie = $this->cookieManager->getCookie(self::GEOIP_POPUP_DISABLE);
        $geoipBackRedirect = $this->cookieManager->getCookie(self::GEOIP_BACK_REDIRECT);
        if(isset($geoipEnableCoockie) && isset($geoipBackRedirect))
        {
           $this->_redirect($geoipBackRedirect);
        }
        $redirectUrl = $this->_redirect->getRedirectUrl();
        if(!isset($geoipBackRedirect))
        {
            $back_redirect_cookie_time = $this->_geoipHelper->getPopupRegenerateTime();
            $back_redirect_cookie_time = $back_redirect_cookie_time/2;
            $this->_publiccookieMetadata->setDuration(null)->setPath('/');
            $this->cookieManager->setPublicCookie(self::GEOIP_BACK_REDIRECT, $redirectUrl, $this->_publiccookieMetadata);
        }
        return $this->resultPageFactory->create();
    }
}
