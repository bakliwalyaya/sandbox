<?php

namespace Biztech\Geoip\Cron;
use \Biztech\Geoip\Helper\Data;
use \Biztech\Geoip\Helper\GeoipDb;

class DatabaseDownload
{
	/**
     * @var Biztech\Geoip\Helper\Data
     */
    protected $_helper;

    /**
     * @var Biztech\Geoip\Helper\GeoipDb
     */
    protected $_geoipHelper;
	
    /**
     * @var Magento\Config\Model\ResourceModel\Config
     */
    protected $resourceConfig;

    /**
     * @var Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $_date;

	/**
     * @param Data $helper
     * @param GeoipDb $geoipHelper
     * @param Logger $logger
     */
    public function __construct(
        Data $helper,
        GeoipDb $geoipHelper,
        \Magento\Config\Model\ResourceModel\Config $resourceConfig,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date
    ) {
        $this->_helper = $helper;
        $this->_geoipHelper = $geoipHelper;
        $this->resourceConfig = $resourceConfig;
        $this->_date =  $date;
    }

    public function execute()
    {
    	try {
            if($this->_helper->downloadDatabaseByCron()) {
    			if($this->_helper->isEnabled()) {
    				$maxmind_key = $this->_helper->maxmindKey();
    				if($maxmind_key !='' || $maxmind_key != null)
    	            {
    	            	$result = $this->_geoipHelper->downloadDatabase();
                        $dateTime = $this->_date->date()->format('d-m-Y H:i');
                        $this->resourceConfig->saveConfig('geoip/cron/last_synched', $dateTime , 'default', 0);
                        $types = ['config','full_page'];
                        $this->_helper->flushCache($types);
                    }	
                }
            }
	    } catch (\Exception $e) { }
    }
}