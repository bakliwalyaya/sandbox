<?php
namespace Biztech\Geoip\Model\ResourceModel\Rule;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'rule_id';
	protected $_eventPrefix = 'geoip_rule_collection';
	protected $_eventObject = 'rule_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Biztech\Geoip\Model\Rule','Biztech\Geoip\Model\ResourceModel\Rule');
	}

}
