<?php
namespace Biztech\Geoip\Model\ResourceModel\Analyze;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'analyze_id';
	protected $_eventPrefix = 'geoip_analyze_collection';
	protected $_eventObject = 'analyze_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Biztech\Geoip\Model\Analyze','Biztech\Geoip\Model\ResourceModel\Analyze');
	}

}
