<?php
/** 
 * Copyright © 2020 Biztech . All rights reserved. 
 **/

namespace Biztech\Geoip\Model\Config\Source\Rule;

class Priority implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
    	$options = ['3' => __('High'),'2' => __('Medium'), '1' => __('Low')];
        return $options;
    }
}
