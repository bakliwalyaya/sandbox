<?php
/** 
 * Copyright © 2020 Biztech . All rights reserved. 
 **/

namespace Biztech\Geoip\Model\Config\Source;

use Biztech\Geoip\Helper\Data;

class Popopstyle implements \Magento\Framework\Option\ArrayInterface
{
    protected $_helper;

    /**
     * @param Data $helperdata [description]
     */
    public function __construct(
        Data $helperdata
    ) {
        $this->helper = $helperdata;
    }

    public function toOptionArray()
    {
        $options = [
            ['value' => 1, 'label' => __('Style 1')],['value' => 2, 'label' => __('Style 2')],
            ['value' => 3, 'label' => __('Style 3')],['value' => 4, 'label' => __('Style 4')],
        ];
        $websites = $this->helper->getAllWebsites();
        return $options;
    }
}
