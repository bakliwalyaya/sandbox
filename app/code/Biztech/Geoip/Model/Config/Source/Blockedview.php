<?php
/** 
 * Copyright © 2020 Biztech . All rights reserved. 
 **/

namespace Biztech\Geoip\Model\Config\Source;

use Biztech\Geoip\Helper\Data;

class Blockedview implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        $options = [
            ['value' => 'popup', 'label' => __('Pop up message')],['value' => 'url', 'label' => __('Redirect URL')],
        ];
        return $options;
    }
}
