<?php

/**
 * Copyright © 2020 Biztech . All rights reserved.
 */

namespace Biztech\Geoip\Block\Index;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Framework\Stdlib\CookieManagerInterface;
use \Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;
use \Biztech\Geoip\Helper\GeoipDb;
use \Biztech\Geoip\Helper\Data;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\App\Config\ReinitableConfigInterface;



class Index extends Template
{

    const GEOIP_POPUP_DISABLE = 'Geoip_popup_disable';
    const GEOIP_BACK_REDIRECT = 'Geoip_back_redirect';

    /**
	 * @var \Magento\Framework\Stdlib\CookieManagerInterface
	 */
	protected $_cookieInterface;

	/**
     * @var \Magento\Framework\Stdlib\Cookie\PublicCookieMetadata
     */
    protected $_publiccookieMetadata;

    /**
     * @var Biztech\Geoip\Helper\GeoipDb
     */
    protected $_geoipHelper;

    /**
     * @var Biztech\Geoip\Helper\Data
     */
    protected $_dataHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\App\Config\ReinitableConfigInterface
     */
    protected $_coreConfig;

	 /**
     * @param Context $context
     * @param CookieManagerInterface $cookieInterface
     * @param PublicCookieMetadata $publiccookieMetadata
     * @param GeoipDb $geoipHelper
     * @param StoreManagerInterface $storeManager
     */
   	public function __construct(
        Context $context,
        CookieManagerInterface $cookieInterface,
 		PublicCookieMetadata $publiccookieMetadata,
        GeoipDb $geoipHelper,
        Data $dataHelper,
        StoreManagerInterface $storeManager,
        ReinitableConfigInterface $coreConfig,
        array $data = []
    ) {
        $this->_cookieInterface = $cookieInterface;
        $this->_publiccookieMetadata = $publiccookieMetadata;
        $this->_geoipHelper = $geoipHelper;
        $this->_dataHelper = $dataHelper;
        $this->_storeManager = $storeManager;
        $this->_coreConfig = $coreConfig;
        parent::__construct($context, $data);
    }

     /**
     * Disable the popup for 20 min after user canceled action on popup
     *
     */
    public function setCookieForPopup()
    {

        $popup_regenerate_time = $this->_geoipHelper->getPopupRegenerateTime();
        $this->_publiccookieMetadata->setDuration(null)->setPath('/');
        $this->_cookieInterface->setPublicCookie(self::GEOIP_POPUP_DISABLE, 'disabled', $this->_publiccookieMetadata);
    }
    /**
     * Get the Store name in which auto store switch action.
     *
     */
    public function getGeoDetectedStoreName()
    {     
        $locationData = $this->_geoipHelper->getCountryLoction();
        $locationCountryCode = $locationData['country']['iso_code'];
        $getStoreCurrencyData = $this->_geoipHelper->getStoreCurrency($locationCountryCode);
        if(empty($getStoreCurrencyData)) return;
        foreach ($getStoreCurrencyData as $key => $value) {
           $store_id = $value['store_id'];
           $currency = $value['currency_code'];
        }
        return $this->_storeManager->getStore($store_id)->getName();
    }

    /**
     * Get popup title.
     */
    public function getTitle()
    {
        $title = $this->_dataHelper->pupupTitle();
        if($title == '' || $title == null) {
            return 'Redirect Store?';
        } else {
            return $title;
        }
    }

    /**
     * Get popup message.
     */
    public function getMessage()
    {
        $message = $this->_dataHelper->pupupMessage();
        if($message == '' || $message == null) {
            return 'Based on your location, you should redirect to other store.
                    Click below to get redirected.';
        } else {
            return $message;
        }   
    }

    /**
     * Get popup button text.
     */
    public function getButtonText()
    {
        $button = $this->_dataHelper->pupupButtonText();
        if($button == '' || $button == null) {
            return 'Redirect';
        } else {
            return $button;
        }
    }

    /**
     * is Manual store Swich enable.
     */
    public function manualstoreSwich()
    {
        return $this->_dataHelper->isManualstoreSwich();   
    }

    /**
     * Get redirect back url.
     */
    public function redirectBack()
    {
        if(!$this->manualstoreSwich()) {
            return $this->getUrl('geoip/index/index');
        }
        return $this->_cookieInterface->getCookie(self::GEOIP_BACK_REDIRECT);;
    }
}
