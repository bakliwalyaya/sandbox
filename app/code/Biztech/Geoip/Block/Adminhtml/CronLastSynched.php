<?php

namespace Biztech\Geoip\Block\Adminhtml;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;


class CronLastSynched extends Field
{

    protected $_helper;
    protected $_assetRepo;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Biztech\Geoip\Helper\Data $helper,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        array $data = []
    ) {
        $this->_helper = $helper;
        $this->_assetRepo = $assetRepo;
        parent::__construct($context, $data);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $websites = $this->_helper->getAllWebsites();
        if (!empty($websites)) {
            $html = $element->getElementHtml();
                $html.="<br>&nbsp;Cron Last Synced At: <b>".$this->_helper->cronLastSynched()."</b>";    
            return $html;
        }
    }
}
