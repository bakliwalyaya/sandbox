<?php

namespace Biztech\Geoip\Block\Adminhtml;

use Magento\Framework\View\Element\Template\Context;
use Biztech\Geoip\Helper\Data;
use Biztech\Geoip\Model\Analyze;
use Biztech\Geoip\Model\Rule;
use Magento\Store\Model\StoreManagerInterface;

class Analyzedatadetail extends \Magento\Framework\View\Element\Template
{

	/**
     * @var Biztech\Geoip\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Biztech\Geoip\Model\Rule
     */
    protected $_rule;

    /**
    * @var \Biztech\Geoip\Model\Analyze
    */
    protected $_analyze;

    /**
    * @var \Magento\Store\Model\StoreManagerInterface
    */
    protected $_storeManager;

    /**
     * @param Context                  $context
     * @param Data                     $helperData
     * @param array                    $data
     * @param Rule			 		   $rule
	 * @param Analyze				   $analyze
	 * @param StoreManagerInterface	   $store
     */
    public function __construct(
        Context $context,
        Data $helper,
        Rule $rule,
        Analyze $analyze,
        StoreManagerInterface $store,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->_rule = $rule;
        $this->_analyze = $analyze;
        $this->_storeManager = $store;
        parent::__construct($context, $data);
    }
    /*get anazyze Data*/
    public function getAnalyzeDataDetail()
    {
        $order_id = $this->getRequest()->getParam('id');
        $analyzeData = $this->_analyze->load($order_id);
        return $analyzeData->getData();
    }
    
    /*get rule Data*/
    public function getRuleDetail($ruleId)
    {
        return $this->_rule->load($ruleId)->getData();
    }

    /*get visitor Data*/    
    public function getIP($ip)
    {
    	return long2ip($ip);
    }

    /*get action date Data*/
    public function getDate($date)
    {
    	$time = strtotime($date);
		$date = date('d-M-Y',$time);
    	return $date;
    }

    /*get redirected Store*/    
    public function getStorename($storeid)
    {
    	return $this->_storeManager->getStore()->load($storeid)->getName();
    }

    /**
     * Get redirect back url.
     */
    public function backUrl()
    {
        return $this->getUrl('geoip/analyze/index');
    }
}
