<?php

namespace Biztech\Geoip\Block\Adminhtml\Analyzedata\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;
use Magento\Backend\Block\Context;

class Blocked extends AbstractRenderer
{
    /**
     * @param Context    $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    public function render(DataObject $row)
    {
        if ($row->getBlockedRequest()) {
            $cell = '<span class="grid-severity-critical"><span> Blocked </span></span>';
        } else {
            $cell = '<span class="grid-severity-notice"><span> UnBlocked </span></span>';
        }
        return $cell;
    }
}
