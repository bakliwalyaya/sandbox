<?php

namespace Biztech\Geoip\Block\Adminhtml\Analyzedata\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;
use Magento\Backend\Block\Context;

class Detailview extends AbstractRenderer
{
    /**
     * @param Context    $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    public function render(DataObject $row)
    {
        $html = "<a href='" . $this->getUrl('geoip/analyze/detail', ['id'=>$row->getAnalyzeId()]) . "'>";
        $html .= __('View Detail');
        $html .= "</a>";
        return $html;
    }
}
