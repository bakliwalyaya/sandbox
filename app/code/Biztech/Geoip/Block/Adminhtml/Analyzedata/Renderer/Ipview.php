<?php

namespace Biztech\Geoip\Block\Adminhtml\Analyzedata\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;
use Magento\Backend\Block\Context;

class Ipview extends AbstractRenderer
{
    /**
     * @param Context    $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    public function render(DataObject $row)
    {
         return long2ip($row->getVisitorIp());
    }
}
