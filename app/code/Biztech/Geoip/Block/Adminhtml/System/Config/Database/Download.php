<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */

namespace Biztech\Geoip\Block\Adminhtml\System\Config\Database;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Download extends Field
{
    /**
     * Adds update button to config field
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $url = $this->_urlBuilder->getUrl('geoip/database/download');
        $message = 'Database is downloading from <b>MaxMind server</b> and it will take some time so during the downloading process do not stop the execution.';
        $buttonHtml = $this->getLayout()->createBlock('Magento\Backend\Block\Widget\Button')
            ->setType('button')
            ->setId('download_db')
            ->setLabel(__('Download GeoIP Database'))
            ->setOnClick("confirmSetLocation('{$message}', '{$url}')")
            ->setClass("action-scalable action-secondary")
            ->setStyle("color: #fff;border-color: gray;width:91%")
            ->toHtml();
        return $buttonHtml;
    }
}
