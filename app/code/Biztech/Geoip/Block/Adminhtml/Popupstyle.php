<?php

namespace Biztech\Geoip\Block\Adminhtml;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;


class Popupstyle extends Field
{

    protected $_helper;
    protected $_assetRepo;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Biztech\Geoip\Helper\Data $helper,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        array $data = []
    ) {
        $this->_helper = $helper;
        $this->_assetRepo = $assetRepo;
        parent::__construct($context, $data);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $websites = $this->_helper->getAllWebsites();
        if (!empty($websites)) {
            $html = $element->getElementHtml();
            $style = $this->getUrl("geoip/style/index");
            $style = "<center>&nbsp;<a href='$style'>View and select popup style</a>&nbsp;</center>";
            $html.= $style;    
            return $html;
        }
    }
}
