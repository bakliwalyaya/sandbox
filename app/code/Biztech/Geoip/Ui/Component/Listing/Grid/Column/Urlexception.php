<?php
namespace Biztech\Geoip\Ui\Component\Listing\Grid\Column;
class Urlexception extends \Magento\Ui\Component\Listing\Columns\Column {

    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ){
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource) {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $url_exception =preg_split('/\r?\n/', $item['url_exception']);
                $url_exception1 = '';
                foreach ($url_exception as $key => $value) {
                    $url_exception1.=$value.'<br>';
                }
                $item['url_exception'] = $url_exception1;
            }
        }
        return $dataSource;
    }
}
