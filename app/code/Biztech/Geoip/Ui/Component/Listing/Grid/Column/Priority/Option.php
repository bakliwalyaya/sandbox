<?php
/** 
 * Copyright © 2020 Biztech . All rights reserved. 
 **/

namespace Biztech\Geoip\Ui\Component\Listing\Grid\Column\Priority;

class Option implements \Magento\Framework\Data\OptionSourceInterface
{
    public function toOptionArray()
    {
        $options['3'] = ['value' => 3, 'label' => __("High")];
        $options['2'] = ['value' => 2, 'label' => __("Medium")];
        $options['1'] = ['value' => 1, 'label' => __("Low")];
        return $options;
    }
}
