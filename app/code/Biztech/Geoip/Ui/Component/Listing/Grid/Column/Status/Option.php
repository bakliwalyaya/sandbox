<?php
/** 
 * Copyright © 2020 Biztech . All rights reserved. 
 **/

namespace Biztech\Geoip\Ui\Component\Listing\Grid\Column\Status;

class Option implements \Magento\Framework\Data\OptionSourceInterface
{
    public function toOptionArray()
    {
        $options['0'] = ['value' => 0, 'label' => __("Disable")];
        $options['1'] = ['value' => 1, 'label' => __("Enable")];
        return $options;
    }
}
