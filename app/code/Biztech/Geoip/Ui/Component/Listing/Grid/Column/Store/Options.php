<?php
/**
 * Copyright © 2020 Biztech . All rights reserved.
 */
    
namespace Biztech\Geoip\Ui\Component\Listing\Grid\Column\Store;

use Biztech\Geoip\Helper\Data;
use Magento\Store\Model\StoreManagerInterface;

class Options implements \Magento\Framework\Data\OptionSourceInterface
{

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    public function __construct(
        StoreManagerInterface $storeManager,
        Data $helper,
        array $components = [],
        array $data = []
    ){
        $this->helper = $helper;
        $this->_storeManager = $storeManager;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    { 
        $websites = $this->helper->getAllWebsites();
        $storeView = [];
        foreach ($this->_storeManager->getWebsites() as $website) {
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                    if ($store && in_array($store->getId(), $websites)) {
                        $id = $store->getId();
                        $name = $store->getName();
                        $storeView[] = ['value' => $id, 'label' => __(ucwords($website->getName()." → ". $group->getName()." → ". $name))];
                    }
                }
            } 
        }
        return $storeView;
    }
}