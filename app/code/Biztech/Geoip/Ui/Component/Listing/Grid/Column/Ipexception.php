<?php
namespace Biztech\Geoip\Ui\Component\Listing\Grid\Column;

class Ipexception extends \Magento\Ui\Component\Listing\Columns\Column {

    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ){
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource) {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $ip_exception =preg_split('/\r?\n/', $item['ip_exception']);
                $ip_exception1 = '';
                foreach ($ip_exception as $key => $value) {
                    $ip_exception1.=$value.'<br>';
                }
                $item['ip_exception'] = $ip_exception1;
            }
        }

        return $dataSource;
    }
}
