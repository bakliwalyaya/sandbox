<?php
namespace Biztech\Geoip\Observer;

use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Event\Observer;
use \Biztech\Geoip\Helper\Data;
use \Biztech\Geoip\Helper\GeoipDb;
use \Magento\Store\Model\StoreManagerInterface;
use \Biztech\Geoip\Block\Index\Index;

class HomepageCache implements ObserverInterface
{

    const GEOIP_ENABLE = 'Geoip_store_switch';
    const GEOIP_POPUP_DISABLE = 'Geoip_popup_disable';

   	/**
     * @var Biztech\Geoip\Helper\Data
     */
    protected $_helper;

    /**
     * @var Biztech\Geoip\Helper\GeoipDb
     */
    protected $_geoipHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManagerInterface;

    /**
     * @var \Biztech\Geoip\Block\Index\Index
     */
    protected $_geoipBlock;

    /**
     * @param Data $helper
     * @param GeoipDb $geoipHelper
     * @param StoreManagerInterface $storeManagerInterface
     * @param CookieManagerInterface $cookieManager
     */
	public function __construct(
        Data $helper,
        GeoipDb $geoipHelper,
        StoreManagerInterface $storeManagerInterface,
        Index $geoipBlock,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
    ) {
        $this->_helper = $helper;
        $this->_geoipHelper = $geoipHelper;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_geoipBlock = $geoipBlock;
        $this->cookieManager = $cookieManager;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $base_url = $this->_storeManagerInterface->getStore()->getBaseUrl();
        $current_url = explode('?', $this->_storeManagerInterface->getStore()->getCurrentUrl());
        $currentUrl = $current_url[0];
    	/*
			if the module not enabled or database not exist
		*/
		if(!$this->_helper->isEnabled() || !$this->_geoipHelper->checkDatabaseExist()) return;

        $locationData = $this->_geoipHelper->getCountryLoction();
       
		/*
			if the by fetched geoip country data not fetched.
		*/
        if(!isset($locationData['country']['iso_code'])) return;

        $locationCountryCode = $locationData['country']['iso_code'];
        $get_store_currency = $this->_geoipHelper->getStoreCurrency($locationCountryCode);
        /*
            if the rule not assigned for the switch store of fetched geoLocation country ip.
        */
        if(empty($get_store_currency)) return;

        $geoipEnableCoockie = $this->cookieManager->getCookie(self::GEOIP_ENABLE);
        $geoipPopupCoockie = $this->cookieManager->getCookie(self::GEOIP_POPUP_DISABLE);
        if($this->_helper->isPopupEnabled()) {
            if($currentUrl==$base_url && !isset($geoipEnableCoockie) && !isset($geoipPopupCoockie))
            {
                $this->_geoipBlock->setCookieForPopup();
            }
            if($currentUrl==$base_url) {
                $cacheTypes = [
                    'config',
                    'layout',
                    'block_html',
                    'collections',
                    'reflection',
                    'db_ddl',
                    'eav',
                    'config_integration',
                    'config_integration_api',
                    'full_page',
                    'translate',
                    'config_webservice'
                    ];
                $this->_helper->flushCache($cacheTypes);
            }
        }
	}
}
