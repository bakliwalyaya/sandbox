<?php
namespace Biztech\Geoip\Observer;

use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Event\Observer;
use \Biztech\Geoip\Helper\Data;
use \Biztech\Geoip\Helper\GeoipDb;
use \Biztech\Geoip\Block\Index\Index;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\View\Element\BlockFactory;
use \Magento\Framework\HTTP\PhpEnvironment\Response;

class AutoStoreCurrencySwitcher implements ObserverInterface
{

    const GEOIP_ENABLE = 'Geoip_store_switch';
    const GEOIP_POPUP_DISABLE = 'Geoip_popup_disable';
    const MANUAL_STORE_SWITCH = 'manual_store_switch';
    const MANUAL_CURRENCY_SWITCH = 'manual_currency_switch';
    const REDIRECT_TO_URL = 'redirect_to_url';
    const GEOIP_BACK_REDIRECT = 'Geoip_back_redirect';
    const GEOIP_STORE_SWITCH_FIRTST_ACTION = "get_store_switch_first_action";

    /**
     * @var Biztech\Geoip\Helper\Data
     */
    protected $_helper;

    /**
     * @var Biztech\Geoip\Helper\GeoipDb
     */
    protected $_geoipHelper;

    /**
     * @var \Biztech\Geoip\Block\Index\Index
     */
    protected $_geoipBlock;

    /**
     * @var Biztech\Geoip\Model\MaxMind\Db\Reader
     */
    protected $_reader;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManagerInterface;

    /**
     * @var \Magento\Framework\View\Element\BlockFactory
     */
    protected $_blockFactory;

    /**
     * @var \Magento\Framework\HTTP\PhpEnvironment\Response
     */
    protected $_response;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlInterface;
    
    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\PublicCookieMetadata
     */
    protected $publiccookieMetadata;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory CookieMetadataFactory
     */
    protected $cookieMetadataFactory;

    /**
    * @var \Biztech\Geoip\Model\Analyze
    */
    protected $_analyze;

    /**
    * @var \Biztech\Geoip\Model\AnalyzeFactory
    */
    protected $_analyzeFactory;

    /**
    * @var \Magento\Framework\App\Request\Http
    */
    Protected $_httpRequest;

    /**
    * @var \Biztech\Geoip\Model\Rule
    */
    protected $_rule;

    /**
     * @param Data $helper
     * @param GeoipDb $geoipHelper
     * @param Index $geoipBlock
     * @param StoreManagerInterface $storeManagerInterface
     * @param BlockFactory $blockFactory
     * @param Response $response
     * @param \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager
     * @param \Magento\Framework\UrlInterface $urlInterface
     * @param \Magento\Framework\App\Response\RedirectInterface $redirect
     * @param \Magento\Framework\Stdlib\Cookie\PublicCookieMetadata $publiccookieMetadata
     * @param \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory
     */
    public function __construct(
        Data $helper,
        GeoipDb $geoipHelper,
        Index $geoipBlock,
        StoreManagerInterface $storeManagerInterface,
        BlockFactory $blockFactory,
        Response $response,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Framework\UrlInterface $urlInterface,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Framework\Stdlib\Cookie\PublicCookieMetadata $publiccookieMetadata,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        \Biztech\Geoip\Model\Analyze $analyze,
        \Biztech\Geoip\Model\AnalyzeFactory $analyzeFactory,
        \Magento\Framework\App\Request\Http $httpRequest,
        \Biztech\Geoip\Model\Rule $rule
    ) {
        $this->_helper = $helper;
        $this->_geoipHelper = $geoipHelper;
        $this->_geoipBlock = $geoipBlock;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_blockFactory = $blockFactory;
        $this->_response = $response;
        $this->cookieManager = $cookieManager;
        $this->_urlInterface = $urlInterface;
        $this->redirect = $redirect;
        $this->publiccookieMetadata = $publiccookieMetadata;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->_analyze = $analyze;
        $this->_analyzeFactory = $analyzeFactory;
        $this->_httpRequest = $httpRequest;
        $this->_rule = $rule;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /* if the module not enabled or database not exist. */
        if(!$this->_helper->isEnabled() || !$this->_geoipHelper->checkDatabaseExist()) return;
        $geoipEnableCoockie = $this->cookieManager->getCookie(self::GEOIP_ENABLE);
        $geoipPopupCoockie = $this->cookieManager->getCookie(self::GEOIP_POPUP_DISABLE);
        if($this->_helper->isPopupEnabled()) {
            if (!$geoipEnableCoockie) {
                $metadata = $this->cookieMetadataFactory->createPublicCookieMetadata();
                $metadata->setPath('/');
                $this->cookieManager->deleteCookie(self::MANUAL_STORE_SWITCH,$metadata);
                $this->cookieManager->deleteCookie(self::MANUAL_CURRENCY_SWITCH,$metadata);
            }
        }

        $manualStoreSwicth = $this->cookieManager->getCookie(self::MANUAL_STORE_SWITCH);
        if(isset($manualStoreSwicth)) {
            return;
        }

        $locationData = $this->_geoipHelper->getCountryLoction();

        /* if the by fetched geoip country data not fetched. */
        if(!isset($locationData['country']['iso_code'])) return;

        $locationCountryCode = $locationData['country']['iso_code'];
        $country = $locationData['country']['names']['en'];
        $get_store_currency = $this->_geoipHelper->getStoreCurrency($locationCountryCode);
        if(isset($get_store_currency['blocked'])) {
            /* Store GeoIP Analyze data.*/
            $blocked=1;
            $rule_id = null;
            $this->analyzeDataStore($blocked,$country,$rule_id);
            /*end store analyze data*/
            $base_url = $this->_storeManagerInterface->getStore()->getBaseUrl();
            $current_url = explode('?', $this->_storeManagerInterface->getStore()->getCurrentUrl());
            $currentUrl = $current_url[0];
            $current_url1 = explode($base_url,$currentUrl); 
            $current_url1 = $current_url1[1];
            $current_url1 = rtrim($current_url1,"/");
            $redirect_to = $get_store_currency['redirect_to'];
            $blocked_view = $this->_helper->blockedView();
            if($blocked_view=='popup'){
                $redirect_to = 'geoip/index/blockedCountryPopup';
            }
            if($redirect_to!=$current_url1)
            {
                $url = $this->_urlInterface->getUrl($redirect_to);
                $controller = $observer->getControllerAction();
                return $this->redirect->redirect($controller->getResponse(), $url);
            } else return;
        }
        if(isset($get_store_currency['blocked'])) return;
        /* if the rule not assigned for the switch store of fetched geoLocation country ip. */
        if(empty($get_store_currency)) return;
        
        foreach ($get_store_currency as $key => $value) {
           $store_id = $value['store_id'];
           $currency = $value['currency_code'];
           $ruleId = $value['rule_id'];
        }

        /*Exception IP and URL*/
        $base_url = $this->_storeManagerInterface->getStore()->getBaseUrl();
        $current_url = explode('?', $this->_storeManagerInterface->getStore()->getCurrentUrl());
        $currentUrl = $current_url[0];
        $current_url1 = explode($base_url,$currentUrl); 
        $current_url1 = $current_url1[1];
        $current_url1 = rtrim($current_url1,"/");
        $moduleName = $this->_httpRequest->getModuleName();
        $controller = $this->_httpRequest->getControllerName();
        $action = $this->_httpRequest->getActionName();
        $current_url2 = $moduleName."/".$controller."/".$action;
        $data = $this->_rule->load($ruleId);
        $url_exception =  preg_split('/\r?\n/', $data['url_exception']);
        $ip_exception =  preg_split('/\r?\n/', $data['ip_exception']);
        $customerIp = $this->_geoipHelper->getCustomerIP();
        
        foreach ($url_exception as $key => $value) {
            $url_exception[$key] = rtrim($value,"/");
            $url_exception[$key] = ltrim($url_exception[$key],"/");
        }
        
        if(in_array($current_url1, $url_exception) || in_array($current_url2, $url_exception)) {
            if($current_url1!=null || $current_url1!="") 
                return;
        }
        if(in_array($customerIp, $ip_exception)) {
            return;
        }
        
        /* If manual store switch enable and actined for manual store swiched. */
        if($this->_helper->isManualstoreSwich()) {
            $current_url = explode('?', $this->_storeManagerInterface->getStore()->getCurrentUrl());
            if(strpos($current_url[0], "___store") !== false){
                $this->publiccookieMetadata->setDuration(null)->setPath('/');
                $this->cookieManager->setPublicCookie(self::MANUAL_STORE_SWITCH, 'enabled', $this->publiccookieMetadata);
                return;
            }
        }

        if($this->_helper->isPopupEnabled()) {
            if(!isset($geoipEnableCoockie) && !isset($geoipPopupCoockie))
            {
                $redirectUrl = $this->_storeManagerInterface->getStore()->getCurrentUrl();
                $back_redirect_cookie_time = $this->_geoipHelper->getPopupRegenerateTime();
                $back_redirect_cookie_time = $back_redirect_cookie_time/2;
                $this->publiccookieMetadata->setDuration(null)->setPath('/');
                $this->cookieManager->setPublicCookie(self::GEOIP_BACK_REDIRECT, $redirectUrl, $this->publiccookieMetadata);
                $this->_geoipBlock->setCookieForPopup();
                $url = $this->_urlInterface->getUrl('geoip/index/geoip');
                $controller = $observer->getControllerAction();
                $this->redirect->redirect($controller->getResponse(), $url);
            }
            if(!isset($geoipEnableCoockie)) return;
        }

        /* Store GeoIP Analyze data.*/
        $blocked=0;
        $this->analyzeDataStore($blocked,$country,$ruleId);
        /*end store analyze data*/

        $this->_storeManagerInterface->setCurrentStore($store_id);

        if ($currency) {
            if($this->_helper->isManualCurrencySwich()) {
                $current_url = explode('?', $this->_storeManagerInterface->getStore()->getCurrentUrl());
                if(strpos($current_url[0], "currency/switch") !== false){
                    $this->publiccookieMetadata->setDuration(null)->setPath('/');
                    $this->cookieManager->setPublicCookie(self::MANUAL_CURRENCY_SWITCH, 'enabled', $this->publiccookieMetadata);
                }
            }
            if(!$this->cookieManager->getCookie(self::MANUAL_CURRENCY_SWITCH)) {
                if($this->_storeManagerInterface->getStore()->getCurrentCurrencyCode() != $currency) { 
                    $this->_storeManagerInterface->getStore()->setCurrentCurrencyCode($currency);
                }
            }
        }
        if(!$this->cookieManager->getCookie(self::GEOIP_STORE_SWITCH_FIRTST_ACTION)) {
            $this->publiccookieMetadata->setDuration(null)->setPath('/');
            $this->cookieManager->setPublicCookie(self::GEOIP_STORE_SWITCH_FIRTST_ACTION, 'enabled', $this->publiccookieMetadata);
            $current_url = $this->_storeManagerInterface->getStore()->getCurrentUrl(false);
            $controller = $observer->getControllerAction();
            return $this->redirect->redirect($controller->getResponse(),$current_url);
        }
    }
    public function analyzeDataStore($blocked,$country,$ruleId)
    {
        $analyze_factory = $this->_analyzeFactory->create();
        $customerIP = ip2long($this->_geoipHelper->getCustomerIP());
        $analyzeData = $analyze_factory->getCollection()->getData();
        $analyze_data = array();
        $analyze_data['analyze_rule_id'] = $ruleId;
        $analyze_data['visitor_country'] = $country;
        $analyze_data['visitor_ip'] = $customerIP;
        $analyze_data['blocked_request'] = $blocked;
        $current_url = explode('?', $this->_storeManagerInterface->getStore()->getCurrentUrl());
        $base_url = $this->_storeManagerInterface->getStore()->getBaseUrl();
        $currentUrl = $current_url[0];
        $currentUrl = explode($base_url,$currentUrl);
        $currentUrl = $currentUrl[1];
        if($currentUrl==null || $currentUrl=='') {
            $analyze_data['redirect_to'] = 'Homepage';
        } else {
            $analyze_data['redirect_to'] = $currentUrl;
        }
        
        $save_analyze_data = true;
        foreach ($analyzeData as $key => $value) {
                $time = strtotime($value['created_at']);
                $date = date('d-m-y',$time);
                if($value['visitor_ip']==$customerIP && date('d-m-y')==$date) {
                    $save_analyze_data = false;
                }

        }
        if(empty($analyzeData)) {
            $analyze_factory->setData($analyze_data);
            $analyze_factory->save();
        } elseif($save_analyze_data==true) {
            $analyze_factory->setData($analyze_data);
            $analyze_factory->save();
        }
    }
}
