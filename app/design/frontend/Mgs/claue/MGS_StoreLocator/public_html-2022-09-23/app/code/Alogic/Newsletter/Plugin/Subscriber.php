<?php
namespace Alogic\Newsletter\Plugin;

use Magento\Framework\App\Request\Http;

class Subscriber {
	protected $request;
	public function __construct(Http $request) {
		$this->request = $request;
	}

	public function aroundSubscribe($subject, \Closure $proceed, $email) {

		if ($this->request->isPost() && $this->request->getPost('fullname')) {

			$fullname = explode(' ', $this->request->getPost('fullname'), 2);
			$firstname = '';
			$lastname = '';
			if (isset($fullname[0])) {
				$firstname = $fullname[0];
			}
			if (isset($fullname[1])) {
				$lastname = $fullname[1];
			}

			$subject->setCFirstname($firstname);
			$subject->setCLastname($lastname);
			$result = $proceed($email);

			try {
				$subject->save();
			} catch (\Exception $e) {
				throw new \Exception($e->getMessage());
			}
		}
		return $result;
	}
}