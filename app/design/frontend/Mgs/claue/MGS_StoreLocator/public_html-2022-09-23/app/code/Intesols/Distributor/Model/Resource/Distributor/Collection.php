<?php

namespace Intesols\Distributor\Model\Resource\Distributor;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
	/**
	 * Define model & resource model
	 */
	protected function _construct() {
		$this->_init('Intesols\Distributor\Model\Distributor', 'Intesols\Distributor\Model\Resource\Distributor'

		);
	}
}
