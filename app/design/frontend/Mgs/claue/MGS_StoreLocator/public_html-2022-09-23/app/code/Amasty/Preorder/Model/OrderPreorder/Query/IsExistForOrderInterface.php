<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_Preorder
 */


declare(strict_types=1);

namespace Amasty\Preorder\Model\OrderPreorder\Query;

interface IsExistForOrderInterface
{
    /**
     * @param int $orderId
     * @return bool
     */
    public function execute(int $orderId): bool;

    /**
     * @param int $orderId
     * @return void
     */
    public function setAsProcessed(int $orderId): void;
}
