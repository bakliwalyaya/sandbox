<?php
namespace Alogic\Stockalerts\Cron;

use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\InventoryCatalog\Model\GetStockIdForCurrentWebsite;
use Magento\InventorySalesApi\Model\GetStockItemDataInterface;
use Magento\InventorySales\Model\GetProductSalableQty;
use Magento\InventorySales\Model\ResourceModel\GetAssignedStockIdForWebsite;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

class Emailalerts {
	private $getAssignedStockIdForWebsite;
	public $websiteRepository;
	protected $_productRepository;
	protected $logger;
	protected $_subscriberFactory;
	protected $transportBuilder;
	protected $storeManager;
	protected $inlineTranslation;
	/**
	 * @var GetProductSalableQty
	 */
	private $getProductSalableQty;

	/**
	 * @var GetStockIdForCurrentWebsite
	 */
	private $getStockIdForCurrentWebsite;

	/**
	 * @var GetStockItemDataInterface
	 */
	private $getStockItemData;

	public function __construct(LoggerInterface $logger,
		\Alogic\Stockalerts\Model\SubscriberFactory $subscriberFactory,
		GetProductSalableQty $getProductSalableQty,
		GetStockIdForCurrentWebsite $getStockIdForCurrentWebsite,
		\Magento\Catalog\Model\ProductRepository $productRepository,
		GetAssignedStockIdForWebsite $getAssignedStockIdForWebsite,
		WebsiteRepositoryInterface $websiteRepository,
		GetStockItemDataInterface $getStockItemData,
		TransportBuilder $transportBuilder,
		StoreManagerInterface $storeManager,
		StateInterface $state
	) {
		$this->logger = $logger;
		$this->_subscriberFactory = $subscriberFactory;
		$this->getProductSalableQty = $getProductSalableQty;
		$this->getStockIdForCurrentWebsite = $getStockIdForCurrentWebsite;
		$this->_productRepository = $productRepository;
		$this->getAssignedStockIdForWebsite = $getAssignedStockIdForWebsite;
		$this->websiteRepository = $websiteRepository;
		$this->transportBuilder = $transportBuilder;
		$this->storeManager = $storeManager;
		$this->inlineTranslation = $state;
		$this->getStockItemData = $getStockItemData;
	}

	/**
	 * Write to system.log
	 *
	 * @return void
	 */
	public function execute() {

		$subscriber = $this->_subscriberFactory->create();
		$collection = $subscriber->getCollection();
		foreach ($collection as $item) {
			$this->getStock($item);
		}
		$this->logger->info('Cron Works');
	}
	public function getStock($item) {

		$product = $this->getProductById($item->getProductId());

		$website_id = $item->getWebsiteId();
		$stockId = $this->getStockIdForWebsite($website_id);

		$productQtyInStock = $this->getProductSalableQty->execute($product->getSku(), $stockId);
		$isInStock = $productQtyInStock ? 1 : 0;
		$qtyAsked = $item->getQty();
		$this->logger->info('isInStock =>' . $isInStock);
		$this->logger->info('qtyAsked =>' . $qtyAsked);
		if ($isInStock && $productQtyInStock >= $qtyAsked) {
			if ($item->getSendCount() == 0) {
				$this->notifyCustomer($item, $product);
				$item->setSendCount(1);
				$item->save();
			}

		}

	}
	public function notifyCustomer($item, $product) {
		// this is an example and you can change template id,fromEmail,toEmail,etc as per your need.
		$templateId = 35; // template id
		$fromEmail = 'weborders@alogic.co'; // sender Email id
		$fromName = 'Admin'; // sender Name
		$toEmail = $item->getEmail(); // receiver email id

		try {
			// template variables pass here
			$templateVars = [
				'customerName' => $item->getFirstname(),
				'store_email' => 'store@email.com',
				'sku' => $product->getSku(),
			];

			$storeId = $this->storeManager->getStore()->getId();

			$from = ['email' => $fromEmail, 'name' => $fromName];
			$this->inlineTranslation->suspend();

			$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
			$templateOptions = [
				'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
				'store' => $storeId,
			];
			$transport = $this->transportBuilder->setTemplateIdentifier($templateId, $storeScope)
				->setTemplateOptions($templateOptions)
				->setTemplateVars($templateVars)
				->setFrom($from)
				->addTo($toEmail)
				->getTransport();
			$transport->sendMessage();
			$this->inlineTranslation->resume();
		} catch (\Exception $e) {
			$this->_logger->info($e->getMessage());
		}

		$this->logger->info('notifying =>' . $item->getEmail());
	}
	public function getProductById($id) {
		return $this->_productRepository->getById($id);
	}
	public function getStockIdForWebsite($websiteId) {
		$websiteCode = $this->getWebsiteCode($websiteId);
		$this->logger->info('Website Code=>' . $websiteCode);
		return $this->getAssignedStockIdForWebsite->execute($websiteCode);
	}
	public function getWebsiteCode($websiteId) {

		try {

			$website = $this->websiteRepository->getById($websiteId);

			return $websiteCode = $website->getCode();
		} catch (Exception $exception) {
			$this->logger->error($exception->getMessage());
		}
	}
}

