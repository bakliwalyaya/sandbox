<?php

namespace Alogic\Customtheme\Helper;

use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use \Magento\Framework\Pricing\Helper\Data as PriceHelperData;

class Data extends AbstractHelper {

	const XML_PATH_ZENDESK = 'zendesk_ticket/';
	const MODULE_SHOW_CART_CONFIG_PATH = 'alogic_store_switch/general/disable_cart';
	const DEFAULT_FREESHIPPING_RULE = 2;
	protected $storeManager;

	protected $scopeConfig;
	protected $_customerSession;
	protected $ruleFactory;
	protected $productFactory;
	protected $_productRepository;
	protected $_helperData;

	public function __construct(
		\Magento\Backend\Block\Template\Context $context,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Customer\Model\Session $customerSession,
		\Magento\SalesRule\Model\RuleFactory $ruleFactory,
		\Magento\Catalog\Model\ProductRepository $productRepository,
		ProductFactory $productFactory,
		PriceHelperData $helperData,
		ScopeConfigInterface $scopeConfig,
		array $data = []
	) {
		$this->scopeConfig = $scopeConfig;
		$this->ruleFactory = $ruleFactory;
		$this->_customerSession = $customerSession;
		$this->productRepository = $productRepository;
		$this->productFactory = $productFactory;
		$this->_helperData = $helperData;
		$this->storeManager = $storeManager;

	}

	public function getIsCartDisabled() {
		return $this->scopeConfig->getValue(self::MODULE_SHOW_CART_CONFIG_PATH, ScopeInterface::SCOPE_STORE,
			$this->getStoreId());
	}
	public function getStore() {
		return $this->storeManager->getStore();
	}
	public function getStoreId() {
		return $this->storeManager->getStore()->getId();
	}

	public function getWebsiteId() {
		return $this->storeManager->getStore()->getWebsiteId();
	}

	public function getConfigValue($field, $storeId = null) {
		return $this->scopeConfig->getValue(
			$field, ScopeInterface::SCOPE_STORE, $storeId
		);
	}

	public function getGeneralConfig($code, $storeId = null) {

		return $this->getConfigValue(self::XML_PATH_ZENDESK . 'general/' . $code, $storeId);
	}

	public function getShippingAbove() {
		$rule_id = self::DEFAULT_FREESHIPPING_RULE;
		$websiteId = $this->getWebsiteId();
		if ($websiteId == 1 || $websiteId == 2) {
			$rule_id = $rule_id;
		} elseif ($websiteId == 5) {
			$rule_id = 10;
		} elseif ($websiteId == 4) {
			$rule_id = 9;
		} elseif ($websiteId == 3) {
			$rule_id = 8;
		}
		$rule = $this->ruleFactory->create()->load($rule_id);
		$serializedConditions = $rule->getConditionsSerialized();
		$abovePrice = json_decode($serializedConditions)->conditions[0]->value;
		return $this->_helperData->currency($abovePrice, true, false);
	}

	public function getProductRule($_product) {
		$_product = $this->productRepository->get($_product->getSku());

		if ($ruleId = $_product->getProductRules()) {
			return $this->ruleFactory->create()->load($ruleId);
		}
		return false;
	}

	public function checkIfCouponValidForProduct($_product) {

		$groupId = $this->getGroupId();
		if (!$groupId) {
			$groupId = 0;
		}

		$websiteId = $this->getWebsiteId();

		$rule_discount = $this->getProductRule($_product);

		if ($rule_discount) {
			$item = $this->productFactory->create();
			$item->setProduct($_product);
			$isValid = $rule_discount->getActions()->validate($item);
			$isActive = $rule_discount->getData('is_active');

			return (in_array($groupId, $rule_discount['customer_group_ids'])
				&& in_array($websiteId, $rule_discount['website_ids'])
				&& $isValid
				&& $isActive
			);
		}

	}

	public function getGroupId() {
		if ($this->_customerSession->isLoggedIn()):
			return $customerGroup = $this->_customerSession->getCustomer()->getGroupId();
		endif;
	}

	/*SELECT `rule_coupons`.`code`
			FROM `salesrule` AS `main_table`
		 		INNER JOIN `salesrule_website` AS `website` ON website.website_id IN ('1') AND main_table.row_id = website.row_id
		 		INNER JOIN `salesrule_customer_group` AS `customer_group_ids` ON main_table.row_id = customer_group_ids.row_id AND customer_group_ids.customer_group_id = 0
		 		LEFT JOIN `salesrule_coupon` AS `rule_coupons` ON main_table.rule_id = rule_coupons.rule_id AND main_table.coupon_type != 1
	*/
}
