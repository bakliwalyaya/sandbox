<?php
namespace Alogic\Customtheme\Plugin;

/**
 * Class FinalPriceBox
 */

class FinalPriceBox {
	function aroundToHtml($subject, callable $proceed) {

		if (!$subject->getSaleableItem()->isSaleable()) {
			return '';
		} else {
			return $proceed();
		}

	}

}