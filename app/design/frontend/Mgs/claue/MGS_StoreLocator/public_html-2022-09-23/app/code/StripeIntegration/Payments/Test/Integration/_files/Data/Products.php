<?php

include_once dirname(__FILE__) . '/../Helper/Product.php';

use Magento\Catalog\Api\CategoryLinkManagementInterface;
use Magento\Catalog\Api\CategoryLinkRepositoryInterface;
use Magento\Catalog\Api\Data\CategoryInterfaceFactory;
use Magento\Catalog\Api\Data\ProductInterfaceFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Eav\Model\Config;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;

$objectManager = Bootstrap::getObjectManager();

$websiteRepository = $objectManager->get(WebsiteRepositoryInterface::class);
$baseWebsite = $websiteRepository->get('base');
$rootCategoryId = $baseWebsite->getDefaultStore()->getRootCategoryId();

$storeManager = $objectManager->get(StoreManagerInterface::class);

$defaultAttributeSet = $objectManager->get(Config::class)->getEntityType(Product::ENTITY)->getDefaultAttributeSetId();
$productRepository = $objectManager->get(ProductRepositoryInterface::class);
$categoryFactory = $objectManager->get(CategoryInterfaceFactory::class);

$categoryLinkRepository = $objectManager->create(
    CategoryLinkRepositoryInterface::class,
    [
        'productRepository' => $productRepository,
    ]
);

$categoryLinkManagement = $objectManager->get(CategoryLinkManagementInterface::class);
$reflectionClass = new \ReflectionClass(get_class($categoryLinkManagement));
$properties = [
    'productRepository' => $productRepository,
    'categoryLinkRepository' => $categoryLinkRepository,
];
foreach ($properties as $key => $value) {
    if ($reflectionClass->hasProperty($key)) {
        $reflectionProperty = $reflectionClass->getProperty($key);
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($categoryLinkManagement, $value);
    }
}

$category = $categoryFactory->create();
$category->isObjectNew(true);
$category->setId(3)
    ->setName('Products')
    ->setParentId(2)
    ->setPath('1/2/3')
    ->setLevel(2)
    ->setAvailableSortBy('name')
    ->setDefaultSortBy('name')
    ->setIsActive(true)
    ->setPosition(1)
    ->save();

$productInterfaceFactory = $objectManager->get(ProductInterfaceFactory::class);

$product = $productInterfaceFactory->create();
$product->setTypeId(Type::TYPE_SIMPLE)
    ->setAttributeSetId($defaultAttributeSet)
    ->setStoreId($storeManager->getDefaultStoreView()->getId())
    ->setWebsiteIds([$baseWebsite->getId()])
    ->setName('Simple Product')
    ->setSku('simple-product')
    ->setPrice(13)
    ->setStockData(['use_config_manage_stock' => 0])
    ->setVisibility(Visibility::VISIBILITY_BOTH)
    ->setStatus(Status::STATUS_ENABLED)
    ->save();

$product = $productRepository->save($product);

$categoryLinkManagement->assignProductToCategories(
    $product->getSku(),
    [$category->getId()]
);

$product = $productInterfaceFactory->create();
$product->setTypeId(Type::TYPE_VIRTUAL)
    ->setAttributeSetId($defaultAttributeSet)
    ->setStoreId($storeManager->getDefaultStoreView()->getId())
    ->setWebsiteIds([$baseWebsite->getId()])
    ->setName('Virtual Product')
    ->setSku('virtual-product')
    ->setPrice(8)
    ->setStockData(['use_config_manage_stock' => 0])
    ->setVisibility(Visibility::VISIBILITY_BOTH)
    ->setStatus(Status::STATUS_ENABLED)
    ->save();

$product = $productRepository->save($product);

$categoryLinkManagement->assignProductToCategories(
    $product->getSku(),
    [$category->getId()]
);

$product = $productInterfaceFactory->create();
$product->setTypeId(Type::TYPE_SIMPLE)
    ->setAttributeSetId($defaultAttributeSet)
    ->setStoreId($storeManager->getDefaultStoreView()->getId())
    ->setWebsiteIds([$baseWebsite->getId()])
    ->setName('Simple Monthly Subscription')
    ->setSku('simple-monthly-subscription-product')
    ->setPrice(10)
    ->setStockData(['use_config_manage_stock' => 0])
    ->setVisibility(Visibility::VISIBILITY_BOTH)
    ->setStatus(Status::STATUS_ENABLED)
    ->save();

$product = $productRepository->save($product);

setCustomAttribute($product, "stripe_sub_enabled", true);
setCustomAttribute($product, "stripe_sub_interval", "month");
setCustomAttribute($product, "stripe_sub_interval_count", 1);

$categoryLinkManagement->assignProductToCategories(
    $product->getSku(),
    [$category->getId()]
);

$product = $productInterfaceFactory->create();
$product->setTypeId(Type::TYPE_SIMPLE)
    ->setAttributeSetId($defaultAttributeSet)
    ->setStoreId($storeManager->getDefaultStoreView()->getId())
    ->setWebsiteIds([$baseWebsite->getId()])
    ->setName('Simple Quarterly Subscription')
    ->setSku('simple-quarterly-subscription-product')
    ->setPrice(10)
    ->setStockData(['use_config_manage_stock' => 0])
    ->setVisibility(Visibility::VISIBILITY_BOTH)
    ->setStatus(Status::STATUS_ENABLED)
    ->save();

$product = $productRepository->save($product);

setCustomAttribute($product, "stripe_sub_enabled", true);
setCustomAttribute($product, "stripe_sub_interval", "month");
setCustomAttribute($product, "stripe_sub_interval_count", 3);

$categoryLinkManagement->assignProductToCategories(
    $product->getSku(),
    [$category->getId()]
);

$product = $productInterfaceFactory->create();
$product->setTypeId(Type::TYPE_SIMPLE)
    ->setAttributeSetId($defaultAttributeSet)
    ->setStoreId($storeManager->getDefaultStoreView()->getId())
    ->setWebsiteIds([$baseWebsite->getId()])
    ->setName('Simple Monthly Subscription + Initial Fee')
    ->setSku('simple-monthly-subscription-initial-fee-product')
    ->setPrice(10)
    ->setStockData(['use_config_manage_stock' => 0])
    ->setVisibility(Visibility::VISIBILITY_BOTH)
    ->setStatus(Status::STATUS_ENABLED)
    ->save();

$product = $productRepository->save($product);

setCustomAttribute($product, "stripe_sub_enabled", true);
setCustomAttribute($product, "stripe_sub_interval", "month");
setCustomAttribute($product, "stripe_sub_interval_count", 1);
setCustomAttribute($product, "stripe_sub_initial_fee", 3);

$categoryLinkManagement->assignProductToCategories(
    $product->getSku(),
    [$category->getId()]
);

$product = $productInterfaceFactory->create();
$product->setTypeId(Type::TYPE_SIMPLE)
    ->setAttributeSetId($defaultAttributeSet)
    ->setStoreId($storeManager->getDefaultStoreView()->getId())
    ->setWebsiteIds([$baseWebsite->getId()])
    ->setName('Simple Trial Monthly Subscription')
    ->setSku('simple-trial-monthly-subscription-product')
    ->setPrice(10)
    ->setStockData(['use_config_manage_stock' => 0])
    ->setVisibility(Visibility::VISIBILITY_BOTH)
    ->setStatus(Status::STATUS_ENABLED)
    ->save();

$product = $productRepository->save($product);

setCustomAttribute($product, "stripe_sub_enabled", true);
setCustomAttribute($product, "stripe_sub_interval", "month");
setCustomAttribute($product, "stripe_sub_interval_count", 1);
setCustomAttribute($product, "stripe_sub_trial", 14);

$categoryLinkManagement->assignProductToCategories(
    $product->getSku(),
    [$category->getId()]
);

$product = $productInterfaceFactory->create();
$product->setTypeId(Type::TYPE_SIMPLE)
    ->setAttributeSetId($defaultAttributeSet)
    ->setStoreId($storeManager->getDefaultStoreView()->getId())
    ->setWebsiteIds([$baseWebsite->getId()])
    ->setName('Simple Trial Monthly Subscription + Initial Fee')
    ->setSku('simple-trial-monthly-subscription-initial-fee')
    ->setPrice(10)
    ->setStockData(['use_config_manage_stock' => 0])
    ->setVisibility(Visibility::VISIBILITY_BOTH)
    ->setStatus(Status::STATUS_ENABLED)
    ->save();

$product = $productRepository->save($product);

setCustomAttribute($product, "stripe_sub_enabled", true);
setCustomAttribute($product, "stripe_sub_interval", "month");
setCustomAttribute($product, "stripe_sub_interval_count", 1);
setCustomAttribute($product, "stripe_sub_trial", 14);
setCustomAttribute($product, "stripe_sub_initial_fee", 3);

$categoryLinkManagement->assignProductToCategories(
    $product->getSku(),
    [$category->getId()]
);

$product = $productInterfaceFactory->create();
$product->setTypeId(Type::TYPE_VIRTUAL)
    ->setAttributeSetId($defaultAttributeSet)
    ->setStoreId($storeManager->getDefaultStoreView()->getId())
    ->setWebsiteIds([$baseWebsite->getId()])
    ->setName('Virtual Monthly Subscription')
    ->setSku('virtual-monthly-subscription-product')
    ->setPrice(10)
    ->setStockData(['use_config_manage_stock' => 0])
    ->setVisibility(Visibility::VISIBILITY_BOTH)
    ->setStatus(Status::STATUS_ENABLED)
    ->save();

$product = $productRepository->save($product);

setCustomAttribute($product, "stripe_sub_enabled", true);
setCustomAttribute($product, "stripe_sub_interval", "month");
setCustomAttribute($product, "stripe_sub_interval_count", 1);

$categoryLinkManagement->assignProductToCategories(
    $product->getSku(),
    [$category->getId()]
);

$product = $productInterfaceFactory->create();
$product->setTypeId(Type::TYPE_VIRTUAL)
    ->setAttributeSetId($defaultAttributeSet)
    ->setStoreId($storeManager->getDefaultStoreView()->getId())
    ->setWebsiteIds([$baseWebsite->getId()])
    ->setName('Virtual Trial Monthly Subscription')
    ->setSku('virtual-trial-monthly-subscription-product')
    ->setPrice(10)
    ->setStockData(['use_config_manage_stock' => 0])
    ->setVisibility(Visibility::VISIBILITY_BOTH)
    ->setStatus(Status::STATUS_ENABLED)
    ->save();

$product = $productRepository->save($product);

setCustomAttribute($product, "stripe_sub_enabled", true);
setCustomAttribute($product, "stripe_sub_interval", "month");
setCustomAttribute($product, "stripe_sub_interval_count", 1);
setCustomAttribute($product, "stripe_sub_trial", 14);

$categoryLinkManagement->assignProductToCategories(
    $product->getSku(),
    [$category->getId()]
);

$product = $productInterfaceFactory->create();
$product->setTypeId(Type::TYPE_VIRTUAL)
    ->setAttributeSetId($defaultAttributeSet)
    ->setStoreId($storeManager->getDefaultStoreView()->getId())
    ->setWebsiteIds([$baseWebsite->getId()])
    ->setName('Virtual Trial Monthly Subscription + Initial Fee')
    ->setSku('virtual-monthly-subscription-initial-fee-product')
    ->setPrice(10)
    ->setStockData(['use_config_manage_stock' => 0])
    ->setVisibility(Visibility::VISIBILITY_BOTH)
    ->setStatus(Status::STATUS_ENABLED)
    ->save();

$product = $productRepository->save($product);

setCustomAttribute($product, "stripe_sub_enabled", true);
setCustomAttribute($product, "stripe_sub_interval", "month");
setCustomAttribute($product, "stripe_sub_interval_count", 1);
setCustomAttribute($product, "stripe_sub_trial", 14);
setCustomAttribute($product, "stripe_sub_initial_fee", 14);

$categoryLinkManagement->assignProductToCategories(
    $product->getSku(),
    [$category->getId()]
);
