<?php

namespace Amasty\AdvancedReview\Model\ResourceModel;

class Unsubscribe extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Model Initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('amasty_advanced_review_unsubscribe', 'entity_id');
    }
}
