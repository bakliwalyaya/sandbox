<?php

namespace Alogic\Customadmin\Controller\Adminhtml\Inventory;

use Magento\Backend\App\Action;

class Index extends Action {

	/**
	 * @var \Magento\Framework\View\Result\PageFactory
	 */
	protected $resultPageFactory;

	/**
	 * @param \Magento\Backend\App\Action\Context $context
	 * @param \Magento\Framework\View\Result\PageFactory resultPageFactory
	 */
	public function __construct(
		\Magento\Backend\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	) {
		parent::__construct($context);
		$this->resultPageFactory = $resultPageFactory;
	}

	/**
	 *
	 * @return \Magento\Backend\Model\View\Result\Page
	 */
	public function execute() {
		/** @var \Magento\Backend\Model\View\Result\Page $resultPage */
		$resultPage = $this->resultPageFactory->create();
		$resultPage->setActiveMenu('Alogic_Customadmin::stock_alert');
		$resultPage->addBreadcrumb(__('Alogic'), __('Alogic'));
		$resultPage->addBreadcrumb(__('Stock Alert'), __('stockalert'));
		// $resultPage->getConfig()->getTitle()->prepend(__(''));

		return $resultPage;
	}

	/*
		 * Check permission via ACL resource
	*/
	protected function _isAllowed() {
		return $this->_authorization->isAllowed('Alogic_Customadmin::stock_alert');
	}

}