<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_Preorder
 */


declare(strict_types=1);

namespace Amasty\Preorder\Model\Product;

class Constants
{
    const NOTE = 'amasty_preorder_note';
    const CART_LABEL = 'amasty_preorder_cart_label';
    const BACKORDERS_PREORDER_OPTION = 101;
}
