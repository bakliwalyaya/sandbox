<?php
/**
 * @category   Alogic
 * @package    Alogic_Resellers
 * @author     dushyant.joshi@alogic.co
 * @copyright  This file was generated by using Module Creator(http://code.vky.co.in/magento-2-module-creator/) provided by VKY <viky.031290@gmail.com>
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Alogic\Resellers\Model\ResourceModel\Resellers;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {
	protected $_idFieldName = 'id';
	/**
	 * Define model & resource model
	 */
	protected function _construct() {
		$this->_init(
			'Alogic\Resellers\Model\Resellers',
			'Alogic\Resellers\Model\ResourceModel\Resellers'
		);
	}
}