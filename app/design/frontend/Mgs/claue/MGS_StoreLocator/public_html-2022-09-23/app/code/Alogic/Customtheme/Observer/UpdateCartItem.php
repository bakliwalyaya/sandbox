<?php
namespace Alogic\Customtheme\Observer;
use Magento\Framework\DataObject;

class UpdateCartItem implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
    	$writer = new \Zend_Log_Writer_Stream(BP . '/var/log/UpdateCartItem.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $items = $observer->getCart()->getQuote()->getItems();
        $info = $observer->getInfo()->getData();

        $skuIris = '';
        $skuIrisQty = '';
        $skuClarityQty = '';
        $itemIdCurrent = key($info);
        $item = $observer->getCart()->getQuote()->getItemById($itemIdCurrent);
        
        $sku = $item->getSku();
        $qty = $info[$item->getId()]['qty'];
        $storeId = $observer->getCart()->getQuote()->getStoreId();
        if($info[$item->getId()]['qtyplusminus'] == 'plus'){
            $qty = $qty + 1;
        }
        if($info[$item->getId()]['qtyplusminus'] == 'minus'){
            $qty = $qty - 1;
        }
        
        if($storeId == 35){
	        if ($sku == 'IUWA09') {
	            foreach ($items as $item) {
	                $skui = $item->getSku();
	                if ($skui == '27F34KCPD') {
	                    $skuClarityQty = $item->getQty();
	                }
	                if ($skui == 'IUWA09') {
	                    $skuIrisQty = $qty;
	                }

	                

	            }

	
	            if ($skuClarityQty != '') {
	                if ($skuIrisQty > $skuClarityQty) {
	                    throw new \Magento\Framework\Exception\LocalizedException(
	                        __('Please increase the quantity of Clarity 27" UHD 4K Monitor.')
	                    );
	                }
	            }
	        }elseif ($sku == '27F34KCPD') {
	            foreach ($items as $item) {
	                $skui = $item->getSku();
	               
	                if ($skui == '27F34KCPD') {
	                    $skuClarityQty = $qty;
	                }
	                if ($skui == 'IUWA09') {
	                    $skuIrisQty = $qty;

	                }
	            }
	            $paramsIris = [
	                'qty' => $skuIrisQty,
	                'reset_count' => 1,
	                'options' => [],
	                'id' => $item->getData('item_id')
	            ];
	            $itemIris = $observer->getCart()->updateItem($item->getData('item_id'), new DataObject($paramsIris));
	            if (is_string($itemIris)) {
	                throw new LocalizedException(__($itemIris));
	            }
	            if ($itemIris->getHasError()) {
	                throw new LocalizedException(__($itemIris->getMessage()));
	            }
	           
	        }
    	}
		$logger->info(' Item Id key' . key($info));
        $logger->info(print_r($info, true));
        //throw new \Magento\Framework\Exception\LocalizedException(
					//__('The order does not allow an invoice to be created.')
				//);
    }
}