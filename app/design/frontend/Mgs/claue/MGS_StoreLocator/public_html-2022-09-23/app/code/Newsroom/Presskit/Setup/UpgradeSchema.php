<?php
namespace Newsroom\Presskit\Setup;

/**
 * Class UpgradeSchema
 */
class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface {
	public function upgrade(
		\Magento\Framework\Setup\SchemaSetupInterface $setup,
		\Magento\Framework\Setup\ModuleContextInterface $context
	) {
		$installer = $setup;
		$installer->startSetup();
		//START: install stuff
		//END:   install stuff

		//START table setup
		$table = $installer->getConnection()->newTable(
			$installer->getTable('newsroom_presskit_image')
		)->addColumn(
			'image_id',
			\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
			null,
			['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true],
			'Entity ID'
		)->addColumn(
			'presskit_id',
			\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
			null,
			['nullable' => false, 'primary' => false, 'unsigned' => true],
			'Press ID'
		)->addColumn(
			'images',
			\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
			'2M',
			['nullable' => true, 'default' => null],
			'Content'
		)->addForeignKey(
			$setup->getFkName(
				'newsroom_presskit_image',
				'presskit_id',
				'newsroom_presskit',
				'presskit_id'
			),
			'presskit_id',
			$setup->getTable('newsroom_presskit'),
			'presskit_id',
			\Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
		);
		$installer->getConnection()->createTable($table);
		//END   table setup
		$installer->endSetup();
	}
}
