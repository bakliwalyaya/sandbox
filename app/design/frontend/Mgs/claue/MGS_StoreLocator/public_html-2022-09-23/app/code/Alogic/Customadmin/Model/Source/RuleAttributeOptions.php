<?php
namespace Alogic\Customadmin\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\SalesRule\Model\ResourceModel\Rule\Collection;
class RuleAttributeOptions extends AbstractSource
{
    /**
     * @var \Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory
    */
    protected $ruleCollectionFactory;

     /**     
     * @param \Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory     
     */
    public function __construct(
        \Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory $ruleCollectionFactory,
    ) {
        $this->ruleCollectionFactory = $ruleCollectionFactory;
    }

    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $collection = $this->ruleCollectionFactory->create();
        foreach ($collection->getItems() as $ruleModel) {
            $ruleModel->load($ruleModel->getId());
            echo "<pre>";
            print_r($ruleModel->getData());
            die();
        }
        if (null === $this->_options) {
            $this->_options=[
                                ['label' => __('Option 1'), 'value' => 1],
                                ['label' => __('Option 2'), 'value' => 2],
                                ['label' => __('Option 3'), 'value' => 3]
                            ];
        }
        return $this->_options;
    }
}
