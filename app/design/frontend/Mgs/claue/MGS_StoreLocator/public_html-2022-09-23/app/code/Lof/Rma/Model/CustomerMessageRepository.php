<?php
/**
 * LandOfCoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://www.landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   LandOfCoder
 * @package    Lof_Rma
 * @copyright  Copyright (c) 2020 Landofcoder (http://www.LandOfCoder.com/)
 * @license    http://www.LandOfCoder.com/LICENSE-1.0.html
 */

namespace Lof\Rma\Model;

use Lof\Rma\Api\Repository\CustomerMessageRepositoryInterface;
use Lof\Rma\Api\Data\MessageSearchResultsInterfaceFactory;
use Lof\Rma\Api\Data\MessageInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Lof\Rma\Model\ResourceModel\Message as ResourceMessage;
use Lof\Rma\Model\ResourceModel\Message\CollectionFactory as MessageCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

class CustomerMessageRepository implements CustomerMessageRepositoryInterface
{

    protected $resource;

    protected $messageFactory;

    protected $messageCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataMessageFactory;

    private $storeManager;


    /**
     * @param ResourceMessage $resource
     * @param MessageFactory $messageFactory
     * @param MessageInterfaceFactory $dataMessageFactory
     * @param MessageCollectionFactory $messageCollectionFactory
     * @param MessageSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceMessage $resource,
        MessageFactory $messageFactory,
        MessageInterfaceFactory $dataMessageFactory,
        MessageCollectionFactory $messageCollectionFactory,
        MessageSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->messageFactory = $messageFactory;
        $this->messageCollectionFactory = $messageCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataMessageFactory = $dataMessageFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    public function create()
    {
        return $this->messageFactory->create();
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        $customerId,
        \Lof\Rma\Api\Data\MessageInterface $message
    ) {
        if(!$customerId){
            throw new NoSuchEntityException(__('You should login with your account.'));
        }
        try {
            $messageCustomerId = $message->getCustomerId();
            if($customerId == $messageCustomerId){
                $message->getResource()->save($message);
            }else {
                throw new CouldNotSaveException(__(
                    'Could not save the message because wrong Customer ID'
                ));
            }
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the message: %1',
                $exception->getMessage()
            ));
        }
        return $message;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($customerId, $messageId)
    {
        if(!$customerId){
            throw new NoSuchEntityException(__('You should login with your account.'));
        }
        $message = $this->messageFactory->create();
        $message->getResource()->load($message, $messageId);
        if (!$message->getId()) {
            throw new NoSuchEntityException(__('message with id "%1" does not exist.', $messageId));
        }
        $messageCustomerId = $message->getCustomerId();
        if($customerId != $messageCustomerId){
            throw new NoSuchEntityException(__('rma message with id "%1" does not exist for this Customer.', $messageId));
        }
        return $message;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        $customerId, 
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        if(!$customerId){
            throw new NoSuchEntityException(__('You should login with your account.'));
        }
        $collection = $this->messageCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        //Add filter for this customer ID
        $collection->addFieldToFilter("customer_id", $customerId);
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());
        return $searchResults;
    }
}
