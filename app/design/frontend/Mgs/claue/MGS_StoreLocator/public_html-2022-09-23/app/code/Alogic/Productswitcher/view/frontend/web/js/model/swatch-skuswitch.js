define([
    'jquery',
    'mage/utils/wrapper'
], function ($, wrapper) {
	'use strict';
 
    return function(targetModule){
        console.log("in targetModule");
        var updatePrice = targetModule.prototype._UpdatePrice;
        targetModule.prototype.configurableSku = $('div.product-info-main .sku .value').html();
        targetModule.prototype.configurableBarcode = $('div.product-info-main .part-no .value').html();
        targetModule.prototype.configurableName = $('div.product-info-main .product-name').html();

        var updatePriceWrapper = wrapper.wrap(updatePrice, function(original){
            var allSelected = true;
            for(var i = 0; i<this.options.jsonConfig.attributes.length;i++){
                if (!$('div.product-info-main .product-options-wrapper .swatch-attribute.' + this.options.jsonConfig.attributes[i].code).data('option-selected')){
                	allSelected = false;
                }
            }
            var simpleSku = this.configurableSku;
            var simpleBarcode = this.configurableBarcode;
            var simpleName = this.configurableName;
            var simpleQty = '';
            

            if (allSelected){
                var products = this._CalcProducts();
                
                /*simpleSku = this.options.jsonConfig.skus[products.slice().shift()];
                simpleBarcode = this.options.jsonConfig.barcodes[products.slice().shift()];
                simpleName = this.options.jsonConfig.names[products.slice().shift()];*/
                simpleQty = this.options.jsonConfig.qtys[products.slice().shift()];
            }

            
            $('div.product-info-main .box-tocart').show();
            
            $('#outofstock').hide();
            
            if(simpleQty.qty == 0){
                
                $('div.product-info-main .box-tocart').hide();
                $('#outofstock').show();
                //$('div.product-info-main').html('<div id="outofstock" class="out-stock">Sold Out</div>');
            }
            
            $('div.product-info-main .sku .value').html(simpleSku);
            
            $('div.product-info-main .part-no .value').html(simpleBarcode);
            $('div.product-info-main .product-name').html(simpleName);
              return original();
        });
 
        targetModule.prototype._UpdatePrice = updatePriceWrapper;
        return targetModule;
	};
});