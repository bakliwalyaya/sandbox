<?php

namespace Lof\Rma\Controller\Adminhtml\Report;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;

class Detail extends Action
{
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Lof_Rma::rma_report_detail';

    /**
     * Detail constructor.
     *
     * @param \Magento\Framework\Registry         $registry
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        Registry $registry,
        Context $context
    ) {

        $this->registry         = $registry;
        $this->context          = $context;
        $this->backendSession   = $context->getSession();

        parent::__construct($context);
    }

    /**
     * @param \Magento\Backend\Model\View\Result\Page\Interceptor $resultPage
     * @return \Magento\Backend\Model\View\Result\Page\Interceptor
     */
    protected function initPage($resultPage)
    {
        $resultPage->setActiveMenu('Lof_Rma::rma_report');
        $resultPage->getConfig()->getTitle()->prepend(__('Rma'));
        $resultPage->getConfig()->getTitle()->prepend(__('RMA Detail Reports'));
        return $resultPage;
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page\Interceptor $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $this->initPage($resultPage);
        return $resultPage;
    }
}
