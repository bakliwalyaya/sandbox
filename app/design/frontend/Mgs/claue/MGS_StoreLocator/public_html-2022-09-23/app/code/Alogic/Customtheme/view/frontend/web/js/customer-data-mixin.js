/**
 * trunglv - Fixing bugs
 * 
 */
define([
    'jquery',
    'mage/utils/wrapper'
], function ($, wrapper) {
    'use strict';

    var mixin = {
        /**
         * @param {Function} originFn - Original method.
         * @return {Array}
         */
        getExpiredSectionNames: function (originFn) {
            var expiredSectionNames = originFn(),
                storage = $.initNamespaceStorage('mage-cache-storage').localStorage,            
                sectionData;
            
            /* to add more if you want. It temporary fix for customer data on local storage */
            var sectionNames = ['customer'];
            
            _.each(sectionNames, function (sectionName) {
                
                sectionData = storage.get(sectionName);
                
                if(typeof sectionData === 'undefined'){
                        expiredSectionNames.push(sectionName);
                }
                
            });
            return expiredSectionNames;
        }
    };

    /**
     * Override default customer-data.getExpiredSectionNames().
     */
    return function (target) {
        return wrapper.extend(target, mixin);
    };
});