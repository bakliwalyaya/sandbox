<?php

namespace Newsroom\Presskit\Model\ResourceModel\Image;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Newsroom\Presskit\Model\Image;
use Newsroom\Presskit\Model\ResourceModel\Image as ResourceModelImage;

class Collection extends AbstractCollection {
	protected function _construct() {
		$this->_init(Image::class, ResourceModelImage::class);
	}
}