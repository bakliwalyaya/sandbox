<?php
namespace Alogic\Stockalerts\Model\ResourceModel;


class Subscriber extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}
	
	protected function _construct()
	{
		$this->_init('alogic_alert_stock', 'alert_stock_id');
	}
	
}

