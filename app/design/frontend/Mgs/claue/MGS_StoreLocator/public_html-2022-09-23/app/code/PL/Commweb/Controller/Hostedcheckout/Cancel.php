<?php
/**
 * PL Development.
 *
 * @category    PL
 * @author      Linh Pham <plinh5@gmail.com>
 * @copyright   Copyright (c) 2016 PL Development. (http://www.polacin.com)
 */
namespace PL\Commweb\Controller\Hostedcheckout;

class Cancel extends \PL\Commweb\Controller\Hostedcheckout
{
    protected $hostedCheckout;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \PL\Commweb\Helper\Data $mpgsHelper,
        \PL\Commweb\Logger\Logger $plLogger,
        \PL\Commweb\Model\HostedCheckout $hostedCheckout
    ) {
        parent::__construct(
            $context,
            $orderFactory,
            $checkoutSession,
            $storeManager,
            $mpgsHelper,
            $plLogger
        );
        $this->hostedCheckout = $hostedCheckout;
    }

    public function execute()
    {
        $incrementId = $this->checkoutSession->getLastRealOrderId();
        $order = $this->orderFactory->create()->loadByIncrementId($incrementId);
        $this->messageManager->addError(__('You have cancelled the order. Please try again'));
        $this->hostedCheckout->cancelTransaction($order);
        $this->_redirect('checkout/cart');
    }
}