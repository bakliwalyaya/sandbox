define(
    [
        'Amasty_Checkout/js/model/resource-url-manager',
        'Magento_Checkout/js/model/totals',
        'Magento_Checkout/js/model/quote',
        'mage/storage',
        'Magento_Checkout/js/model/error-processor',
        'Amasty_Checkout/js/action/recollect-shipping-rates',
        'Magento_Checkout/js/model/payment-service',
        'Magento_Checkout/js/model/payment/method-converter',
        'Magento_Customer/js/customer-data',
        'Amasty_Checkout/js/action/update-items-content',
        'jquery',
    ],
    function (
        resourceUrlManager,
        totals,
        quote,
        storage,
        errorProcessor,
        recollectShippingRates,
        paymentService,
        methodConverter,
        customerData,
        updateItemsContent,
        $
    ) {
        "use strict";

         /**
             *
             * @param item
             * @return {*}
             */
            function getItemFromQuote(itemId) {
                var items = quote.getItems();
                var quoteItems = items.filter(function (quoteItem) {
                    return quoteItem.item_id == itemId;
                });

                if (quoteItems.length == 0) {
                    return false;
                }

                return quoteItems[0];
            }

        return function (itemId, formData, itemQty) {
            if (totals.isLoading()) {
                return;
            }

            totals.isLoading(true);
            
            storage.post(
                resourceUrlManager.getUrlForUpdateItem(quote),
                JSON.stringify({
                    itemId: itemId,
                    formData: formData,
                    testd: 'productId'
                }), false
            ).done(
                function (result) {
                    if (!result) {
                        window.location.reload();
                    }

                    recollectShippingRates();

                    paymentService.setPaymentMethods(methodConverter(result.payment));
                    customerData.reload(['cart']);
                    totals.isLoading(false);
                    updateItemsContent(result.totals);
                }
            ).fail(

                function (response) {
                    console.log(response.responseJSON.message);
                    if(response.responseJSON.message == 'Please increase the quantity of Clarity 27" UHD 4K Monitor.'){
                        var Item = getItemFromQuote(itemId);

                        
                        var items = quote.getItems();
                        items.filter(function (quoteItem) {
                            console.log(quoteItem.sku,quoteItem.qty);
                            if(quoteItem.sku == '27F34KCPD'){
                                var inputItemId = document.getElementById('cart-item-'+itemId+'-qty');
                                if(inputItemId){

                                    inputItemId.value = itemQty;
                                    //document.getElementById('cart-item-'+itemId+'-qty').setAttribute('value', quoteItem.qty);
                                }  
                            }
                        });                        
                    }
                    
                    errorProcessor.process(response);
                    document.getElementById('monitor-error').style.display = "block";
                    $("#monitor-error").delay(8000).fadeOut('slow');   
                    $('[data-role="checkout-messages"]').hide();                                    
                    totals.isLoading(false);
                }
            );
        };
    }
);
