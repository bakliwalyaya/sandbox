<?php
/**
 * PL Development.
 *
 * @category    PL
 * @author      Linh Pham <plinh5@gmail.com>
 * @copyright   Copyright (c) 2016 PL Development. (http://www.polacin.com)
 */
namespace PL\Commweb\Block\Form;


class HostedCheckout extends \Magento\Payment\Block\Form
{
    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var  \Magento\Checkout\Model\Order
     */
    protected $order;

    protected $countryFactory;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Directory\Model\CountryFactory $countryFactory,

        array $data = []
    ) {
        $this->orderFactory = $orderFactory;
        $this->checkoutSession = $checkoutSession;
        $this->countryFactory = $countryFactory;
        parent::__construct($context, $data);
        $this->getOrder();
    }

    /**
     * @return $this
     */
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getMethodInstance()
    {
        try {
            $order = $this->order;
            if ($order->getPayment()) {
                $method = $this->order->getPayment()->getMethodInstance();
                return $method;
            }
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException($e->getMessage());
        }
    }
    public function getHostedCheckoutSessionId()
    {
        try {
            $order = $this->order;
            if ($order->getPayment()) {
                $session = $this->order->getPayment()->getMethodInstance()->getHostedCheckoutSessionId();
                return $session;
            }
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException($e->getMessage());
        }
    }

    public function getCheckoutSuccessUrl()
    {
        try {
            $order = $this->order;
            if ($order->getPayment()) {
                $url = $this->order->getPayment()->getMethodInstance()->getCheckoutSuccessUrl();
                return $url;
            }
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException($e->getMessage());
        }
    }

    public function getOrderId()
    {
        return $this->checkoutSession->getLastRealOrderId();
    }
    /**
     * @return \Magento\Checkout\Model\Order|\Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        if (!$this->order) {
            $incrementId = $this->checkoutSession->getLastRealOrderId();
            $this->order = $this->orderFactory->create()->loadByIncrementId($incrementId);
        }
        return $this->order;
    }

    /**
     * @return \Magento\Checkout\Model\Session
     */
    protected function getCheckout()
    {
        return $this->checkoutSession;
    }

    public function getConfigData($name)
    {
        try {
            $order = $this->order;
            if ($order->getPayment()) {
                $url = $this->order->getPayment()->getMethodInstance()->getConfigData($name);
                return $url;
            }
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException($e->getMessage());
        }
    }

    public function getCountryCodeIso3()
    {
        return $this->countryFactory->create()
            ->load($this->getCheckout()->getCountryId())
            ->getIso3Code();
    }
}