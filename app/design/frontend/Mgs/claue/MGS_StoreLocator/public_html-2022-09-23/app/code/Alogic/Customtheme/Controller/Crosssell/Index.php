<?php
namespace Alogic\Customtheme\Controller\Crosssell;
use Magento\Checkout\Model\Session;
use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action {

	protected $resultPageFactory;
	protected $productRepository;
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		Session $session,
		\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
		ResultFactory $resultFactory,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory) {
		$this->_session = $session;
		$this->productRepository = $productRepository;
		$this->resultFactory = $resultFactory;
		$this->resultPageFactory = $resultPageFactory;
		parent::__construct($context);
	}

	public function execute() {
		$items = $this->_session->getQuote()->getAllVisibleItems();
		$resultPage = $this->resultFactory->create(ResultFactory::TYPE_LAYOUT);

		$block = $resultPage->getLayout()->getBlock('alogic_customtheme_block_crosssell');
		$quote = $this->_session->getQuote();
		$crossSells = [];
		foreach ($items as $_item) {
			$crossSells[] = $this->getCrosssellProductsList($_item->getProductId());
		}

		$items = [];

		foreach ($crossSells as $products) {
			foreach ($products as $product) {
				if ($product->isSaleable() && $product->getTypeId() == 'simple') {
					$items[] = $product;
				}

			}

		}

		$block->setData('quote_items', $items);

		return $resultPage;
		return $this->resultPageFactory->create();
	}
	public function getCrosssellProductsList($productId) {
		//$productId = 4451;

		$crossSellProduct = [];
		try {
			$product = $this->productRepository->getById($productId);
			$crossSell = $product->getCrossSellProducts();

			if (count($crossSell)) {
				foreach ($crossSell as $productItem) {
					$crossSellProduct[] = $this->productRepository->getById($productItem->getId());
				}
			}
		} catch (\Exception $exception) {
			//$this->logger->error($exception->getMessage());
		}

		return $crossSellProduct;
	}
}
