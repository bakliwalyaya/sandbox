<?php

namespace Alogic\Helppages\Controller\Index;

require_once BP . '/lib/sns/vendor/autoload.php';

use Aws\Exception\AwsException;
use Aws\Sns\SnsClient;


class Index extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\SalesRule\Model\RuleFactory $ruleFactory,
        \Magento\SalesRule\Model\Converter\ToDataModel $toDataModelConverter,
        \Magento\Framework\Serialize\Serializer\Json $serializer = null
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->ruleFactory = $ruleFactory;
        $this->toDataModelConverter = $toDataModelConverter;
        $this->serializer = $serializer ?: \Magento\Framework\App\ObjectManager::getInstance()->get(
            \Magento\Framework\Serialize\Serializer\Json::class
        );
        parent::__construct($context);
    }

    public function execute()
    {

        try {
            $ruleCollection = $this->ruleFactory->create()->getCollection()
                ->addFieldToFilter('product_rule', 1)
                ->addFieldToFilter('rule_id', ['eq' => 20]);
            foreach ($ruleCollection as $rule) {
                echo "<pre>11";
                $couponCode = 'TAKE30';
                $ruleActions = $rule->getActions()->asArray();
                print_r($rule->getData());
                $skus = 'UP2QC10AWM-SGR,UP2QC10A-SGR,SSULHD01-SGR,SSULHDXX-SGR,TB4H3TB,ULACMN-SGR,MSP31CS15WAU';
                $ruleActions['conditions'][0]['value'] = $skus;
                $rule->setActionsSerialized($this->serializer->serialize($ruleActions));
                $rule->setCode($couponCode);
                $rule->setCouponCode($couponCode);
                $rule->setActions(null);
                $rule->save();
            }
        } catch (AwsException $e) {
            // output error message if fails
            echo $e->getMessage() . "<br>";
        }

        $resultPageFactory = $this->resultPageFactory->create();
        $resultPageFactory->getConfig()->getTitle()->set(__('Help'));

        return $resultPageFactory;
    }

    public function executeOld()
    {

        /** AWS SNS Access Key ID */
        $access_key_id = 'AKIAVBQFVFOOGSTRDGOZ';

        /** AWS SNS Secret Access Key */
        $secret = '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';

        /** Create SNS Client By Passing Credentials */
        $SnSclient = new SnsClient([
            'region' => 'ap-south-1',
            'version' => 'latest',
            'credentials' => [
                'key' => $access_key_id,
                'secret' => $secret,
            ],
        ]);

        /** Message data & Phone number that we want to send */
        $message = 'Testing AWS SNS Messaging';

        /** NOTE: Make sure to put the country code properly else SMS wont get delivered */
        $phone = '+918369346804';

        try {
            /** Few setting that you should not forget */
            $result = $SnSclient->publish([
                'MessageAttributes' => array(
                    /** Pass the SENDERID here */
                    'AWS.SNS.SMS.SenderID' => array(
                        'DataType' => 'String',
                        'StringValue' => 'StackCoder',
                    ),
                    /** What kind of SMS you would like to deliver */
                    'AWS.SNS.SMS.SMSType' => array(
                        'DataType' => 'String',
                        'StringValue' => 'Transactional',
                    ),
                ),
                /** Message and phone number you would like to deliver */
                'Message' => $message,
                'PhoneNumber' => $phone,
            ]);
            /** Dump the output for debugging */
            echo '<pre>';
            print_r($result);
            echo '<br>--------------------------------<br>';
        } catch (AwsException $e) {
            // output error message if fails
            echo $e->getMessage() . "<br>";
        }

        $resultPageFactory = $this->resultPageFactory->create();
        $resultPageFactory->getConfig()->getTitle()->set(__('Help'));

        return $resultPageFactory;
    }
}
