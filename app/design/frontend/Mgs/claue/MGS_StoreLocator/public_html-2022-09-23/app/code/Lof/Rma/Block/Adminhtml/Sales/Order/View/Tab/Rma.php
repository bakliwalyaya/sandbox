<?php
/**
 * LandOfCoder
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Landofcoder.com license that is
 * available through the world-wide-web at this URL:
 * http://www.landofcoder.com/license-agreement.html
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category   LandOfCoder
 * @package    Lof_Rma
 * @copyright  Copyright (c) 2020 Landofcoder (http://www.LandOfCoder.com/)
 * @license    http://www.LandOfCoder.com/LICENSE-1.0.html
 */



namespace Lof\Rma\Block\Adminhtml\Sales\Order\View\Tab;

use Magento\Backend\Block\Widget;
use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Lof\Rma\Helper\Data as RmaHelper;

class Rma extends Widget implements TabInterface
{
    protected $rmaHelper;
    protected $orderRepository;

    public function __construct(
        RmaHelper $rmaHelper,
        OrderRepositoryInterface  $orderRepository,
        Context $context,
        array $data = []
    ) {
        $this->rmaHelper = $rmaHelper;
        $this->orderRepository       = $orderRepository;
        parent::__construct($context, $data);
    }

     /**
     * @return boolean
     */
    public function IsItemsQtyAvailable($order){
        $items = $order->getAllItems();
        foreach ($items as  $item) {
            if($item->getData('base_row_total') <=0||$item->getData('product_type') == 'bundle')  continue; 
            if($this->rmaHelper->getItemQuantityAvaiable($item)>0){
                return true;
            }
        }  
        return false;    
    }

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('RMA');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('RMA');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function _toHtml()
    {
        $id = $this->getRequest()->getParam('order_id');
        $rmaNewUrl = $this->getUrl('rma/rma/add', ['order_id' => $id]);
        $button = $this->getLayout()->createBlock('\Magento\Backend\Block\Widget\Button')
            ->setClass('add')
            ->setType('button')
            ->setOnClick('window.location.href=\'' . $rmaNewUrl . '\'')
            ->setLabel(__('Create RMA for this order'));
        $button_html = $button->toHtml();

        $grid = $this->getLayout()->createBlock('\Lof\Rma\Block\Adminhtml\Rma\Grid');
        $grid->setId('rma_grid_internal');
        $grid->setActiveTab('RMA');
        $grid->addCustomFilter('order_id', $id);
        $grid->setFilterVisibility(false);
        $grid->setExportVisibility(false);
        $grid->setPagerVisibility(0);

        $grid->setTabMode(true);
        $order = $this->orderRepository->get($id);
        if ($this->IsItemsQtyAvailable($order)) {
            $meetMessage = __('Order meets RMA policy');
        } else {
            $meetMessage = __('Order doesn\'t meet RMA policy');
            $button_html = "";
        }

        return '<br>
        <div>' . $button_html . '<div style="float:right;color:#eb5e00"><i>' . $meetMessage . '</i></div>
        <br><br>' . $grid->toHtml() . '</div>';
    }
}
