<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2021 Amasty (https://www.amasty.com)
 * @package Amasty_PreOrderRelease
 */


declare(strict_types=1);

namespace Amasty\PreOrderRelease\Model;

use Amasty\PreOrderRelease\Model\Source\ReleaseDateFormat;

class ConfigProvider extends \Amasty\Preorder\Model\ConfigProvider
{
    const RELEASE_ORDER_STATUSES = 'release_notification/order_status';
    const RELEASE_EMAIL_TEMPLATE = 'release_notification/email_template';
    const RELEASE_EMAIL_SENDER = 'release_notification/email_sender';
    const RELEASE_DATE_ENABLED = 'release_date/enabled';
    const RELEASE_DATE_ATTRIBUTE = 'release_date/attribute';
    const RELEASE_DATE_FORMAT = 'release_date/format';
    const RELEASE_CHANGE_BACKORDERS = 'release_date/change_backorders';
    const RELEASE_NOTE_STATE = 'release_date/note_state';

    public function getReleaseEmailTemplate(int $storeId): string
    {
        return (string) $this->getValue(self::RELEASE_EMAIL_TEMPLATE, $storeId);
    }

    public function getReleaseEmailSender(int $storeId): string
    {
        return (string) $this->getValue(self::RELEASE_EMAIL_SENDER, $storeId);
    }

    public function getReleaseOrderStatuses(): array
    {
        return explode(',', (string) $this->getValue(self::RELEASE_ORDER_STATUSES));
    }

    public function isReleaseDateEnabled(): bool
    {
        return $this->isSetFlag(self::RELEASE_DATE_ENABLED);
    }

    public function getReleaseDateAttribute(): string
    {
        return (string) $this->getValue(self::RELEASE_DATE_ATTRIBUTE);
    }

    public function getReleaseDateFormat(): ?string
    {
        $releaseDateFormat = (string) $this->getValue(self::RELEASE_DATE_FORMAT);

        return $releaseDateFormat !== ReleaseDateFormat::DEFAULT_FORMAT
            ? $releaseDateFormat
            : null;
    }

    public function getNewBackordersValue(): int
    {
        return (int) $this->getValue(self::RELEASE_CHANGE_BACKORDERS);
    }

    public function getReleaseNoteState(): int
    {
        return (int) $this->getValue(self::RELEASE_NOTE_STATE);
    }
}
