<?xml version="1.0"?>
<!--
/**
 * Copyright © 2016 Wyomind. All rights reserved.
 * https://www.wyomind.com
 */
-->
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:module:Magento_Config:etc/system_file.xsd">
    <system>
        <tab id="wyomind" translate="label" sortOrder="1000000">
            <label><![CDATA[<span class='wyomind-logo'>Wyomind</span>]]></label>
        </tab>
        <section id="catalog">
            <group id="search">
                <field id="engine" translate="label" type="select" sortOrder="19" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Search Engine</label>
                    <source_model>Magento\Search\Model\Adminhtml\System\Config\Source\Engine</source_model>
                </field>
                <group id="elasticsearch" translate="label" showInDefault="1" showInWebsite="1" showInStore="1" sortOrder="40">
                    <label>Elasticsearch Configuration</label>
                    <depends>
                        <field id="engine">elasticsearch</field>
                    </depends>
                    <field id="enable_debug_mode" translate="label comment" type="select" sortOrder="50" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Enable Debug Mode</label>
                        <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                    </field>
                    
                    <field id="version" translate="label comment" type="text" sortOrder="50" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Elasticsearch version</label>
                        <frontend_model>Wyomind\Elasticsearch\Block\Adminhtml\System\Config\Form\Field\Version</frontend_model>
                        <comment><![CDATA[Save the configuration to refresh this value]]></comment>
                    </field>
                    <field id="servers" translate="label comment" type="textarea" sortOrder="70" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Servers</label>
                        <frontend_model>Wyomind\Elasticsearch\Block\Adminhtml\System\Config\Form\Field\Servers</frontend_model>
                        <comment><![CDATA[host:port separated by comma. Follow this pattern for full available parameters: <a href="http://php.net/manual/en/function.parse-url.php" target="_blank">http://php.net/manual/en/function.parse-url.php</a>]]></comment>
                        <validate>required-entry</validate>
                    </field>
                    <field id="verify_host" translate="label comment" type="select" sortOrder="80" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Verify Host</label>
                        <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                        <comment><![CDATA[Only used for https connection. Useful to set No here if you don't have a valid SSL certificate.]]></comment>
                    </field>
                    <field id="timeout" translate="label comment" type="text" sortOrder="90" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Connect Timeout</label>
                        <comment><![CDATA[Connection timeout in seconds.]]></comment>
                        <validate>required-entry validate-greater-than-zero validate-number</validate>
                    </field>
                    <field id="index_prefix" translate="label comment" type="text" sortOrder="100" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Index Prefix</label>
                        <comment><![CDATA[Used to prefix index names to avoid potential collisions.]]></comment>
                        <validate>validate-code</validate>
                    </field>
                    <field id="index_settings" translate="label comment" type="textarea" sortOrder="110" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Index Settings</label>
                        <comment><![CDATA[Having more shards enhances the indexing performance and allows to distribute a big index across machines.<br>The number of replicas each shard has. Having more replicas enhances the search performance and improves the cluster availability.<br /><strong><span style="color: red;">Be careful</span></strong> when modifying this parameter. Write consistency (one, quorum or all) must be considered in order to avoid timeout write action. More info here: <a href="https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html#bulk-consistency" target="_blank">https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html#bulk-consistency</a> and here <a href="https://github.com/elasticsearch/elasticsearch/issues/444" target="_blank">https://github.com/elasticsearch/elasticsearch/issues/444</a>]]></comment>
                        <tooltip><![CDATA[<strong>Default write consistency is quorum</strong> (active shards > replicas / 2 + 1).<br />For example, in a N shards with 2 replicas index, there will have to be at least 2 active shards within the relevant partition (quorum) for the operation to succeed. In a N shards with 1 replica scenario, there will need to be a single shard active (in this case, one and quorum is the same).]]></tooltip>
                    </field>
                    <field id="safe_reindex" translate="label comment" type="select" sortOrder="120" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Safe Reindex</label>
                        <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                        <comment><![CDATA[Reindex in a temporary index and switch to it once finished. Especially useful for large product catalogs.]]></comment>
                    </field>
                    <field id="query_operator" translate="label comment" type="select" sortOrder="130" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Query Operator</label>
                        <source_model>Wyomind\Elasticsearch\Model\System\Config\Source\Query\Operator</source_model>
                        <comment><![CDATA[For example, with the OR operator, the query "digital camera" is translated to "digital OR camera", with the AND operator, the same query is translated to "digital AND camera". The default value is AND.]]></comment>
                    </field>
                    <field id="enable_fuzzy_query" translate="label comment" type="select" sortOrder="140" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Enable Fuzzy Query</label>
                        <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                        <comment><![CDATA[Enables approximative search.]]></comment>
                    </field>
                    <field id="fuzzy_query_mode" translate="label comment" type="select" sortOrder="150" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Fuzzy Query Mode</label>
                        <source_model>Wyomind\Elasticsearch\Model\System\Config\Source\Fuzzyness\Mode</source_model>
                        <comment><![CDATA[- <b>0</b>, <b>1</b>, <b>2</b>: the maximum allowed Levenshtein Edit Distance (or number of edits)<br/>
- <b>AUTO</b>: generates an edit distance based on the length of the term. For lengths:<br/>
&nbsp;&nbsp;- 0..2: must match exactly<br/>
&nbsp;&nbsp;- 3..5: one edit allowed<br/>
&nbsp;&nbsp;- &gt;5: two edits allowed</br/>
<br/>
<b>AUTO</b> should generally be the preferred value<br/><br/>
More information: <a href="https://www.elastic.co/guide/en/elasticsearch/reference/current/common-options.html#fuzziness">https://www.elastic.co/guide/en/elasticsearch/reference/current/common-options.html#fuzziness</a>]]></comment>
                        <depends>
                            <field id="enable_fuzzy_query">1</field>
                        </depends>
                    </field>
                    <field id="enable_product_weight" translate="label comment" type="select" sortOrder="150" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Enable Product Weight</label>
                        <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                        <comment><![CDATA[Enables product weight modifier (only available for search results)]]></comment>
                    </field>
                    <field id="enable_pub_folder" translate="label comment" type="select" sortOrder="160" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Add the 'pub' folder to the images url</label>
                        <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                        <comment><![CDATA[The Elasticsearch index must be run after changing this option]]></comment>
                    </field>
                </group>
            </group>
        </section>
        <section id="elasticsearch" translate="label" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1">
            <class>separator-top</class>
            <label>Elasticsearch</label>
            <tab>wyomind</tab>
            <resource>Wyomind_Elasticsearch::config_elasticsearch</resource>
            <group id="license" translate="label" sortOrder="1" showInDefault="1" showInWebsite="0" showInStore="0">
                <label>License activation</label>
                <field id="extension_version" translate="label" type="link" sortOrder="1" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Extension version</label>
                </field>
                <field id="activation_key" translate="label comment" type="text" sortOrder="2" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Activation key</label>
                    <comment><![CDATA[<span class="notice">Enter your activation key and click on `save config`.</span>]]></comment>
                    <backend_model>Magento\Config\Model\Config\Backend\Encrypted</backend_model>
                    <frontend_model>Wyomind\Core\Block\Adminhtml\System\Config\Form\Field\Encrypted</frontend_model>
                </field>
                <field id="activation_code" translate="label comment" type="text" sortOrder="4" showInDefault="1" showInWebsite="0" showInStore="0">
                    <label>Activation code</label>
                    <comment><![CDATA[<span class="notice">Enter your license code <b>only if prompted<b>.</span>]]></comment>
                    <backend_model>Magento\Config\Model\Config\Backend\Encrypted</backend_model>
                    <frontend_model>Wyomind\Core\Block\Adminhtml\System\Config\Form\Field\Encrypted</frontend_model>
                </field>
            </group>
            <group id="autocomplete" translate="label" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Autocomplete Settings</label>
                <field id="enable" translate="label comment" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Enable Autocomplete</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                    <comment><![CDATA[If enabled, will override default autocomplete to display products directly.]]></comment>
                </field>
                <group id="advanced" translate="label" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1">
                    <depends>
                        <field id="elasticsearch/autocomplete/enable">1</field>
                    </depends>
                    <label><![CDATA[Template Settings (<u>Advanced users only</u>)]]></label>
                    <field id="load" translate="label comment" type="select" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Load a pre-defined template</label>
                        <frontend_model>Wyomind\Elasticsearch\Block\Adminhtml\System\Config\Form\Field\Templates</frontend_model>
                        <depends>
                            <field id="elasticsearch/autocomplete/enable">1</field>
                        </depends>
                        <comment><![CDATA[A pre-defined template can help you to configure your own template.]]></comment>
                    </field>
                    <field id="css_rules" translate="label comment" type="textarea" sortOrder="30" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Css rules</label>
                        <comment><![CDATA[Be careful when editing this template, any error can break the autocomplete feature.]]></comment>
                        <depends>
                            <field id="elasticsearch/autocomplete/enable">1</field>
                        </depends>
                    </field>
                    <field id="js_template" translate="label comment" type="textarea" sortOrder="40" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Template</label>
                        <comment><![CDATA[Be careful when editing this template, any error can break the autocomplete feature.]]></comment>
                        <depends>
                            <field id="elasticsearch/autocomplete/enable">1</field>
                        </depends>
                    </field>
                </group>
            </group>
            <group id="types" translate="label" showInDefault="1" showInWebsite="1" showInStore="1" sortOrder="30">
                <label>Types Settings</label>
                <group id="doyoumean" translate="label" sortOrder="30" showInDefault="1" showInWebsite="1" showInStore="1">
                    <depends>
                        <field id="elasticsearch/autocomplete/enable">1</field>
                    </depends>
                    <label>Do you mean?</label>
                    <field id="enable_search" translate="label comment" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Enable in Search Results</label>
                        <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                        <comment><![CDATA[If enabled, suggestions will be added to the search results page.]]></comment>
                    </field>
                    <field id="enable_autocomplete" translate="label comment" type="select" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Enable in Autocomplete</label>
                        <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                        <comment><![CDATA[If enabled, suggestions will also be added to autocomplete box.]]></comment>
                        <depends>
                            <field id="elasticsearch/autocomplete/enable">1</field>
                        </depends>
                    </field>
                </group>
                <group id="product" translate="label" sortOrder="40" showInDefault="1" showInWebsite="1" showInStore="1">
                    <depends>
                        <field id="elasticsearch/autocomplete/enable">1</field>
                    </depends>
                    <label>Product</label>
                    <field id="enable_autocomplete" translate="label comment" type="select" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Enable in Autocomplete</label>
                        <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                        <comment><![CDATA[If enabled, products will also be searchable in autocomplete box.]]></comment>
                        <depends>
                            <field id="elasticsearch/autocomplete/enable">1</field>
                        </depends>
                    </field>                    
                    <field id="image_size" translate="label comment" type="text" sortOrder="30" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Image Size</label>
                        <comment><![CDATA[Image size in px, default is 50px.]]></comment>
                        <validate>required-entry validate-greater-than-zero validate-digits</validate>
                        <depends>
                            <field id="elasticsearch/autocomplete/enable">1</field>
                            <field id="enable_autocomplete">1</field>
                        </depends>
                    </field>
                    <field id="autocomplete_limit" translate="label comment" type="text" sortOrder="40" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Autocomplete Limit</label>
                        <comment><![CDATA[Limit the number of results in autocompletion.]]></comment>
                        <validate>required-entry validate-greater-than-zero validate-digits</validate>
                        <depends>
                            <field id="elasticsearch/autocomplete/enable">1</field>
                            <field id="enable_autocomplete">1</field>
                        </depends>
                    </field>
                </group>
                <group id="category" translate="label" sortOrder="50" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Category</label>
                    <field id="enable" translate="label comment" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Enable Category Search</label>
                        <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                        <comment><![CDATA[If enabled, catagories will be indexed in Elasticsearch.]]></comment>
                    </field>
                    <field id="enable_search" translate="label comment" type="select" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Enable in Search Results</label>
                        <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                        <comment><![CDATA[If enabled, categories will also be displayed in left column of search results page.]]></comment>
                        <depends>
                            <field id="enable">1</field>
                        </depends>
                    </field>
                    <field id="limit" translate="label comment" type="text" sortOrder="25" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Search Results Limit</label>
                        <comment><![CDATA[Limit number of results. Zero or empty means unlimited.]]></comment>
                        <validate>validate-greater-than-zero validate-digits</validate>
                        <depends>
                            <field id="enable">1</field>
                            <field id="enable_search">1</field>
                        </depends>
                    </field>
                    <field id="enable_autocomplete" translate="label comment" type="select" sortOrder="30" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Enable in Autocomplete</label>
                        <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                        <comment><![CDATA[If enabled, categories will also be searchable in autocomplete box.]]></comment>
                        <depends>
                            <field id="elasticsearch/autocomplete/enable">1</field>
                            <field id="enable">1</field>
                        </depends>
                    </field>
                    <field id="autocomplete_limit" translate="label comment" type="text" sortOrder="35" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Autocomplete Limit</label>
                        <comment><![CDATA[Limit the number of results in autocompletion.]]></comment>
                        <validate>required-entry validate-greater-than-zero validate-digits</validate>
                        <depends>
                            <field id="enable">1</field>
                            <field id="enable_autocomplete">1</field>
                            <field id="elasticsearch/autocomplete/enable">1</field>
                        </depends>
                    </field>
                    <field id="attributes" translate="label comment" type="multiselect" sortOrder="60" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Searchable Attributes</label>
                        <source_model>Wyomind\Elasticsearch\Model\System\Config\Source\Attribute\Category</source_model>
                        <comment><![CDATA[Define which attributes are searchable.]]></comment>
                        <depends>
                            <field id="enable">1</field>
                        </depends>
                    </field>
                </group>
                <group id="cms" translate="label" sortOrder="60" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>CMS</label>
                    <field id="enable" translate="label comment" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Enable CMS Search</label>
                        <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                        <comment><![CDATA[If enabled, CMS pages will be indexed in Elasticsearch.]]></comment>
                    </field>
                    <field id="enable_search" translate="label comment" type="select" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Enable in Search Results</label>
                        <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                        <comment><![CDATA[If enabled, CMS pages will also be displayed in left column of search results page.]]></comment>
                        <depends>
                            <field id="enable">1</field>
                        </depends>
                    </field>
                    <field id="enable_autocomplete" translate="label comment" type="select" sortOrder="30" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Enable in Autocomplete</label>
                        <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                        <comment><![CDATA[If enabled, CMS pages will also be searchable in autocomplete box.]]></comment>
                        <depends>
                            <field id="enable">1</field>
                            <field id="elasticsearch/autocomplete/enable">1</field>
                        </depends>
                    </field>
                    <field id="autocomplete_limit" translate="label comment" type="text" sortOrder="35" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Autocomplete Limit</label>
                        <comment><![CDATA[Limit the number of results in autocompletion.]]></comment>
                        <validate>required-entry validate-greater-than-zero validate-digits</validate>
                        <depends>
                            <field id="enable">1</field>
                            <field id="enable_autocomplete">1</field>
                            <field id="elasticsearch/autocomplete/enable">1</field>
                        </depends>
                    </field>
                    <field id="limit" translate="label comment" type="text" sortOrder="40" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Search Results Limit</label>
                        <comment><![CDATA[Limit number of results. Zero or empty means unlimited.]]></comment>
                        <validate>validate-greater-than-zero validate-digits</validate>
                        <depends>
                            <field id="enable">1</field>
                            <field id="enable_search">1</field>
                        </depends>
                    </field>
                    <field id="attributes" translate="label comment" type="multiselect" sortOrder="50" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Searchable Attributes</label>
                        <source_model>Wyomind\Elasticsearch\Model\System\Config\Source\Attribute\Cms</source_model>
                        <comment><![CDATA[Define which attributes are searchable.]]></comment>
                        <depends>
                            <field id="enable">1</field>
                        </depends>
                    </field>
                    <field id="excluded_pages" translate="label comment" type="multiselect" sortOrder="60" showInDefault="1" showInWebsite="1" showInStore="1">
                        <label>Excluded Pages</label>
                        <source_model>Wyomind\Elasticsearch\Model\System\Config\Source\Cms\Page</source_model>
                        <comment><![CDATA[Selected CMS pages will be excluded from search.]]></comment>
                        <depends>
                            <field id="enable">1</field>
                        </depends>
                    </field>
                </group>
            </group>
        </section>
    </system>
</config>
